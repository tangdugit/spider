package com.tdu.spider.dao;

import com.tdu.spider.model.JkTopicDO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface JkTopicRepository extends MongoRepository<JkTopicDO, String> {
}
