package com.tdu.spider.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tdu.spider.model.XclientVersionDO;

public interface XclientVersionRepository  extends MongoRepository<XclientVersionDO,String>{
}
