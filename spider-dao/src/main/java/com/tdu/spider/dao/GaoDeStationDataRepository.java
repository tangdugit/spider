package com.tdu.spider.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tdu.spider.model.GaoDeStationDataDO;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: GaoDeStationDataRepository.java, v 0.1 2017年12月2017/12/1日 下午4:53 tangdu Exp $
 * @name TODO: GaoDeStationDataRepository
 */
public interface GaoDeStationDataRepository extends MongoRepository<GaoDeStationDataDO,String> {
}
