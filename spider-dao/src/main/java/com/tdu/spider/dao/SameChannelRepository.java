package com.tdu.spider.dao;

import com.tdu.spider.model.SameChannelDO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SameChannelRepository extends MongoRepository<SameChannelDO,Long> {

}
