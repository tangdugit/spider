package com.tdu.spider.dao;

import com.tdu.spider.model.JkCategoryDO;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: JkCategoryRepository.java, v 0.1 2017年06月2017/6/7日 下午10:31 tangdu Exp $
 * @name TODO: JkCategoryRepository
 */
public interface JkCategoryRepository extends MongoRepository<JkCategoryDO,Integer> {
}
