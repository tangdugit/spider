package com.tdu.spider.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tdu.spider.model.XclientInfoDO;


public interface XclientInfoDORepository extends MongoRepository<XclientInfoDO,String> {
}
