package com.tdu.spider.dao;

import com.tdu.spider.model.SameChannelActivityDO;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SameChannelActivityRepository.java, v 0.1 2018年01月26日 下午10:33 tangdu Exp $
 */
public interface SameChannelActivityRepository extends MongoRepository<SameChannelActivityDO,Long> {
}
