package com.tdu.spider.dao;


import com.tdu.spider.model.JkMessageDO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface JkMessageRepository extends MongoRepository<JkMessageDO,String> {
}
