package com.tdu.spider.dao.tv;

import com.tdu.spider.model.tv.CategoryDO;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: CategoryRepository.java, v 0.1 2018年07月13日 下午3:48 tangdu Exp $
 */
public interface CategoryRepository extends MongoRepository<CategoryDO,String> {
}
