package com.tdu.spider.dao;

import com.tdu.spider.model.SameUserDO;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SameUserRepository.java, v 0.1 2018年02月03日 下午8:36 tangdu Exp $
 */
public interface SameUserRepository extends MongoRepository<SameUserDO,Long> {
}
