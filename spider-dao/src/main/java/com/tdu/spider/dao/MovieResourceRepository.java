package com.tdu.spider.dao;

import com.tdu.spider.model.MovieResourceDO;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieResourceRepository.java, v 0.1 2018年03月06日 下午10:33 tangdu Exp $
 */
public interface MovieResourceRepository extends MongoRepository<MovieResourceDO, String> {


    MovieResourceDO findByDoubanId(String doubanId);
}
