package com.tdu.spider.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tdu.spider.model.UserDO;

import java.util.List;

public interface UserRepository extends MongoRepository<UserDO, String> {

    List<UserDO> findByType(String type);

    UserDO findByUserId(String userId);
}
