package com.tdu.spider.dao;


import com.tdu.spider.model.SameSenseDO;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SameSenseRepository extends MongoRepository<SameSenseDO,Long> {

    public List<SameSenseDO> findByUser_id(Long user_id);
}
