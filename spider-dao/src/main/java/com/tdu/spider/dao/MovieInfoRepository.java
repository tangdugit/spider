package com.tdu.spider.dao;

import com.tdu.spider.model.MovieInfoDO;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieInfoRepository.java, v 0.1 2018年03月06日 下午10:33 tangdu Exp $
 */
public interface MovieInfoRepository extends MongoRepository<MovieInfoDO, String> {


}
