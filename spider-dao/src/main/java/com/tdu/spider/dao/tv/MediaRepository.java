package com.tdu.spider.dao.tv;

import com.tdu.spider.model.tv.MediaDO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MediaRepository.java, v 0.1 2018年07月13日 下午3:48 tangdu Exp $
 */
public interface MediaRepository extends MongoRepository<MediaDO, String> {

    Page<MediaDO> findByCategoryCode(String categoryCode, Pageable pageable);

}

