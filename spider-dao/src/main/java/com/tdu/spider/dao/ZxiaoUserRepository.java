package com.tdu.spider.dao;

import com.tdu.spider.model.ZxiaoUserDO;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ZxiaoUserRepository extends MongoRepository<ZxiaoUserDO,Long> {
}
