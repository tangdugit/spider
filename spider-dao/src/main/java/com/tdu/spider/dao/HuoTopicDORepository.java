package com.tdu.spider.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tdu.spider.model.HuoTopicDO;


public interface HuoTopicDORepository extends MongoRepository<HuoTopicDO,Integer> {
}
