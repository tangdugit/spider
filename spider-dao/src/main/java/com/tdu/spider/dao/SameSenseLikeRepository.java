package com.tdu.spider.dao;

import com.tdu.spider.model.SenseLikeDO;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SenseLikeRepository.java, v 0.1 2018年02月01日 下午4:50 tangdu Exp $
 */
public interface SameSenseLikeRepository extends MongoRepository<SenseLikeDO,Long> {

}
