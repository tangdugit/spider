package com.tdu.spider.dao;

import com.tdu.spider.model.JkPictureDO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface JkPictureRepository extends MongoRepository<JkPictureDO,String> {
}
