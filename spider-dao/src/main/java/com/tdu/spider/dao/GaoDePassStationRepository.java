package com.tdu.spider.dao;

import com.tdu.spider.model.GaoDePassStationDO;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: GaoDePassStationRepository.java, v 0.1 2017年12月2017/12/1日 下午8:11 tangdu Exp $
 * @name TODO: GaoDePassStationRepository
 */
public interface GaoDePassStationRepository extends MongoRepository<GaoDePassStationDO,String> {
}
