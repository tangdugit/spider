package com.tdu.spider.dao;


import org.springframework.data.mongodb.repository.MongoRepository;

import com.tdu.spider.model.HuoArcitleDO;

public interface HuoArcitleRepository extends MongoRepository<HuoArcitleDO,Long> {
}
