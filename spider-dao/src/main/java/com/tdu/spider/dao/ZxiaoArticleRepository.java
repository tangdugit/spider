package com.tdu.spider.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tdu.spider.model.ZxiaoArticleDO;


public interface ZxiaoArticleRepository extends MongoRepository<ZxiaoArticleDO,String> {
}
