package com.tdu.spider.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "xclient_version")
public class XclientVersionDO {

    @Id
    private String  id;
    private Integer viewNum;
    private String  infoId;
    private String  baiduLink;
    private String  baiduPwd;
    private String  version;
    private String  activeDesc;
    private String  updateLog;
    private String  appDesc;
    private String  updateDate;
    private String  size;
    /**没有版权显示官方下载**/
    private String  officialUrl;
}
