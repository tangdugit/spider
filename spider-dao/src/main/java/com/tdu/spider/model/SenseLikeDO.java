package com.tdu.spider.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SenseLikeDO.java, v 0.1 2018年02月01日 下午4:49 tangdu Exp $
 */
@Setter
@Getter
@Document(collection = "same_sense_like")
public class SenseLikeDO {
    private Long userId;
    /** 评论是属于哪个用户的 s**/
    private Long targetUserId;

    private String username;
    private String avatar;
    private Integer created_at;
    /**1:浏览，2:喜欢**/
    private Integer source;
    private Long senseId;
}
