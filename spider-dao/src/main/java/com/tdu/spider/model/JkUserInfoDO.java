package com.tdu.spider.model;

import lombok.Data;

@Data
public class JkUserInfoDO {

    private UserDO      user;
    private UserStateDO statsCount;

    @Data
    public class UserDO {
        private String  id;
        private String  username;
        private Integer userId;
        private String  screenName;
        private String  updatedAt;
    }

    @Data
    public class UserStateDO {
        private Integer topicSubscribed;
        private Integer topicCreated;
        private Integer followedCount;
        private Integer followingCount;
    }
}
