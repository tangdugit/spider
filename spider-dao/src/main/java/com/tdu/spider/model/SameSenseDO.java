package com.tdu.spider.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "t_same_sense")
public class SameSenseDO {
    @Id
    private Long               id;
    private Boolean            is_active;
    private String             txt;
    private Long               user_id;

    private Long               created_at;
    private Long               channel_id;
    private String             photo;
    private Integer            height;
    private Integer            width;
    private Integer            media_id;
    private String             medis_src;
    private Boolean            is_folded;
    private Integer            cate;
    private Integer            likes;
    private Integer            views;
    private Integer            replies;

    private SameUserDO         user;
    private SameChannelDO      channel;
    private List<SameReplieDO> recent_replies;
    private Integer            is_liked;


}
