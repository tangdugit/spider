package com.tdu.spider.model;
import lombok.Data;

@Data
public class JkMaintainerDO {
    private String username;
    private String screenName;
    private String updatedAt;
    private String profileImageUrl;
    private String bio;
    private String gender;
    private String ref;
    private JkPictureDO avatarImage;
}
