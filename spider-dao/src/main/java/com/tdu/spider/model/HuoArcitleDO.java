package com.tdu.spider.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@Document(collection = "huo_arcitles")
public class HuoArcitleDO implements Serializable {

    @Id
    private Long    id;

    private Integer topic_id;

    private String  topic_name;

    private String  topic_type;

    private Integer view;

    private Long    author_id;

    private String  link_url;

    private String  voice_url_mp3;

    private String  voice_url_aac;

    private String  voice_thumb;

    private String  voice_thumb_org;

    private Integer voice_view;

    private String  voice_long_time;

    private String  voice_wave_data;

    private double  voice_sample_rate;

    private String  author;

    private String  avatar;

    private String  role;

    private String  thumb;

    private String  thumb_org;

    private String  width;

    private String  height;

    private String  content;

    private Integer heart;

    private Integer forward;

    private Integer repost;

    private Integer comment;

    private String  created;

    private String  pubtime2;

    private Integer is_private;

    private String  vip_type;

    private String  relation;

    private Integer isself;

    private Integer is_fav;

    private Integer is_like;
}
