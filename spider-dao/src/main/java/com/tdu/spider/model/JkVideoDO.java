package com.tdu.spider.model;

import lombok.Data;

@Data
public class JkVideoDO {
    private String type;
    private String thumbnailUrl;
    private Integer duration;
}
