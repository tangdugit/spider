package com.tdu.spider.model;
import lombok.Data;

@Data
public class SameReplieDO {
    private Long id;
    private Long created_at;
    private String content;
    private String target_type;
    private String user_id;
    private Long target_id;
    private Long target_user_id;
    private Long target_parent_id;
    private Long target_parent_user_id;

}
