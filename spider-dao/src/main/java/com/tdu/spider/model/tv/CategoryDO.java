package com.tdu.spider.model.tv;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: CategoryModel.java, v 0.1 2018年07月11日 下午11:13 tangdu Exp $
 */
@Data
@Document(collection = "tv_category")
public class CategoryDO {
    private String categoryCode;
    private String categoryTitle;
    private String categoryImage;
    private int sno;
}
