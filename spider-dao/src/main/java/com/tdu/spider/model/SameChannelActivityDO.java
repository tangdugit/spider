package com.tdu.spider.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 活动状态分析(每个渠道)
 *
 * @author tangdu
 * @version $: SameChannelActivityDO.java, v 0.1 2018年01月26日 下午10:33 tangdu Exp $
 */
@Data
@Document(collection = "t_same_channel_activity")
public class SameChannelActivityDO {
    @Id
    private Long               id;
    /** yyyy-MM-dd HH格式**/
    private String time;

    private Long channelId;
    private String channelName;
    private Integer online;
}
