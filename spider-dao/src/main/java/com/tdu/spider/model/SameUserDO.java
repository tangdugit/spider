package com.tdu.spider.model;


import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;


@Setter
@Getter
@Document(collection = "same_user_info")
public class SameUserDO {
    private Long    id;
    private String  username;
    private String  avatar;
    private Long    created_at;
    private String  latest;
    private String  timezone;
    private Boolean is_active;
    /** 1男，2女 **/
    private Integer sex;
    private String  token;
    private String  auth_token;
    private String  mobile;

    private String  email;
    //是否可以下载图片
    private String  is_download;
    private String  push_token;
    //文章数
    private Integer senses;
    private Long    join_at;
    //频道数
    private Integer channels;
    private UserMetaDO meta;

}
