package com.tdu.spider.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: GaoDePassStationDO.java, v 0.1 2017年12月2017/12/1日 下午8:10 tangdu Exp $
 * @name TODO: GaoDePassStationDO
 */
@Data
@Document(collection = "wp_passstation")
public class GaoDePassStationDO {

    private String name;
    private String address;
    //1 可乘降 0无
    private int takeDown;
    //1 行李 0无
    private int takeLuggage;
    //1 包裹 0无
    private int takePackage;
}
