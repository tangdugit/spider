package com.tdu.spider.model;

import lombok.Data;

@Data
public class JkPictureDO {
    private String  format;
    private String  middlePicUrl;
    private String  picUrl;
    private String  thumbnailUrl;
    private Double  cropperPosX;
    private Double  cropperPosY;
    private Integer width;
    private Integer height;
}
