package com.tdu.spider.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "jk_category")
public class JkCategoryDO {
    private Integer id;
    private String  name;
    private String  pictureUrl;
    private String  iconUrl;
    private String  avatarUrl;
    private Integer count;
}
