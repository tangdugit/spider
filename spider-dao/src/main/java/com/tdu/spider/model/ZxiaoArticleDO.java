package com.tdu.spider.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "zxiao_article")
public class ZxiaoArticleDO {
    @Id
    private String nid;

    private String title;

    private String author;

    private String video;

    private String video_url;

    private String h5;

    private String h5url;

    private List<String> images ;

    private String images_show;

    private String images_total;

    private String read_num;

    private String url;

    private String is_new;

    private String posttime;

    private String have_time;

    private String is_business;

    private String portion;

    private String surplus;

    private String score;
}
