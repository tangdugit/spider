package com.tdu.spider.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: GaoDeStationDataDO.java, v 0.1 2017年12月2017/12/1日 下午4:53 tangdu Exp $
 * @name TODO: GaoDeStationDataDO
 */
@Data
@Document(collection = "wp_station")
public class GaoDeStationDataDO {
    @Id
    private String id;
    private String name;
    private String type;
    private String typecode;
    private String address;
    private String location;
    private String tel;
    private String pname;
    private String pcode;
    private String cityname;
    private String citycode;
    private String adname;
    private String adcode;
}
