package com.tdu.spider.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "zxiao_user")
public class ZxiaoUserDO {

    @Id
    private Long userId;

    private String token;

    private String sessionId;


}
