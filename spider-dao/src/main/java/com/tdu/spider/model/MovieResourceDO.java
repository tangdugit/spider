package com.tdu.spider.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieResourceDO.java, v 0.1 2018年03月06日 下午10:06 tangdu Exp $
 */
@Setter
@Getter
@Document(collection = "movie_resource")
public class MovieResourceDO {

    @Id
    String id;

    //豆瓣ID
    String doubanId;

    //名称
    Integer name;

    //电影1、连续剧2
    Integer type;

    //创建时间
    Date created;

    //更新时间
    Date updated;

    //是否有效
    Boolean valid;

    //资源路径
    List<MovieResourceDetailDO> detail;

    public class MovieResourceDetailDO{

        //名称
        Integer title;

        //来源（腾讯\）
        String source;

        //资源类型(m3u8/mp4/flv)
        String type;

        //资源路径
        String url;


        //清晰度
        String definition;

        //是否有效
        Boolean valid;

        //大小(M)
        String size;

        Integer sno;
    }
}
