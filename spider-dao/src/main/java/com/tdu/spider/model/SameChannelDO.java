package com.tdu.spider.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "t_same_channel")
@Setter
@Getter
public class SameChannelDO {

    @Id
    private Long                id;
    private String              name;
    private String              icon;
    private Integer             cate;
    private Integer             times;
    private Long                user_id;
    private Integer             mode;
    private SameChannelConfigDO config;

    public static SameChannelDO build(Long id, String name) {
        SameChannelDO sameChannelDO = new SameChannelDO();
        sameChannelDO.setId(id);
        sameChannelDO.setName(name);
        return sameChannelDO;
    }

    @Override
    public boolean equals(Object o) {
        SameChannelDO that = (SameChannelDO) o;
        return id.equals(that.getId());
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Setter
    @Getter
    public class SameChannelConfigDO {

        private List<SameChannelTabVO> tabs;

        private List<SameChannelActionsVO> actions;

        @Setter
        @Getter
        public class SameChannelActionsVO {
            private String action;
            private String icon;
            private String selected_icon;
        }

        @Data
        public class SameChannelTabVO {

            private Integer default_tab;

            private List<SameChannelTabContent> content;

            @Data
            public class SameChannelTabContent {
                private String title;
                private String url;
                private String style;
                private String description;
                private String empty_description;
            }

        }
    }

}
