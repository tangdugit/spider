package com.tdu.spider.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "same_user")
public class UserDO {

    @Id
    private String id;

    private String userId;

    private String name;

    private String token;

    /**same,jk**/
    private String type;

    private String cookie;

    private String pwd;

}
