package com.tdu.spider.model;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SameLikeCountVO.java, v 0.1 2018年02月03日 下午9:44 tangdu Exp $
 */
@Getter
@Setter
public class SameLikeCountVO {
    private Long    userId;
    private String  username;
    private String  avatar;
    private Integer usernum;
}
