package com.tdu.spider.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@Document(collection = "jk_topic")
public class JkTopicDO {

    @Id
    private String         id;
    private Boolean        anonymous;
    private String         briefIntro;
    private String         content;
    private Date           createdAt;
    private Boolean        dreamTopic;
    private String         friendsAlsoSubscribe;
    private String         keywords;
    private Date           lastMessagePostTime;
    private String         messagePrefix;
    private String         operateStatus;
    private String         pictureUrl;
    private String         ref;
    private Date           subscribedAt;
    private Integer        subscribedStatusRawValue;
    private String         thumbnailUrl;
    private Date           timeForRank;
    private Integer        topicId;
    private String         topicType;
    private Date           updatedAt;
    private Boolean        valid;
    private JkPictureDO    rectanglePicture;
    private JkPictureDO    squarePicture;
    private JkMaintainerDO maintainer;
    private String         internalTags;
    private List<String>   customTags;
    private Boolean        isDreamTopic = false;
    private Boolean        isAnonymous  = false;
    /**内容数**/
    private Integer        collectCount;
    /**评论数**/
    private Integer        commentCount;
    /**热度**/
    private Integer        popularity;
    /**喜欢数**/
    private Integer        likeCount;
    /**子标题**/
    private String         subtitle;
    /**订阅数**/
    private Integer        subscribersCount;
}
