package com.tdu.spider.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieInfoDO.java, v 0.1 2018年04月15日 上午7:29 tangdu Exp $
 */
@Setter
@Getter
@Document(collection = "movie_info")
public class MovieInfoDO {
    //分类（reying,jiying,top250,koubei,beimei,xinpian）
    private List<String> categorys;
    private RatingDO rating;
    private List<String> genres;
    private String title;
    private List<DirectorsDO> casts;
    private Integer collect_count;
    private String original_title;
    private String subtype;
    private String year;
    private String alt;
    @Id
    private String id;
    private MovieImageDO images;

    @Setter
    @Getter
    public static class RatingDO {
        private Integer max;
        private Float average;
        private String stars;
        private Integer min;
    }

    @Setter
    @Getter
    public static class DirectorsDO {
        private String alt;
        private String name;
        private String id;
        private MovieImageDO avatars;
    }

    @Setter
    @Getter
    public static class MovieImageDO {
        private String small;
        private String large;
        private String medium;
    }
}
