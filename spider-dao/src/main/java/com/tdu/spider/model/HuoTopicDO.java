package com.tdu.spider.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@Document(collection = "huo_topics")
public class HuoTopicDO implements Serializable {
    @Id
    Integer topic_id;
    String topic_name;
    String topic_type;
    Integer  view;
    Integer user_id;
    String user_name;
    Integer thread_count;

}
