package com.tdu.spider.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@Document(collection = "jk_message")
public class JkMessageDO {
    @Id
    private String            id;
    private String            content;
    private String            title;
    private Long              messageId;
    private Integer           topicId;
    private String            linkUrl;
    private String            videoLink;
    private String            sourceRawValue;
    private String            iconUrl;
    private String            messagePrefix;
    private Integer           collectCount;
    private Integer           commentCount;
    private Integer           popularity;
    private Integer           likeCount;
    private Boolean           withPush;
    private Boolean           read;
    private Boolean           collected;
    private Boolean           liked;
    private Date              createdAt;
    private Date              updatedAt;
    private List<JkPictureDO> pictureUrls;
    private JkVideoDO         video;
}
