package com.tdu.spider.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "xclient_info")
public class XclientInfoDO {

    @Id
    private String  id;
    private String  url;
    private String  img;
    private String  title;
    private Integer downLoadNum;
    private String  updateDate;
    private String  content;
    private String  cates;
    private String  catesLink;
}
