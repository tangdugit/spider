package com.tdu.spider.web.controller.zhihu;

import com.tdu.spider.biz.service.zhihu.HtmlExtractService;
import com.tdu.spider.biz.service.zhihu.vo.AnswerQueryVO;
import com.tdu.spider.biz.service.zhihu.vo.AnswerResultVO;
import com.tdu.spider.biz.service.zhihu.vo.QuestionResultVO;
import com.tdu.spider.biz.service.zhihu.vo.TopicVO;
import com.tdu.spider.biz.vo.Result;
import com.tdu.spider.web.controller.vo.WebResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("zhihu")
public class ZhihuController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ZhihuController.class);
    @Autowired
    private HtmlExtractService  htmlExtractService;

    @RequestMapping(value = { "question" })
    public String index(Model model) {
        model.addAttribute("topics",htmlExtractService.queryAllTopic());
        return "zhihu/question";
    }

    @RequestMapping("queryAnswer")
    @ResponseBody
    public Result<AnswerResultVO> queryAnswer(AnswerQueryVO answerQueryVO) {
        try {
            return WebResult.success(htmlExtractService.queryAnswer(answerQueryVO));
        } catch (Exception e) {
            LOGGER.error("queryAnswer", e);
            return WebResult.failed(e.getMessage());
        }
    }

    @RequestMapping("queryQuestion")
    @ResponseBody
    public Result<QuestionResultVO> queryQuestion(AnswerQueryVO answerQueryVO) {
        try {
            return WebResult.success(htmlExtractService.queryQuestionrByTopic(answerQueryVO));
        } catch (Exception e) {
            LOGGER.error("queryQuestion", e);
            return WebResult.failed(e.getMessage());
        }
    }

    @RequestMapping("queryTopic")
    @ResponseBody
    public Result<List<TopicVO>> queryTopic() {
        try {
            return WebResult.success(htmlExtractService.queryAllTopic());
        } catch (Exception e) {
            LOGGER.error("queryTopic", e);
            return WebResult.failed(e.getMessage());
        }
    }
}
