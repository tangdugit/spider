package com.tdu.spider.web.controller.same;

import com.alibaba.fastjson.JSON;
import com.tdu.spider.biz.service.same.SameAnalyseService;
import com.tdu.spider.biz.service.same.SameService;
import com.tdu.spider.biz.service.same.vo.SameAnalyseInfoVO;
import com.tdu.spider.biz.service.same.vo.SameAnalyseQueryVO;
import com.tdu.spider.biz.service.same.vo.SameAnyQueryVO;
import com.tdu.spider.biz.service.same.vo.SameSenseQueryVO;
import com.tdu.spider.biz.vo.Result;
import com.tdu.spider.model.SameChannelDO;
import com.tdu.spider.model.SameUserDO;
import com.tdu.spider.web.controller.vo.WebResult;
import com.tdu.spider.web.controller.vo.WordAnalyseVO;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.util.Args;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/supersame")
@ResponseBody
public class SameController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SameController.class);


    @Autowired
    private SameService sameService;

    @Autowired
    private SameAnalyseService sameAnalyseService;

    @RequestMapping("query_user_info")
    public Result<SameUserDO> getUserInfo(Long userId) {
        try {
            return WebResult.success(sameService.getUserInfo(userId));
        } catch (Exception e) {
            LOGGER.error("queryUserInfo", e);
            return WebResult.failed("查询失败");
        }
    }

    @RequestMapping("query_hot_channel_info")
    public Result<List<SameChannelDO>> query_hot_channel_info(Long channelId) {
        try {
            List<SameChannelDO> channels =sameService.queryHotChannelInfo();
            return WebResult.success(channels);
        } catch (Exception e) {
            LOGGER.error("query_hot_channel_info", e);
            return WebResult.failed(e.getMessage());
        }
    }



    @RequestMapping("channel_sense")
    public Result<Boolean> channel_sense(Long channelId) {
        try {
            Args.notNull(channelId, "channelId");
            SameSenseQueryVO sameSenseQueryVO = new SameSenseQueryVO();
            sameSenseQueryVO.setChannelId(channelId);
            sameService.pageQuerySense(sameSenseQueryVO);
            return WebResult.success(true);
        } catch (Exception e) {
            LOGGER.error("channelSense", e);
        }
        return WebResult.failed("notFound");
    }

    @RequestMapping("same_spider_analyse")
    public Result<Boolean> same_spider_analyse(Long userId,Boolean getLikes) {
        try {
            Args.notNull(userId, "userId");
            SameAnyQueryVO sameAnyQueryVO=new SameAnyQueryVO();
            sameAnyQueryVO.setUserId(userId);
            if(getLikes==null){
                getLikes=true;
            }
            sameAnyQueryVO.setGetLikes(getLikes);
            sameService.spiderUserSenseAndLikes(sameAnyQueryVO);
            return WebResult.success(true);
        } catch (Exception e) {
            LOGGER.error("same_spider_analyse", e);
        }
        return WebResult.failed("notFound");
    }

    @RequestMapping("same_user_info")
    public Result<SameAnalyseInfoVO> same_user_info(Long userId) {
        try {
            Args.notNull(userId, "userId");
            SameAnalyseQueryVO sameAnyQueryVO=new SameAnalyseQueryVO();
            sameAnyQueryVO.setUserId(userId);
            SameAnalyseInfoVO analyseInfoVO = sameAnalyseService.analyseInfoVO(sameAnyQueryVO);
            return WebResult.success(analyseInfoVO);
        } catch (Exception e) {
            LOGGER.error("same_user_info", e);
        }
        return WebResult.failed("notFound");
    }


    @RequestMapping("same_user_analyse")
    public Result<SameAnalyseInfoVO> same_user_analyse(Long userId) {
        try {
            Args.notNull(userId, "userId");
            SameAnalyseQueryVO sameAnyQueryVO=new SameAnalyseQueryVO();
            sameAnyQueryVO.setUserId(userId);
            SameAnalyseInfoVO analyseInfoVO = sameAnalyseService.analyseInfoVO(sameAnyQueryVO);
            //调用
            Response response = Request.Get("http://192.168.1.6:8000/useranalyse/" + userId).execute();
            String string = response.returnContent().asString();
            if(StringUtils.hasLength(string)){
                WordAnalyseVO analyseResult = JSON.parseObject(string, WordAnalyseVO.class);
                if("OK".equals(analyseResult.getStatus())){
                    analyseInfoVO.setKeyword(analyseResult.getData().getKeyword());
                    analyseInfoVO.setMayun(analyseResult.getData().getMayun());
                }
            }
            return WebResult.success(analyseInfoVO);
        } catch (Exception e) {
            LOGGER.error("same_user_analyse", e);
        }
        return WebResult.failed("notFound");
    }

}
