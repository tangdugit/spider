package com.tdu.spider.web.controller.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: WordAnalyseResult.java, v 0.1 2018年02月04日 下午9:37 tangdu Exp $
 */
@Setter
@Getter
public class WordAnalyseVO {

    private String status;

    private WordAnalyseDate data;

    @Setter
    @Getter
    public class WordAnalyseDate {
        private String mayun;
        private String keyword;
    }
}
