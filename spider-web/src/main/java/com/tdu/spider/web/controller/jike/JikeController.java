package com.tdu.spider.web.controller.jike;

import com.tdu.spider.biz.service.jike.JikeService;
import com.tdu.spider.biz.service.xclient.XclientService;
import com.tdu.spider.biz.vo.Result;
import com.tdu.spider.web.controller.vo.WebResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/jike")
public class JikeController {

    @Autowired
    private JikeService jikeService;


    @Autowired
    private XclientService xclientService;

    @RequestMapping("loadTopic")
    @ResponseBody
    public Result<Boolean> 加载所有主题() {
        try {
            jikeService.updateTopic();
            return WebResult.success(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            return WebResult.failed(ex.getMessage());
        }
    }

    @RequestMapping("loadMessage")
    @ResponseBody
    public Result<Boolean> 加载所有消息() {
        try {
            jikeService.updateMessage();
            return WebResult.success(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            return WebResult.failed(ex.getMessage());
        }
    }


    @RequestMapping("daliyUpdate")
    @ResponseBody
    public Result<Boolean> daliyUpdate() {
        try {
            jikeService.daliyUpdate();
            return WebResult.success(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            return WebResult.failed(ex.getMessage());
        }
    }

    @RequestMapping("updateBestTopic")
    @ResponseBody
    public Result<Boolean> 按类目查找主题() {
        try {
            jikeService.updateBestTopic();
            return WebResult.success(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            return WebResult.failed(ex.getMessage());
        }
    }

    @RequestMapping("loadCategory")
    @ResponseBody
    public Result<Boolean> 加载所有主题类目() {
        try {
            jikeService.updateCategorys();
            return WebResult.success(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            return WebResult.failed(ex.getMessage());
        }
    }

    @RequestMapping("/xclient/batch")
    @ResponseBody
    public Result<Boolean> batch() {
        try {
            xclientService.batch();
            return WebResult.success(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            return WebResult.failed(ex.getMessage());
        }
    }

    @RequestMapping("/xclient/batchVersion")
    @ResponseBody
    public Result<Boolean> batchVersion() {
        try {
            xclientService.batchVersion();
            return WebResult.success(true);
        } catch (Exception ex) {
            ex.printStackTrace();
            return WebResult.failed(ex.getMessage());
        }
    }
}
