package com.tdu.spider.web.controller.article;

import com.tdu.spider.biz.service.article.ArticleService;
import com.tdu.spider.biz.service.article.vo.ArtcleTag;
import com.tdu.spider.biz.service.article.vo.ArticleQueryVO;
import com.tdu.spider.biz.service.article.vo.ArticleVO;
import com.tdu.spider.biz.service.article.vo.Page;
import com.tdu.spider.biz.vo.Result;
import com.tdu.spider.web.controller.vo.WebResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/article")
public class ArticleController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArticleController.class);

    @Autowired
    private ArticleService articleService;

    @RequestMapping("/page")
    public Result<Page<ArticleVO>> list(ArticleQueryVO photoQueryVO) {
        try {
            return WebResult.success(articleService.page(photoQueryVO));
        } catch (Exception ex) {
            LOGGER.error("list", ex);
            return WebResult.failed(ex.getMessage());
        }
    }

    @RequestMapping("/view")
    public Result<ArticleVO> view(Long articleId) {
        try {
            return WebResult.success(articleService.view(articleId));
        } catch (Exception ex) {
            LOGGER.error("view", ex);
            return WebResult.failed(ex.getMessage());
        }
    }

    @RequestMapping("/tags")
    public Result<List<ArtcleTag>> tags() {
        try {
            return WebResult.success(articleService.tags());
        } catch (Exception ex) {
            LOGGER.error("tags", ex);
            return WebResult.failed(ex.getMessage());
        }
    }

    @RequestMapping("/rlist")
    public Result<List<ArticleVO>> rlist(Long articleId) {
        try {
            return WebResult.success(articleService.rList(articleId));
        } catch (Exception ex) {
            LOGGER.error("rlist", ex);
            return WebResult.failed(ex.getMessage());
        }
    }
}
