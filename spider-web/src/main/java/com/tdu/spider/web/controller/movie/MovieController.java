package com.tdu.spider.web.controller.movie;

import com.google.common.collect.Lists;
import com.tdu.spider.biz.cont.Keys;
import com.tdu.spider.biz.service.douban.DouMovieService;
import com.tdu.spider.biz.service.movie.CategoryVO;
import com.tdu.spider.biz.service.movie.MovieApiService;
import com.tdu.spider.biz.service.movie.MovieInfoVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieController.java, v 0.1 2018年03月06日 下午10:01 tangdu Exp $
 */
@RequestMapping("/movie")
@RestController
@Slf4j
public class MovieController {

    @Autowired
    private DouMovieService douMovieService;

    @Autowired
    private MovieApiService movieApiService;

    @CrossOrigin
    @RequestMapping("/dou_api")
    public String douApi(String url) {
        try {
            Request request = Request.Get(url).userAgent(Keys.USER_AGENT);
            return request.execute().returnContent().asString();
        } catch (Exception e) {
            log.error("dou_api", e);
            return "";
        }
    }


    @RequestMapping("/list")
    public List<MovieInfoVO> list() {
        try {
            return movieApiService.getMainMovieList();
        } catch (Exception e) {
            log.error("list", e);
            return Lists.newArrayList();
        }
    }

    @RequestMapping("/category")
    public List<CategoryVO> category() {
        try {
            return movieApiService.getCategory();
        } catch (Exception e) {
            log.error("category", e);
            return Lists.newArrayList();
        }
    }
}
