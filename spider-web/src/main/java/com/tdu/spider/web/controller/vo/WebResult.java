package com.tdu.spider.web.controller.vo;

import com.tdu.spider.biz.vo.Result;


public class WebResult {
    public static <T> Result<T> failed(String message){
        Result result=new Result();
        result.setMessage(message);
        result.setCode(1);
        return result;
    }

    public static <T>  Result<T> success(T data){
        Result result=new Result();
        result.setData(data);
        result.setCode(0);
        return result;
    }
}
