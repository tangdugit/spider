package com.tdu.run;

import org.junit.runner.RunWith;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: CommonTest.java, v 0.1 2017年11月2017/11/30日 上午6:41 tangdu Exp $
 * @name TODO: CommonTest
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class CommonTest {
}
