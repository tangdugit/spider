package com.tdu;


import com.tdu.run.Application;
import com.tdu.spider.biz.service.qutou.QuTouApiService;
import com.tdu.spider.biz.service.qutou.vo.QuTouArticleDataVO;
import com.tdu.spider.biz.service.qutou.vo.QuTouArticleQueryVO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class QutouTest {

    @Autowired
    private QuTouApiService quTouApiService;

    @Test
    public void 测试分页(){
        QuTouArticleQueryVO quTouArticleQueryVO=new QuTouArticleQueryVO();
        quTouArticleQueryVO.setSign("5b0b0b0560e97faea35d1ba2a8e31ff0");
        quTouArticleQueryVO.setContent_type("1%2C2%2C4%2C3");
        quTouArticleQueryVO.setUuid("02E2A164-B150-4036-8709-9EEDC5389D20");
        quTouArticleQueryVO.setToken("fd954ANyuLjIrmRRgCGsRTEJ2sHg2O9gtBkMbpuzR9F3R4K-33bIZGe_kSa_wGt3b3EELomF22tMITcU");
        try {
            List<QuTouArticleDataVO> quTouArticleDataVOS = quTouApiService.queryArticle(quTouArticleQueryVO);
            System.err.println(quTouArticleDataVOS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
