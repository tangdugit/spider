//package com.tdu.same.test;
//
//import com.tdu.run.Application;
//import org.elasticsearch.index.query.BoolQueryBuilder;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.SpringBootConfiguration;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
//import org.springframework.data.elasticsearch.core.query.*;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.List;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = Application.class)
//@SpringBootConfiguration
//public class ElasticSearchTest {
//
//    @Autowired
//    private ElasticsearchTemplate elasticsearchTemplate;
//
//    @Test
//    public void 测试创建索引(){
//        boolean test = elasticsearchTemplate.createIndex("test001");
//        Assert.assertTrue(test);
//    }
//
//    @Test
//    public void 测试新增记录(){
//        Customer customer=new Customer();
//        customer.setAge(222);
//        customer.setName("我是中国人");
//        IndexQuery indexQuery=new IndexQueryBuilder().withId("8").withObject(customer).build();
//        elasticsearchTemplate.putMapping(Customer.class);
//        String index = elasticsearchTemplate.index(indexQuery);
//        System.out.println(index);
//    }
//
//    @Test
//    public void 测试查询记录(){
//        NativeSearchQueryBuilder nativeSearchQueryBuilder=new NativeSearchQueryBuilder();
//        BoolQueryBuilder qb= QueryBuilders.boolQuery();
//        qb.must(QueryBuilders.matchQuery("id","1"));
//        nativeSearchQueryBuilder.withQuery(qb);
//        SearchQuery query=nativeSearchQueryBuilder.build();
//        List<Customer> customers = elasticsearchTemplate.queryForList(query, Customer.class);
//        System.out.println(customers);
//    }
//
//    @Test
//    public void 测试删除记录(){
//        DeleteQuery deleteQuery=new DeleteQuery();
//        BoolQueryBuilder qb= QueryBuilders. boolQuery();
//        qb.must(QueryBuilders.matchQuery("id","1"));
//        deleteQuery.setQuery(qb);
//        elasticsearchTemplate.delete(deleteQuery,Customer.class);
//    }
//}
