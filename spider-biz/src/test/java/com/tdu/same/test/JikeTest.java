package com.tdu.same.test;


import com.alibaba.fastjson.JSON;
import com.tdu.run.Application;
import com.tdu.spider.biz.service.jike.JikeApiService;
import com.tdu.spider.biz.service.jike.vo.MessageQueryVO;
import com.tdu.spider.model.JkMessageDO;
import com.tdu.spider.model.JkTopicDO;
import com.tdu.spider.model.JkUserInfoDO;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class JikeTest {
    private static final Logger logger = LoggerFactory.getLogger(JikeTest.class);

    @Autowired
    private JikeApiService jikeService;


    @Test
    public void 查询Topic(){
        try {
            List<JkTopicDO> topicVOS = jikeService.getMyTopic(0);
            System.out.print(JSON.toJSONString(topicVOS));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 自动订阅(){
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 查询用户信息(){
        try {
            JkUserInfoDO userInfo = jikeService.getUserInfo();
            System.out.print(JSON.toJSONString(userInfo));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 查询Topic下内容(){
        try {
            MessageQueryVO messageQueryVO=new MessageQueryVO();
            messageQueryVO.setLimit(10);
            messageQueryVO.setTopic("57749312a723ff11003f3fa1");
            //messageQueryVO.setMessageIdLessThan(28489674l);
            File file=new File("/Users/tangdu/git/tangdu/feature/spider/spider-biz/57749312a723ff11003f3fa1.json");
            FileUtils.write(file,"UTF-8","[",true);
            long start=System.currentTimeMillis();
            boolean flag=false;
            while(!flag){
                List<JkMessageDO> messageVOS = jikeService.queryMessage(messageQueryVO);
                if(CollectionUtils.isEmpty(messageVOS)){
                    flag=true;
                    break;
                }else{
                    Thread.sleep(2000);
                    System.out.println("--------execute:"+messageVOS.size());
                    String content=JSON.toJSONString(messageVOS);
                    content=content.substring(1);
                    content=content.substring(0,content.length()-1);
                    FileUtils.write(file,"UTF-8",content,true);
                    messageQueryVO.setMessageIdLessThan(messageVOS.get(messageVOS.size()-1).getMessageId());
                }
            }
            FileUtils.write(file,"UTF-8","]",true);
            long end=System.currentTimeMillis();
            System.out.println("--------time:"+(end-start)/1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void 查询Topic2下内容(){

        try {
            boolean b = jikeService.subscribedTopic("572c6ee2d9595811007a0d67");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void  注册用户(){

        try {
            boolean b = jikeService.createUser("daluobo123","大萝卜的硌","daluobo123_");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
