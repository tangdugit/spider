package com.tdu.same.test;

import com.alibaba.fastjson.JSON;
import com.tdu.run.Application;
import com.tdu.spider.biz.service.common.UserService;
import com.tdu.spider.biz.service.same.SameAnalyseService;
import com.tdu.spider.biz.service.same.SameApiService;
import com.tdu.spider.biz.service.same.SameService;
import com.tdu.spider.biz.service.same.vo.*;
import com.tdu.spider.dao.SameChannelRepository;
import com.tdu.spider.dao.SameSenseRepository;
import com.tdu.spider.model.SameChannelDO;
import com.tdu.spider.model.SameSenseDO;
import com.tdu.spider.model.UserDO;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class SameTest {
    @Autowired
    SameApiService      sameApiService;

    @Autowired
    SameService         sameService;

    @Autowired
    private SameAnalyseService sameAnalyseService;

    @Autowired
    private SameChannelRepository sameChannelRepository;

    @Autowired
    private UserService userService;
    //Token 1512211959-ZaMHUry0T9U6v2RZ-16053307

    @Test
    public void 查询余额(){
        try {
            for (Long aLong : Lists.newArrayList(15752075L,16053307L,15111234L)) {
              System.out.println(aLong+"=对应余额:"+sameApiService.queryAccountBalance(aLong)/100d+"元");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void 查询火热的渠道(){
        try {
            List<SameChannelDO> sameChannelDOS = sameApiService.queryHotChannel();
            System.err.println(JSON.toJSONString(sameChannelDOS));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void 测试渠道查询(){
        try {
            List<SameChannelDO> sameChannelDOS = sameApiService.querySearchChannel("细胞");
            System.err.println(JSON.toJSONString(sameChannelDOS));
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Test
    public void 提现余额(){
        try {
            OrderResultVO orderResultVO = sameApiService.drawCash(15752075L);
            System.err.println(JSON.toJSONString(orderResultVO));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void 测试保存用户信息(){
        try {
            sameService.saveUserInfo(15936403L);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void 测试数据评论(){
        try {
            List<SenseLikeVO> senseLikeVOS = sameApiService.querySenseLikeUsers(77149203L);
            System.out.println(JSON.toJSONString(senseLikeVOS));
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Test
    public void 测试抓取用户文章信息以及评论(){
        try {
            SameAnyQueryVO sameAnyQueryVO=new SameAnyQueryVO();
            sameAnyQueryVO.setUserId(15936403L);
            sameService.spiderUserSenseAndLikes(sameAnyQueryVO);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Test
    public void 测试数据统计(){
        try {
            SameAnalyseQueryVO sameAnyQueryVO=new SameAnalyseQueryVO();
            sameAnyQueryVO.setUserId(15936403L);
            SameAnalyseInfoVO analyseInfoVO = sameAnalyseService.analyseInfoVO(sameAnyQueryVO);
            System.err.println(JSON.toJSONString(analyseInfoVO));
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Test
    public void 抓取数据测试(){
        try {
            SameSenseQueryVO sameSenseQueryVO=new SameSenseQueryVO();
            sameSenseQueryVO.setFilterLike(20);
            sameSenseQueryVO.setFilterPage(2);
//            sameSenseQueryVO.setChannelId(1092473L);
            sameSenseQueryVO.setUserId(15814301L);
//            sameSenseQueryVO.setOffset(58336822L);
            sameService.daliyUpdateDefaultChannelSense(sameSenseQueryVO);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void 插入画手数据() {
        UserDO userDO = new UserDO();
        userDO.setId("same-1512211959");
        userDO.setType("same");
        userDO.setName("13282806286(画手)");
        userDO.setToken("1512211959-ZaMHUry0T9U6v2RZ-16053307");
        userDO.setUserId("16053307");
        userService.put(userDO);
    }
    @Test
    public void 插入测试数据() {
        userService.removeAll();

        UserDO userDO = new UserDO();
        userDO.setId("same-15814301");
        userDO.setType("same");
        userDO.setName("13216609495(备用)");
        userDO.setToken("1512267481-ZsJMZOAZetikRtR8-15814301");
        userDO.setUserId("15814301");
        userService.put(userDO);

        UserDO userDO2 = new UserDO();
        userDO2.setId("same-15752075");
        userDO2.setType("same");
        userDO2.setName("17557292868(女粉)");
        userDO2.setToken("1512264427-AROfHLApdRTBFOWL-15752075");
        userDO2.setUserId("15752075");
        userService.put(userDO2);

        UserDO userDO3 = new UserDO();
        userDO3.setId("same-15111234");
        userDO3.setType("same");
        userDO3.setName("18516280229(专用)");
        userDO3.setToken("1512264669-qvFSAgidFmAjuTMY-15111234");
        userDO3.setUserId("15111234");
        userService.put(userDO3);

        UserDO userDO4 = new UserDO();
        userDO4.setId("same-16053307");
        userDO4.setType("same");
        userDO4.setName("13282806286(画手)");
        userDO4.setToken("1512211959-ZaMHUry0T9U6v2RZ-16053307");
        userDO4.setUserId("16053307");
        userService.put(userDO4);
    }

    /***********************需要登录方法**************************************/
    @Test
    public void 用户登录() {
        UserLoginVO userLoginVO = new UserLoginVO();
        userLoginVO.setDevice("d:d:d:9958172218");
        userLoginVO.setFormat("json");
        userLoginVO.setMobile("17557292868");
        userLoginVO.setPassword("qwert1234");
        //userLoginVO.setPassword("qwert1234");
        //userLoginVO.setMobile("13216609495");
        try {
            UserVO page = sameApiService.login(userLoginVO);
            System.out.println(JSON.toJSONString(page));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 获取用户所有渠道() {
        UserInfoQuery query = new UserInfoQuery();
        query.setUserId(15752075l);
        try {
            List<SameChannelDO> page = sameApiService.channels(query);
            System.out.println(JSON.toJSONString(page));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 查询渠道详情() {
        UserInfoQuery query = new UserInfoQuery();
        query.setChannelId(1244040L);
        query.setUserId(15752075l);
        try {
            SameChannelDO channelDO = sameApiService.queryChannelDetail(query);
            System.out.println(JSON.toJSONString(channelDO));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 用户选择渠道发帖() {
        AddSenSeVO addSenSeVO = new AddSenSeVO();
        addSenSeVO.setUserId(15814301l);
        addSenSeVO.setChannelId(1002620l);
        addSenSeVO.setText("花");
        try {
            SenSeVO senSeVO = sameApiService.createSense(addSenSeVO);
            System.out.println(JSON.toJSONString(senSeVO));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 测试编码() {
        try {
            String contxt = URLEncoder.encode("潘就为了我认为", "UTF-8");
            System.out.println(contxt);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /***********************不需要登录方法**************************************/
    @Test
    public void 根据文章ID查询文章详情() {
        UserInfoQuery query = new UserInfoQuery();
        query.setSenseId(76787000L);
        try {
            SenSeVO page = sameApiService.querySenseDetail(query);
            System.out.println(JSON.toJSONString(page));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 根据文章IDs浏览文章() {
        UserInfoQuery query = new UserInfoQuery();
        query.setUserId(15111234L);
        query.setSenseIds(Lists.newArrayList(77419460L));
        try {
            boolean sense = sameApiService.viewSense(query);
            System.out.println(sense);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 查询用户聊天记录() {
        UserChatHistoryQueryVO query = new UserChatHistoryQueryVO();
        query.setUserId(15111234L);
        query.setLimit(50);
        query.setToUserId(14290083L);
        try {
            List<UserChatHistoryVO> userChatHistoryVOS = sameApiService.privateHistory(query);
            System.out.println(userChatHistoryVOS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 查询我未读消息_不明() {
        UserInfoQuery query = new UserInfoQuery();
        query.setUserId(15752075L);
        try {
             sameApiService.queryUnreadMessage(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 查询我未读消息() {
        UserInfoQuery query = new UserInfoQuery();
        query.setUserId(15111234L);
        try {
            UserMessageResultVO userMessageResultVO = sameApiService.queryMessage(query);
            System.out.println(JSON.toJSONString(userMessageResultVO));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 查询我的波波值排名() {
        UserInfoQuery query = new UserInfoQuery();
        query.setUserId(15111234L);
        try {
            UserBoboResultVO userBoboResultVO = sameApiService.queryBoboRank(query);
            System.out.println(JSON.toJSONString(userBoboResultVO));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void 分页根据渠道获取下文章() {
        UserInfoQuery query = new UserInfoQuery();
        query.setChannelId(1288952l);
        try {
            PageSenseVO page = sameApiService.pageQueryChannelSense(query);
            System.out.println(JSON.toJSONString(page));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 分页根据渠道获取最新动态() {
        UserInfoQuery query = new UserInfoQuery();
        query.setChannelId(1180675l);
        query.setUserId(15814301l);
        try {
            PageSenseVO page = sameApiService.pageQueryActivitySense(query);
            System.out.println(JSON.toJSONString(page));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 上传图片() {
        UploadFileVO uploadFileVO = new UploadFileVO();
        uploadFileVO.setFileName(
            "/Users/tangdu/Downloads/1466C5F5-E1A3-43A4-977B-5930513F9A60__c0_65_1080_1080__w1080_h1440.jpg");
        try {
            UploadFileResult uploadFileResult = sameApiService.uploadFile(uploadFileVO);
            System.out.println(JSON.toJSONString(uploadFileResult));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 下载图片() {
        String url = "https://testcdn.same.com/sense/A548D58B-3D83-47A9-B276-19818661B3C5__c0_-180_1080_1080__w1080_h720.jpg";
        try {
            Response response = Request.Get(url).execute();
            int copy = IOUtils.copy(response.returnContent().asStream(),
                new FileOutputStream(new File("a.jpg")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 下载视频() {
        String url = "http://s.same.com/video/mok_D1105F3C-F288-47E2-AD21-DDAAF96A13FB_10.mp4";
        try {
            Response response = Request.Get(url).execute();
            int copy = IOUtils.copy(response.returnContent().asStream(),
                new FileOutputStream(new File("a.mp4")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 识别图片() {
        try {
            File file = new File(
                "/Users/tangdu/Downloads/1466C5F5-E1A3-43A4-977B-5930513F9A60__c0_65_1080_1080__w1080_h1440.jpg");
            BufferedImage sourceImg = ImageIO.read(new FileInputStream(file));
            System.out.println(String.format("%.1f", file.length() / 1024.0));
            System.out.println(sourceImg.getWidth());
            System.out.println(sourceImg.getHeight());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 测试分隔() {
        String ssss = "2222222__w10990_h2222.jpg";
        String temp = ssss.substring(ssss.lastIndexOf("__") + 2);
        System.out.println(temp);
        String[] whs = temp.substring(0, temp.lastIndexOf(".")).replaceAll("w|h|W|H", "")
            .split("_");
        System.out.println(whs[0] + "," + whs[1]);
    }

    @Test
    public void 测试查询渠道文章信息() {
        UserInfoQuery infoQuery = new UserInfoQuery();
        infoQuery.setChannelId(1003690l);
        infoQuery.setOffset(1451675l);
        try {
            PageSenseVO pageSenseVO = sameApiService.pageQueryChannelSense(infoQuery);
            System.out.println(pageSenseVO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 测试查询渠道文章并且点赞() {
        try {

            List<SameChannelDO> channelInfoVOS = Lists.newArrayList();
            //        channelInfoVOS.add(SameChannelDO.build(967l,"要命的自拍控"));
            //        channelInfoVOS.add(SameChannelDO.build(1000816l,"每天泡在健身房"));
            //        channelInfoVOS.add(SameChannelDO.build(1002393l,"每日搭配"));
            //        channelInfoVOS.add(SameChannelDO.build(1005871l,"人人都有一双大长腿"))
            //        channelInfoVOS.add(SameChannelDO.build(1006617l,"因为拍了照没地方发"));
            channelInfoVOS.add(SameChannelDO.build(1011855l, "足控只是会欣赏美"));
            //        channelInfoVOS.add(SameChannelDO.build(1015326l,"我这么美我不能死"));
            //        channelInfoVOS.add(SameChannelDO.build(1019837l,"起始之源"));
            //        channelInfoVOS.add(SameChannelDO.build(1029212l,"不露脸的照片"));//
            //        channelInfoVOS.add(SameChannelDO.build(1030258l,"大龄samer频道"));
            //         channelInfoVOS.add(SameChannelDO.build(1032823l,"长腿a杯"));
            //        channelInfoVOS.add(SameChannelDO.build(1033563l,"轻性感"));
            //        channelInfoVOS.add(SameChannelDO.build(1125933l,"S.T.S.B.H.Q"));
            //        channelInfoVOS.add(SameChannelDO.build(1166214l,"DALUK"));
            //        channelInfoVOS.add(SameChannelDO.build(1228982l,"贫乳控"));
            //        channelInfoVOS.add(SameChannelDO.build(1371486l,"比太阳还温暖的是你的笑"));
            //        channelInfoVOS.add(SameChannelDO.build(1388511l,"femininity"));
            //        channelInfoVOS.add(SameChannelDO.build(1399676l,"引力"));
            //        channelInfoVOS.add(SameChannelDO.build(1491085l,"Masker"));
            //        channelInfoVOS.add(SameChannelDO.build(1510643l,"高速路上奔驰的小白兔"));
            LikeSenseQueryVO likeSenseQueryVO = new LikeSenseQueryVO();
            likeSenseQueryVO.setUserId(15111234l);
            likeSenseQueryVO.setChannelInfoVOS(channelInfoVOS);
            sameService.likeUpdateTopSense(likeSenseQueryVO);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void ttt() {
        try {
            List<SameChannelDO> channelInfoVOS = Lists.newArrayList();
            channelInfoVOS.add(SameChannelDO.build(1011855l, "足控只是会欣赏美"));
            channelInfoVOS.add(SameChannelDO.build(1032823l, "长腿a杯"));
            channelInfoVOS.add(SameChannelDO.build(1005871l, "人人都有一双大长腿"));
            channelInfoVOS.add(SameChannelDO.build(1491085l, "Masker"));
            channelInfoVOS.add(SameChannelDO.build(1528593l, "E.T.I.T"));
            channelInfoVOS.add(SameChannelDO.build(1015326l, "我这么美我不能死"));
            channelInfoVOS.add(SameChannelDO.build(1491085l, "Masker"));
            LikeSenseQueryVO likeSenseQueryVO = new LikeSenseQueryVO(15111234l, channelInfoVOS);
            sameService.likeUpdateTopSense(likeSenseQueryVO);
        } catch (Exception e) {
            e.printStackTrace();
            ;
        }
    }

    @Test
    public void 测试下载视频() {
        UserInfoQuery userInfoQuery = new UserInfoQuery();
        userInfoQuery.setUserId(15111234l);
        try {
            sameApiService.recommendationChannel(userInfoQuery);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 测试随机接口() {
        SameSenseQueryVO userInfoQuery = new SameSenseQueryVO();
        userInfoQuery.setChannelId(1288952l);
        try {
            sameService.updateSpiderSense(userInfoQuery);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Autowired
    private SameSenseRepository sameSenseRepository;

    @Test
    public void 测试下载批量(){
        for (int i = 73; i < 144; i++) {
            Pageable pageable = PageRequest.of(i, 100);
            Page<SameSenseDO> senseDOS = sameSenseRepository.findAll(pageable);
            System.out.println("RUN->"+i+",RESULT->"+senseDOS.getContent().size());
            try {
                TimeUnit.SECONDS.sleep(1l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (SameSenseDO senseDO : senseDOS.getContent()) {
                try {
                    System.out.println(senseDO.getId()+"="+senseDO.getMedis_src());
                    TimeUnit.MICROSECONDS.sleep(200l);
                    Response response = Request.Get(senseDO.getMedis_src())
                            .addHeader("User-Agent", "NewsClient/2.4.2 (iPhone; iOS 10.3.2; Scale/2.00)")
                            .execute();
                    HttpResponse httpResponse = response.returnResponse();
                    if(httpResponse.getStatusLine().getStatusCode()== HttpStatus.SC_OK){
                        int copy = IOUtils.copy(httpResponse.getEntity().getContent(),
                                new FileOutputStream(new File("vedio/"+senseDO.getId()+".mp4")));
                    }else{
                        System.err.println("ERROR->"+senseDO.getId());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }


    private static final Logger LOGGER = LoggerFactory.getLogger(SameTest.class);
   @Test
    public void 增量抓取(){
       try {
           SameSenseQueryVO sameSenseQueryVO=new SameSenseQueryVO();
           sameSenseQueryVO.setFilterLike(10);
           sameSenseQueryVO.setFilterPage(50);
           sameService.daliyUpdateDefaultChannelSense(sameSenseQueryVO);
       } catch (Exception e) {
           LOGGER.error("Scheduled.增量抓取.Error ",e);
       }
   }
}
