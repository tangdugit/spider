package com.tdu.same.test;

import com.tdu.run.Application;
import com.tdu.spider.biz.service.same.SameApiService;
import com.tdu.spider.model.SameChannelDO;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SameOnLineTest.java, v 0.1 2018年01月26日 下午9:15 tangdu Exp $
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class SameOnLineTest {
    @Autowired
    SameApiService sameApiService;

    @Test
    public void 测试上线人数() {
        List<SameChannelDO> channelInfoVOS = Lists.newArrayList();
        channelInfoVOS.add(SameChannelDO.build(56L, "旅行的意义"));
        channelInfoVOS.add(SameChannelDO.build(590L, "随手画拯救"));
        channelInfoVOS.add(SameChannelDO.build(967L, "要命的自拍控"));
        channelInfoVOS.add(SameChannelDO.build(1000816L, "每天泡在健身房"));
        channelInfoVOS.add(SameChannelDO.build(1001617L, "Instagrammer"));
        channelInfoVOS.add(SameChannelDO.build(1002393L, "每日搭配"));
        channelInfoVOS.add(SameChannelDO.build(1005871L, "人人都有一双大长腿"));
        channelInfoVOS.add(SameChannelDO.build(1006617L, "因为拍了照没地方发"));
        channelInfoVOS.add(SameChannelDO.build(1010938L, "胶片味道"));//
        channelInfoVOS.add(SameChannelDO.build(1011855L, "足控只是会欣赏美"));
        channelInfoVOS.add(SameChannelDO.build(1015326L, "我这么美我不能死"));
        channelInfoVOS.add(SameChannelDO.build(1019837L, "起始之源"));
        channelInfoVOS.add(SameChannelDO.build(1029212L, "不露脸的照片"));//
        channelInfoVOS.add(SameChannelDO.build(1030258L, "大龄samer频道"));
        channelInfoVOS.add(SameChannelDO.build(1032823L, "长腿a杯"));
        channelInfoVOS.add(SameChannelDO.build(1033563L, "轻性感"));
        channelInfoVOS.add(SameChannelDO.build(1056740L, "\uD83D\uDC23每人安利一样东西"));
        channelInfoVOS.add(SameChannelDO.build(1058509L, "漂亮的手（男女都好）"));//
        channelInfoVOS.add(SameChannelDO.build(1125933L, "S.T.S.B.H.Q"));
        channelInfoVOS.add(SameChannelDO.build(1166214L, "DALUK"));
        channelInfoVOS.add(SameChannelDO.build(1228982L, "贫乳控"));
        channelInfoVOS.add(SameChannelDO.build(1371486L, "比太阳还温暖的是你的笑"));
        channelInfoVOS.add(SameChannelDO.build(1388511L, "femininity"));
        channelInfoVOS.add(SameChannelDO.build(1399676L, "引力"));
        channelInfoVOS.add(SameChannelDO.build(1491085L, "Masker"));
        channelInfoVOS.add(SameChannelDO.build(1510643L, "高速路上奔驰的小白兔"));
        channelInfoVOS.add(SameChannelDO.build(1528593L, "E.T.I.T"));
        channelInfoVOS.add(SameChannelDO.build(1054689L, "口红达人"));
        channelInfoVOS.add(SameChannelDO.build(1021852L, "每天为生活拍一张照片"));
        channelInfoVOS.add(SameChannelDO.build(1026578L, "关于自己的20件事"));
        channelInfoVOS.add(SameChannelDO.build(1019963L, "无彩妆会死星人"));
        channelInfoVOS.add(SameChannelDO.build(1052752L, "美甲控"));
        channelInfoVOS.add(SameChannelDO.build(1002974L, "iPhone摄影"));
        channelInfoVOS.add(SameChannelDO.build(1002620L, "杭州，杭州"));
        channelInfoVOS.add(SameChannelDO.build(825L, "喵星人根据地"));
        channelInfoVOS.add(SameChannelDO.build(1301047L, "为你收集世间所有的蓝"));
        channelInfoVOS.add(SameChannelDO.build(1000807L, "做饭吃"));
        channelInfoVOS.add(SameChannelDO.build(1050882L, "看你今天穿什么"));
        channelInfoVOS.add(SameChannelDO.build(48L, "吃吃喝喝"));
        channelInfoVOS.add(SameChannelDO.build(63L, "我爱健身"));
        channelInfoVOS.add(SameChannelDO.build(1154482L, "高颜值情侣"));
        channelInfoVOS.add(SameChannelDO.build(1020862L, "晒局部"));
        channelInfoVOS.add(SameChannelDO.build(1536873L, "睡什么睡起来嗨"));
        channelInfoVOS.add(SameChannelDO.build(1298556L, "表情包快到碗里来"));
        channelInfoVOS.add(SameChannelDO.build(1323020L, "人人都有两条大粗腿"));
        channelInfoVOS.add(SameChannelDO.build(1552875L, "给自己好看的自拍"));
        channelInfoVOS.add(SameChannelDO.build(1551685L, "桃花酒馆"));
        channelInfoVOS.add(SameChannelDO.build(1005030L, "我只推荐我的品味"));
        channelInfoVOS.add(SameChannelDO.build(1268521L, "我是丝袜控"));
        channelInfoVOS.add(SameChannelDO.build(1395910L, "让你无语的聊天回复"));
        channelInfoVOS.add(SameChannelDO.build(1151333L, "这里只有帅哥美女"));
        channelInfoVOS.add(SameChannelDO.build(1290901L, "低糖低脂高蛋白"));
        channelInfoVOS.add(SameChannelDO.build(1403954L, "低糖低脂高蛋白"));
        channelInfoVOS.add(SameChannelDO.build(1534186L, "Dress like a girl"));
        channelInfoVOS.add(SameChannelDO.build(1595645L, "避之不及"));
        channelInfoVOS.add(SameChannelDO.build(1052752L, "美甲控"));
        channelInfoVOS.add(SameChannelDO.build(1107455L, "熬夜狗の聊天室"));
        channelInfoVOS.add(SameChannelDO.build(1319642L, "合照（基友，闺蜜）"));
        channelInfoVOS.add(SameChannelDO.build(1129604L, "晨间少女赏味期"));
        channelInfoVOS.add(SameChannelDO.build(1582329L, "尤"));
        channelInfoVOS.add(SameChannelDO.build(1312480L, "faceu"));
        channelInfoVOS.add(SameChannelDO.build(1518176L, "ᴉɐʍɐʞʎɐʇs"));
        channelInfoVOS.add(SameChannelDO.build(1006613L, "不发朋友圈"));
        channelInfoVOS.add(SameChannelDO.build(1061178L, "长腿当道"));
        channelInfoVOS.add(SameChannelDO.build(1011867L, "一星期情侣"));
        channelInfoVOS.add(SameChannelDO.build(1541503L, "我想弹琴给你听"));
        channelInfoVOS.add(SameChannelDO.build(1194847L, "高颜值单身狗"));
        channelInfoVOS.add(SameChannelDO.build(1516012L, "PU$SY"));
        channelInfoVOS.add(SameChannelDO.build(1476556L, "面具下的另一个你"));
        channelInfoVOS.add(SameChannelDO.build(1067271L, "日常随身装备论"));
        channelInfoVOS.add(SameChannelDO.build(1433568L, "ITEMS OR MIX&MATCH"));
        channelInfoVOS.add(SameChannelDO.build(1304704L, "居住地+星座+身高"));
        channelInfoVOS.add(SameChannelDO.build(1239284L, "颜值爆表集中营"));
        channelInfoVOS.add(SameChannelDO.build(1092473L, "今天长这样子哦"));
        channelInfoVOS.add(SameChannelDO.build(1288952L, "小电影工厂"));
        channelInfoVOS.add(SameChannelDO.build(1544677L, "足控之庭"));
        channelInfoVOS.add(SameChannelDO.build(1284340L, "良辰好景虚设"));
        channelInfoVOS.add(SameChannelDO.build(1180510L, "眼睛美"));
        channelInfoVOS.add(SameChannelDO.build(1016798L, "网购零食大集合"));
        channelInfoVOS.add(SameChannelDO.build(1122536L, "少女日更"));

        for (SameChannelDO channelInfoVO : channelInfoVOS) {
            try {
                Integer integer = sameApiService.queryChannelCount(channelInfoVO.getId());
                System.out.println(integer + "--" +channelInfoVO.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
