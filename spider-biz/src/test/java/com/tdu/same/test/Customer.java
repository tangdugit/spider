//package com.tdu.same.test;
//
//import org.springframework.data.annotation.Id;
//import org.springframework.data.elasticsearch.annotations.Document;
//import org.springframework.data.elasticsearch.annotations.Field;
//import org.springframework.data.elasticsearch.annotations.FieldIndex;
//import org.springframework.data.elasticsearch.annotations.FieldType;
//
//
//@Document(indexName = "test001", type = "cust",indexStoreType = "ik")
//public class Customer {
//    @Id
//    private String  id;
//
//    @Field(type = FieldType.String, index = FieldIndex.analyzed, store = true)
//    private String  name;
//
//    @Field(type = FieldType.Integer, index = FieldIndex.not_analyzed, store = true)
//    private Integer age;
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public Integer getAge() {
//        return age;
//    }
//
//    public void setAge(Integer age) {
//        this.age = age;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    @Override
//    public String toString() {
//        final StringBuffer sb = new StringBuffer("Customer{");
//        sb.append("id='").append(id).append('\'');
//        sb.append(", name='").append(name).append('\'');
//        sb.append(", age=").append(age);
//        sb.append('}');
//        return sb.toString();
//    }
//}
