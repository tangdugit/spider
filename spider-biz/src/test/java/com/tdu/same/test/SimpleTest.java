package com.tdu.same.test;

import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;


public class SimpleTest {

    @Test
    public void est(){
        Date date=new Date(1493552108);
        Date date2=new Date();
        //1493632815260
        //1493552108
        System.out.println(date);
        System.out.println(date2.getTime());
    }

    @Test
    public void jfu(){
        Date d=new Date();
        Calendar localCalendar = Calendar.getInstance();
        localCalendar.setTime(d);
        localCalendar.add(Calendar.DAY_OF_MONTH, -3);
        System.out.println(localCalendar);
    }

    @Test
    public void 测试下载(){
        try {
            Response execute = Request.Get("http://xclient.info/s/doo.html").userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36").execute();
            Document document = Jsoup.parse(execute.returnContent().asString());
            System.out.println(document.html());
            Elements eq = document.select("div[class=history_version] tbody td[class=version_num]");
            System.out.println(eq.html());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 测试下载2(){
        try {
            Response execute = Request.Get("http://xclient.info/s/doo.html?a=dl&v=2.1.0&k=1&_=aa4cc025f3715a2417563edefdffaf369928858e")
                    .addHeader("Referer","http://xclient.info/s/doo.html")
                    .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36").execute();
            Document document = Jsoup.parse(execute.returnContent().asString());
            System.out.println(document.html());
            Elements eq = document.select("div.down_wrap a.btn_down_link");
            System.out.println(eq.attr("data-clipboard-text")+"----"+eq.attr("data-link"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 测试路径(){
        try {
            for (int i = 92112447; i < 101102447; i++) {
                int code = Request.Get("http://www.xiachufang.com/recipe/"+i).execute().returnResponse().getStatusLine().getStatusCode();
                if(code==HttpStatus.SC_OK){
                    System.out.print("sccc");
                }
                System.out.println(code+"--"+i);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
