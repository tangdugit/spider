package com.tdu;

import com.tdu.run.Application;
import com.tdu.spider.biz.service.xiachufang.XiachufangService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: Xiachufang.java, v 0.1 2018年06月11日 上午12:37 tangdu Exp $
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class XiachufangText {

    @Autowired
    private XiachufangService xiachufangService;


    @Test
    public void 测试点赞(){
        try {
            xiachufangService.autoDdigg();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void 测试点赞2(){
        try {
            xiachufangService.autoHotDdigg();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Test
    public void 测试路径(){
        try {
            List<String> list = xiachufangService.queryHotFeeds(1);
            System.out.println(list);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
