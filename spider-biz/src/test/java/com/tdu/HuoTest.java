package com.tdu;

import com.tdu.spider.dao.HuoTopicDORepository;
import com.tdu.spider.model.HuoTopicDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import com.tdu.run.Application;
import com.tdu.spider.biz.service.huocaihe.HuoService;
import com.tdu.spider.biz.service.huocaihe.vo.HuoQueryVO;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class HuoTest {

    @Autowired
    private HuoService huoService;

    @Autowired
    private HuoTopicDORepository huoTopicDORepository;

    @Test
    public void 测试分布(){
        for(int i=0;i<125;i++) {
            Pageable pageable = PageRequest.of(i, 100);
            Page<HuoTopicDO> topicDOPage = huoTopicDORepository.findAll(pageable);
            for (HuoTopicDO huoTopicDO : topicDOPage.getContent()) {
                System.out.println(huoTopicDO.getTopic_name()+"--"+huoTopicDO.getTopic_id());
                if(huoTopicDO.getTopic_id()==179 ){
                    System.out.println(i);
                    return;
                }
            }
        }
    }

    @Test
    public void 抓取数据() {
        HuoQueryVO queryVO = new HuoQueryVO();
        queryVO.setRegister_id("191e35f7e073b77861a");
        queryVO.setUdid("197fd1a08bb6e69a149042291d744d6d019e6319");
        queryVO.setUid("1535698");
        queryVO.setToken_key(
            "MTUzNTY5OCzlsIbmnaXnmoTkuossLDZkYWYzOzlhYzQ0YjIzYWQxYmFiNjE4NDdkMjJhODhkMjdjZmM4");
        try {
            huoService.batchSpiderTopic(queryVO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 保存内容() {
        HuoQueryVO queryVO = new HuoQueryVO();
        queryVO.setRegister_id("191e35f7e073b77861a");
        queryVO.setUdid("197fd1a08bb6e69a149042291d744d6d019e6319");
        queryVO.setUid("1535698");
        queryVO.setToken_key(
                "MTUzNTY5OCzlsIbmnaXnmoTkuossLDZkYWYzOzlhYzQ0YjIzYWQxYmFiNjE4NDdkMjJhODhkMjdjZmM4");
        try {
            huoService.batchSpiderArcitle(queryVO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
