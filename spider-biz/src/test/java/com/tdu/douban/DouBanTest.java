package com.tdu.douban;

import com.tdu.run.Application;
import com.tdu.spider.biz.service.douban.DouMovieService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: DouBanTest.java, v 0.1 2018年04月15日 上午7:41 tangdu Exp $
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class DouBanTest {

    @Autowired
    private DouMovieService movieService;

    @Test
    public void 更新电影250(){
        movieService.updateTop250();
    }
}
