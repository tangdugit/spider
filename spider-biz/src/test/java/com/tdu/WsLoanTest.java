package com.tdu;


import com.tdu.run.Application;
import com.tdu.spider.biz.service.wsloan.WsLoanApiService;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class WsLoanTest {

    @Autowired
    protected WsLoanApiService wsLoanApiService;


    @Test
    public void 测试签到功能(){
        try {
            wsLoanApiService.qiandao("a1GTivN2kFXl9l/Q2h5Fxw==");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 测试抽奖功能(){
        try {
            wsLoanApiService.choujiang("OTUaqkQeIIvvwhoDzqE+6w==");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 测试指(){

        try {
            List<String> userIds= Lists.newArrayList("OTUaqkQeIIvvwhoDzqE+6w==","a1GTivN2kFXl9l/Q2h5Fxw==");
            for (String userId : userIds) {
                wsLoanApiService.qiandao(userId);
                wsLoanApiService.choujiang(userId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
