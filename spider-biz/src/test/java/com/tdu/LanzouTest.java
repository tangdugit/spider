package com.tdu;

import com.alibaba.fastjson.JSONObject;
import com.tdu.spider.biz.util.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.junit.Test;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: LanzouTest.java, v 0.1 2018年04月14日 上午8:57 tangdu Exp $
 */
@Slf4j
public class LanzouTest {


    @Test
    public void 测试() {
        for (int i = 1000000; i < 1087820; i++) {
            try {
                String       url          = "https://pan.lanzou.com/" + i;
                Response     execute      = Request.Get("https://pan.lanzou.com/" + i)
                        .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36").execute();
                HttpResponse httpResponse = execute.returnResponse();
                if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    String string = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
                    if (!string.contains("访问地址错误") && !string.contains("文件取消分享了")) {
                        String href = "https://pan.lanzou.com" + Jsoup.parse(string).select("iframe.ifr2").attr("src");
                        System.out.println("found spider--> " + url);
                        getFile(href,i);
                    }
                } else {
                    System.out.println("not found " + url);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void getFile(String href,Integer id) throws Exception {
        Response     execute      = Request.Get(href)
                .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36").execute();
        HttpResponse httpResponse = execute.returnResponse();
        if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            String string = EntityUtils.toString(httpResponse.getEntity(), "UTF-8").replaceAll("\\s","");
            string = string.substring(0,string.lastIndexOf(";$.ajax"));
            string=string.substring(string.lastIndexOf("=")+1).replaceAll("'","");
            if(StringUtils.isNoneBlank(string)){
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("id",id);
                jsonObject.put("code",string);
                jsonObject.put("action","down_process");
                log.info(jsonObject.toJSONString());
            }
        }
    }

    @Test
    public void 下载文件(){
        try {
            JSONObject jsonObject=JSONObject.parseObject("{\"k\":\"d66986531e472b45263739a17dfd97f8\",\"file_id\":1000006,\"action\":\"down_process\"}");
            jsonObject.put("t",System.currentTimeMillis()/1000);
            StringEntity stringEntity=new StringEntity(jsonObject.toJSONString());
            stringEntity.setContentEncoding("UTF-8");
            stringEntity.setContentType("application/json");
            Request request = Request.Post("https://pan.lanzou.com/ajaxm.php").body(stringEntity).userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36").addHeader("x-requested-with", "XMLHttpRequest").addHeader("authority", "pan.lanzou.com").addHeader("cookie", "UM_distinctid=162b1c6cd2c329-0b543d2119a97c-336b7b05-fa000-162b1c6cd2d648; CNZZDATA1253610885=684148428-1523399719-%7C1523664866; down_ip=1; CNZZDATA5289284=cnzz_eid%3D418310148-1523665194-%26ntime%3D1523665194; CNZZDATA1253610888=153988311-1523397206-%7C1523668702; CNZZDATA1256333716=14814249-1523663641-%7C1523669065; CNZZDATA5288474=cnzz_eid%3D34255730-1523667361-%26ntime%3D1523672761").addHeader("accept", "application/json, text/javascript, */*").addHeader("content-type", "application/x-www-form-urlencoded");
            Response response = Executor.newInstance(HttpUtils.httpClient()).execute(request);
            String   asString = response.returnContent().asString();
            System.out.println("download url " + asString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
