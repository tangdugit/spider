package com.tdu;

import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.message.BasicNameValuePair;
import org.assertj.core.util.Lists;
import org.junit.Test;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: ChuchujieTest.java, v 0.1 2018年01月24日 下午3:41 tangdu Exp $
 */
public class ChuchujieTest {
    @Test
    public void 测试接口(){
        try {
            List<NameValuePair> valuePairs = Lists.newArrayList();
            valuePairs.add(new BasicNameValuePair("shopToken", "ccjd71548325628b37000092340385e91548325628"));
            valuePairs.add(new BasicNameValuePair("platform", "iphone"));
            valuePairs.add(new BasicNameValuePair("version", "3.16.1"));
            valuePairs.add(new BasicNameValuePair("packageName", "com.culiu.huanletao"));
            valuePairs.add(new BasicNameValuePair("imei", "87D99354-FDB7-4EED-A6C6-8AD79EA7AC48"));
            valuePairs.add(new BasicNameValuePair("deviceId", "6936f8a59fc9db3440807bd1452e9203"));
            valuePairs.add(new BasicNameValuePair("tag12", "1"));
            Request request = Request.Post("https://huodong-hb.chuchujie.com/WeChatCoupon/public/pick_api.php")
                    .addHeader("Referer", "https://huodong-hb.chuchujie.com/WeChatCoupon/index.html?source=8")
                    .userAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_2 like Mac OS X) AppleWebKit/604.4.7 (KHTML, like Gecko) Mobile/15C202")
                    .addHeader("Cookie", "CNZZDATA1258985697=189746456-1516787794-%7C1528686408; access_token=ccjd71548325628b37000092340385e91548325628; client_type=ios; client_version=3.16.1; imei=F2474F84-2C20-4727-A0F9-7EC8704BC9F8; package_name=com.culiu.huanletao; UM_distinctid=16127b407d2b4-077b3a11b5d0f1-2e5e056e-2c600-16127b407d32c2").bodyForm();
            Response response = request.bodyForm(valuePairs).execute();
            String result = response.returnContent().asString();
            System.out.println("A抽奖-->"+result);

            valuePairs.clear();
            valuePairs.add(new BasicNameValuePair("formId", "7f158ddb43e0b196c051478068c421d4"));
            request = Request.Post("https://pintuan.chuchujie.com/app.php?c=HuoDong&m=pickBonus&sid=741CEBA923EDE37D4C6F69F15461A79B")
                    .addHeader("Referer", "https://servicewechat.com/wxbe828768f6d149f1/36/page-frame.html")
                    .addHeader("loginType","smallapp")
                    .userAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_2 like Mac OS X) AppleWebKit/604.4.7 (KHTML, like Gecko) Mobile/15C202");
            response = request.bodyForm(valuePairs).execute();
            result = response.returnContent().asString();
            System.out.println("B抽奖-->"+result);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
