package com.tdu;


import org.junit.Test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class QueueTest {


    @Test
    public void 测试ArrayBlockingQueue(){
        BlockingQueue blockingQueue=new ArrayBlockingQueue(1);
        try {
            blockingQueue.put(1);
            Object take = blockingQueue.take();
            System.out.println(take);
            blockingQueue.put(2);
            System.out.println(blockingQueue.take());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void 测试DelayQueue(){

    }

    @Test
    public void 测试LinkedBlockingQueue(){
        BlockingQueue<String> blockingQueue=new LinkedBlockingDeque<String>(100);
    }
}
