package com.tdu.tv;

import com.google.common.collect.Lists;
import com.tdu.run.Application;
import com.tdu.spider.dao.tv.MediaRepository;
import com.tdu.spider.model.tv.MediaDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: DouBanTest.java, v 0.1 2018年04月15日 上午7:41 tangdu Exp $
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class MovieTest {

    @Autowired
    private MediaRepository mediaRepository;

    @Test
    public void 插入电影() {
        MediaDO mediaDO=new MediaDO();
        mediaDO.setBelong("法国");
        mediaDO.setCategoryCode("A");
        mediaDO.setDirection("克里斯丁·杜瓦");
        mediaDO.setCreateTime(new Date());
        mediaDO.setLead("多瑞安·勒·克利奇 / 巴蒂斯特·弗勒里埃尔 / 帕特里克·布鲁尔 / 艾尔莎·泽贝斯坦");
        mediaDO.setTag(Lists.newArrayList("动作"));
        mediaDO.setType(1);
        mediaDO.setId(6);
        mediaDO.setTotalTime(110*60);
        mediaDO.setYear(2017);
        mediaDO.setTitle("一袋弹子");
        mediaDO.setImageUrl(Lists.newArrayList("https://img1.doubanio.com/view/photo/l/public/p2520545228.webp"));
        mediaDO.setVideoUrl("https://100m3u8.duapp.com/definition.html?fid=https://cdn.kuyunbo.club/20180501/nNJo0Uqh/index.m3u8");
        mediaDO.setCover("https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2520545137.webp");
        mediaDO.setContent("影片改编自法国当代著名作家约瑟夫·若福的自传小说《弹子袋》。在被德军占领的法国，莫里斯和乔是一对年轻的犹太兄弟，父亲经营着一家理发店，一家人生活温馨又美满。直到纳粹要求所有犹太人必须在外衣上缝上一个黄色的六角星以示区别，由于当时人们对犹太人的偏见，同学对他们的态度也开始转变。眼见犹太人的处境越发困窘，父亲命令两兄弟立刻先行前往自由区逃难，一家人约定在尼斯汇合。临走前父亲嘱咐不能告诉任何人他们的犹太人身份。在离开的火车上他们才真正亲眼看到了纳粹对于犹太人民的迫害。漫长的逃亡之路，纳粹仿佛无处不在，而他们惊人的机智与勇气以及陌生人的出手相助让他们躲过了一次又一次死亡的威胁，两兄弟相依为命只为与家人再次团聚");
        mediaRepository.save(mediaDO);

    }

    @Test
    public void 插入电影B() {
        MediaDO mediaDO=new MediaDO();
        mediaDO.setBelong("美国");
        mediaDO.setCategoryCode("B");
        mediaDO.setDirection("盖瑞·罗斯");
        mediaDO.setCreateTime(new Date());
        mediaDO.setLead("马克·沃尔伯格/罗素·克劳/凯瑟琳·泽塔-琼斯/杰弗里·怀特/巴里·佩珀/阿隆娜·塔尔/娜塔丽·马丁内兹/詹姆斯·兰索恩");
        mediaDO.setTag(Lists.newArrayList("动作"));
        mediaDO.setType(1);
        mediaDO.setTotalTime(141*60);
        mediaDO.setYear(2016);
        mediaDO.setId(2);
        mediaDO.setTitle("破碎之城");
        mediaDO.setImageUrl(Lists.newArrayList("https://img1.doubanio.com/view/photo/l/public/p1998401898.webp"));
        mediaDO.setVideoUrl("http://api.tianxianle.com/youku/?id=sa51naKhqaajyqqWmah1aGNjd25raKZmaHd8empmZWdudmlo000ofGxneKSpfWcO0O0O");
        mediaDO.setCover("https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1729011590.webp");
        mediaDO.setContent("比利·塔吉特（马克·沃尔伯格 Mark Wahlberg 饰）是一名备受尊敬的纽约警察。然而在一次追捕杀人犯人中致后者死亡，最终审判结果被私下篡改，犯人被定为自卫，而被市民称为“英雄”的塔吉特却被迫离开警局。 \n" + "　　7年后，塔吉特以一名私家侦探的身份重新出现，然而市场并不景气，濒临破产之际，市长霍斯泰特勒（罗素·克劳 Russell Crowe 饰）找到他，让他帮忙查出妻子凯瑟琳（凯瑟琳·泽塔-琼斯 Catherine Zeta-Jones 饰）的情夫。然而事成之后，塔吉特却渐渐发现事件并非表面上那么简单，在此背后隐藏的政权利益、金钱交易和肉体买卖仿佛黑幕般笼罩着纽约这座破碎之城，牵扯其中的甚至还有7年前自己那桩冤案");
        mediaRepository.save(mediaDO);

    }
}
