package com.tdu.tv;

import com.tdu.run.Application;
import com.tdu.spider.dao.tv.CategoryRepository;
import com.tdu.spider.model.tv.CategoryDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: DouBanTest.java, v 0.1 2018年04月15日 上午7:41 tangdu Exp $
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@SpringBootConfiguration
public class DouBanTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void 插入目录() {
        CategoryDO categoryDO = new CategoryDO();
        categoryDO.setCategoryCode("A");
        categoryDO.setCategoryTitle("为你推荐");
        categoryDO.setSno(1);
        categoryRepository.save(categoryDO);

        categoryDO = new CategoryDO();
        categoryDO.setCategoryCode("B");
        categoryDO.setCategoryTitle("好评周榜");
        categoryDO.setSno(2);
        categoryRepository.save(categoryDO);

        categoryDO = new CategoryDO();
        categoryDO.setCategoryCode("C");
        categoryDO.setCategoryTitle("神话奇幻");
        categoryDO.setSno(3);
        categoryRepository.save(categoryDO);

        categoryDO = new CategoryDO();
        categoryDO.setCategoryCode("D");
        categoryDO.setCategoryTitle("推理悬疑");
        categoryDO.setSno(4);
        categoryRepository.save(categoryDO);

        categoryDO = new CategoryDO();
        categoryDO.setCategoryCode("E");
        categoryDO.setCategoryTitle("末日灾难");
        categoryDO.setSno(5);
        categoryRepository.save(categoryDO);

        categoryDO = new CategoryDO();
        categoryDO.setCategoryCode("F");
        categoryDO.setCategoryTitle("系列电影");
        categoryDO.setSno(6);

        categoryDO = new CategoryDO();
        categoryDO.setCategoryCode("G");
        categoryDO.setCategoryTitle("豆瓣精选");
        categoryDO.setSno(7);
        categoryRepository.save(categoryDO);
    }
}
