package com.tdu.spider.biz.service.qutou.vo;

import lombok.Data;

import java.util.List;

@Data
public class QuTouArticleVO {
    private String       id;
    private String       source;
    private String       source_name;
    private String       source_id;
    private String       title;
    private String       type;
    private String       read_count;
    private String       share_count;
    private String       comment_count;
    private String       url;
    private String       is_hot;
    private String       is_top;
    private String       introduction;
    private List<String> tag;
    private List<String> cover;
    private String       cover_show_type;
    private String       share_type;
    private String       can_comment;
    private String       publish_time;
    private String       content_type;
    private Integer      unlike_enable;
    private Integer      show_comment;
    private String       tips;
    private String       font_color;
    private String       font_bold;
    private String       share_url;
    private Integer      iNative;
    private Long         show_time;
    private boolean      is_favorite;
    private Integer      is_wemedia;
    private String       avatar;
    private String       nickname;
    private String       article_num;
    private String       video_num;
    private String       author_id;
    private Integer      is_follow;
    private Integer      has_article;
    private Integer      has_video;
}
