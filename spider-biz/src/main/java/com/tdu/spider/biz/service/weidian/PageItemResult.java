package com.tdu.spider.biz.service.weidian;

import java.util.List;

public class PageItemResult {

    /**
     * status : {"code":0,"message":"OK","description":""}
     * result : {"goodsCardFlood":false,"items":[],"navigations":{"brands":[],"categories":[],"increServices":[{"key":"flag_bin","value":"积分抵扣"}],"intelPrices":[{"key":"4-20","value":"4-20\n39%的选择"},{"key":"20-36","value":"20-36\n36%的选择"},{"key":"36-48","value":"36-48\n4%的选择"}],"point":0,"wdFeatures":[{"key":"free_shipping","value":"包邮"},{"key":"global_buy","value":"全球购"},{"key":"half_price","value":"热促"}],"wdServices":[{"key":"1048576","value":"全球购"},{"key":"time_facet","value":"店家最近登录"}]},"singleCardFlood":false,"sortOptions":[{"order":"desc","sortKey":"default","sortName":"综合"}],"systemTime":1544501624570,"tags":{"navigationTags":["狗狗","手工","透明","日式","营养","大号","diy","夹心","原味","宝宝"],"tagBags":["专用","家用","迷你","儿童","搭配","可爱","补钙","方形"]},"totalCount":8379,"viewDatas":[{"searchSingleItemDO":{"adsk":"shop;2270472;cpc;smart_search;;3339;1","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":true,"adsk":"shop;2270472;cpc;smart_search;;3339;1","buyNum":0,"city":"长沙","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2656662894","itemMainPic":"https://si.geilicdn.com/wdseller256153882-649d000001677d3755630a028841_800_800.jpg","itemName":"宠德基奶酪三明治500g大礼包","itemOriginalPrice":3200,"itemPointLowPrice":0,"itemPrice":3200,"itemSoldout":0,"itemStatus":0,"itemStock":20,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"256153882","spoor":"1008.263.3339.1","tagList":[],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544437355000","lastActiveTitle":"17小时前登录","payedTotal":1,"placeName":"长沙","regularCustomerNum":0,"repurRate":"0","repurRatePercent":"0%","sayingNum":0,"sellerId":"256153882","shopGrade":1,"shopId":"256153882","shopLogo":"https://si.geilicdn.com/vshop256153882-1427457661.jpg?w=250&h=250","shopName":"宠痴的家","shopScore":"0.07306625867127596","shopSigns":"因为你是它的全世界，这里便是全世界宠它最好的选择，宠痴的家\u2026\u2026本店出售所有产品均为正品，假一罚百～实体店发货，开微店只是为了顾客能更方便的购物体验！请大家收藏本店，不定期会有优惠活动哦！","shopTags":[{"likedNum":1,"order":0,"tagId":"56463071","tagName":"热情","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"56502642","tagName":"爱宠达人","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"56464641","tagName":"养宠专家","type":"seller_tag"}],"similarShopIds":"","spoor":"1008.263.3339.1","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"上海","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"2622208084","itemMainPic":"https://si.geilicdn.com/bj-pc-969129514-1540376225905-185983847_800_800.jpg?w=800&h=800","itemName":"顽皮鸡肉三明治","itemOriginalPrice":6500,"itemPointLowPrice":0,"itemPrice":6500,"itemSoldout":0,"itemStatus":0,"itemStock":998,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"969129514","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544501252000","lastActiveTitle":"刚刚登录","payedTotal":189,"placeName":"上海","regularCustomerNum":0,"repurRate":"35.714285714285715","repurRatePercent":"36%","sayingNum":0,"sellerId":"969129514","shopGrade":8,"shopId":"969129514","shopLogo":"https://si.geilicdn.com/bj-vshop-969129514-1529338137269-1521541403_992_992.jpg?w=992&h=992","shopName":"爱宠时光iPetTime","shopScore":"0.24890727095056425","shopSigns":"分享养宠乐趣，提供宠物美食，有趣用品。\u0003每天发货时间为下午16:30分，之后订单第二天发货哦。","shopTags":[{"likedNum":22,"order":0,"tagId":"13646259","tagName":"比瑞吉狗粮","type":"seller_tag"},{"likedNum":8,"order":0,"tagId":"13646262","tagName":"狗粮猫粮","type":"seller_tag"},{"likedNum":8,"order":0,"tagId":"13646261","tagName":"宠物用品","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"北京","familiarCount":2,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":true,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"2227346572","itemMainPic":"https://si.geilicdn.com/bj-vshop-326041224-1515304376679-809634925_3968_2976.jpg?w=2560&h=1920","itemName":"三明治DIY模具3盒10元","itemOriginalPrice":1000,"itemPointLowPrice":0,"itemPrice":1000,"itemSoldout":4,"itemStatus":0,"itemStock":26,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"326041224","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544454472000","lastActiveTitle":"13小时前登录","payedTotal":292,"placeName":"北京","regularCustomerNum":0,"repurRate":"0","repurRatePercent":"0%","sayingNum":0,"sellerId":"326041224","shopGrade":9,"shopId":"326041224","shopLogo":"https://si.geilicdn.com/vshop1428816056781497423.jpg","shopName":"三德味自主烘焙","shopScore":"0.2513049465196724","shopSigns":"微店下单，到店自提有惊喜","shopTags":[{"likedNum":47,"order":0,"tagId":"17455552","tagName":"周到","type":"seller_tag"},{"likedNum":40,"order":0,"tagId":"17455553","tagName":"热情","type":"seller_tag"},{"likedNum":37,"order":0,"tagId":"26234403","tagName":"耐心","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"天津","familiarCount":15,"freeShipping":1,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":true,"freeShipping":true,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"1721757589","itemMainPic":"https://si.geilicdn.com/vshop798371165-1452910200455-99430-s13.jpg?w=1080&h=1920","itemName":"鳕鱼三明治（5袋起包邮）选用优质的鳕鱼皮和鸡胸肉，做出了味道鲜美的三明治，长期吃，增强爱宠的皮肤免疫力，使皮毛更佳","itemOriginalPrice":1000,"itemPointLowPrice":0,"itemPrice":1000,"itemSoldout":67,"itemStatus":0,"itemStock":10071,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"798371165","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10},{"data":"包邮","name":"包邮标","type":20}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544501383000","lastActiveTitle":"刚刚登录","payedTotal":251,"placeName":"天津","regularCustomerNum":0,"repurRate":"44.680851063829785","repurRatePercent":"45%","sayingNum":0,"sellerId":"798371165","shopGrade":10,"shopId":"798371165","shopLogo":"https://si.geilicdn.com/vshop798371165-1488172972.jpg?w=984&h=984","shopName":"天津三美宠物","shopScore":"0.28731063840225235","shopSigns":"本店只要在微店里拍一定量的产品（除了个别产品外），厂家都是包邮！","shopTags":[{"likedNum":79,"order":0,"tagId":"17568242","tagName":"非常有耐心赞赞赞","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"上海","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":true,"tmallShopIcon":false},"itemId":"2011445009","itemMainPic":"https://si.geilicdn.com/vshop259231374-5243026307711460192192-849092.jpg?w=800&h=800","itemName":"【宠荟网】 宠物三明治零食 狗狗零食 三明治方块 250克/罐","itemOriginalPrice":4600,"itemPointLowPrice":0,"itemPrice":4600,"itemSoldout":1,"itemStatus":0,"itemStock":216,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"215072622","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544430529000","lastActiveTitle":"19小时前登录","payedTotal":66,"placeName":"上海","regularCustomerNum":0,"repurRate":"22.22222222222222","repurRatePercent":"22%","sayingNum":0,"sellerId":"215072622","shopGrade":6,"shopId":"215072622","shopLogo":"https://si.geilicdn.com/vshop215072622-1472728412.jpeg?w=250&h=250","shopName":"宠荟网","shopScore":"0.17871780081606153","shopSigns":"用品满80包邮，咨询宠物活体请加店主微信，【宠荟网】微信1423649297","shopTags":[{"likedNum":7,"order":0,"tagId":"22603753","tagName":"售后保障","type":"seller_tag"},{"likedNum":1,"order":0,"tagId":"22603751","tagName":"首席铲屎官","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"广州","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":true,"tmallShopIcon":false},"itemId":"1907361813","itemMainPic":"https://si.geilicdn.com/vshop314753478-5355648808541468839920-370422.jpg?w=767&h=767","itemName":"鸡肉三明治条散装100g狗零食鸡胸肉鸡肉条泰迪幼犬训练宠物零食","itemOriginalPrice":12100,"itemPointLowPrice":0,"itemPrice":12100,"itemSoldout":0,"itemStatus":0,"itemStock":8487,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"314753478","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544495957000","lastActiveTitle":"1小时前登录","payedTotal":78,"placeName":"广州","regularCustomerNum":0,"repurRate":"12.5","repurRatePercent":"12%","sayingNum":0,"sellerId":"314753478","shopGrade":5,"shopId":"314753478","shopLogo":"https://si.geilicdn.com/vshop314753478-1482752665.jpeg?w=250&h=250","shopName":"中华风水联盟","shopScore":"0.1721926499305405","shopSigns":"本店微信号abc1988020","shopTags":[{"likedNum":3,"order":0,"tagId":"51266512","tagName":"产品好用店长服务","type":"seller_tag"},{"likedNum":1,"order":0,"tagId":"22923379","tagName":"肾虚","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"22923380","tagName":"肾宝","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"shop;1905866;cpc;smart_search;;3339;1","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":true,"adsk":"shop;1905866;cpc;smart_search;;3339;1","buyNum":0,"city":"淮南","familiarCount":0,"freeShipping":1,"hasSafeIcon":false,"highPoint":10,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":true,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"2637285248","itemMainPic":"https://si.geilicdn.com/forward812622485-029d00000167087a33050a02853e_800_800.jpg?w=800&h=800","itemName":"麦富迪宠物狗零食（拍10件230）北美原野牛肉鳕鱼三明治天然宠物零食360g","itemOriginalPrice":2900,"itemPointLowPrice":2780,"itemPrice":2900,"itemSoldout":0,"itemStatus":0,"itemStock":50,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"recommendReason":"积分抵1.2元","shopId":"812622485","spoor":"1008.263.3339.7","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10},{"data":"包邮","name":"包邮标","type":20},{"data":"积分抵1.2元","name":"积分抵扣","type":20}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544501394000","lastActiveTitle":"刚刚登录","payedTotal":31,"placeName":"淮南","regularCustomerNum":0,"repurRate":"34.78260869565217","repurRatePercent":"35%","sayingNum":0,"sellerId":"812622485","shopGrade":4,"shopId":"812622485","shopLogo":"https://si.geilicdn.com/bj-wd-812622485-1526891539753-759999668_250_250.jpg?w=250&h=250","shopName":"豆丁宠物用品企业店","shopScore":"0.20830135848765052","shopSigns":"欢迎光临 豆丁宠物用品店，微店，\u0003【店铺公告】本店精选6大品牌宠物用品：\u00031.欧力优系列宠物冻干狗粮，天然运动型狗粮；\u00032.逸诺PH7宠物沐浴露，消毒液；\u00033.贡维宠物狗粮专宠专用狗粮系列；\u00034.爱贝嘉益生菌宠物保健品系列等；\u00035.Andyou狗粮系列；\u00036.麦富迪系列；\u0003都是经过店主犬舍所有狗狗使用过几年的放心品牌，\u0003性价比很高！\u0003所以店主毅然决然的走向了微店平台，目的是想把好的商品推广给全国各地的家长们，让更多的宠物受益。\u0003【发货说明】店主保证会在48小时之内发货，如果遇到店铺活动促销的商品会在活动促销结束之时算起48小时之内发货，发货地为商品区域总代理的总仓库！\u0003【售后服务】\u0003本店已经开通店长实名认证，\u0003交易资金担保，\u00037天退货服务，\u0003买家保证金保障权益，\u0003请放心购买！\u0003购物过程中如有问题请及时联系店主协商解决！\u0003（买家在微店平台其它店铺综合退货率达到30%的客户，请您绕道）\u0003【客服在线时间】7：00\u2014\u201423：00\u0003 【买家福利】收藏店铺送积分，\u0003关注店铺动态抢福利，本店会员卡折上折\u2026\u2026\u0003本店期待为您和您的爱宠提供无微不至的服务！\u0003店主微信   HH13637119008","shopTags":[{"likedNum":60,"order":0,"tagId":"54087153","tagName":"服务态度非常好","type":"seller_tag"},{"likedNum":55,"order":0,"tagId":"53222215","tagName":"物美价廉","type":"seller_tag"},{"likedNum":52,"order":0,"tagId":"53234216","tagName":"正品","type":"seller_tag"}],"similarShopIds":"","spoor":"1008.263.3339.7","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"厦门","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"2614523288","itemMainPic":"https://si.geilicdn.com/bj-vshop-196297041-1539266019253-1784147666_600_600.jpg?w=600&h=600","itemName":"路斯宠物零食泰迪狗狗零食鸡胸肉干鳕鱼片宠物食品可口三明治200g","itemOriginalPrice":2890,"itemPointLowPrice":0,"itemPrice":2890,"itemSoldout":0,"itemStatus":0,"itemStock":350,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"196297041","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544501113000","lastActiveTitle":"刚刚登录","payedTotal":29,"placeName":"厦门","regularCustomerNum":0,"repurRate":"33.33333333333333","repurRatePercent":"33%","sayingNum":0,"sellerId":"196297041","shopGrade":5,"shopId":"196297041","shopLogo":"https://si.geilicdn.com/bj-vshop-196297041-1537021269601-638182772_984_984.jpg?w=984&h=984","shopName":"边牧宠物用品流浪猫狗心爱义卖","shopScore":"0.2128594041778219","shopSigns":"义卖所得款一部分拿来进货，一部分用来支柱流浪狗猫","shopTags":[{"likedNum":32,"order":0,"tagId":"50268187","tagName":"物美价廉","type":"seller_tag"},{"likedNum":30,"order":0,"tagId":"50268186","tagName":"正品","type":"seller_tag"},{"likedNum":29,"order":0,"tagId":"50268190","tagName":"热情","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"北京","familiarCount":1,"freeShipping":0,"hasSafeIcon":false,"highPoint":11,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":true,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"1695917630","itemMainPic":"https://si.geilicdn.com/vshop168847252-1464249610038-99B9C-s1.jpg?w=1080&h=1080","itemName":"黑鼻头狗零食 黑鼻头牛肉紫薯三明治400g/1袋","itemOriginalPrice":3000,"itemPointLowPrice":2730,"itemPrice":3000,"itemSoldout":2,"itemStatus":0,"itemStock":18,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"recommendReason":"积分抵2.7元","shopId":"168847252","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10},{"data":"积分抵2.7元","name":"积分抵扣","type":20}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544488587000","lastActiveTitle":"3小时前登录","payedTotal":190,"placeName":"北京","regularCustomerNum":0,"repurRate":"28.57142857142857","repurRatePercent":"29%","sayingNum":0,"sellerId":"168847252","shopGrade":9,"shopId":"168847252","shopLogo":"https://si.geilicdn.com/vshop168847252-1464598693.jpeg?w=250&h=250","shopName":"北京缘源爱宠宠物店","shopScore":"0.30554427072345114","shopSigns":"实体店铺地址：北京市朝阳区望京中福百货地下一层宠物用品店，营业时间：9:30\u201419:30。店内满百包邮哦！北京市内发货时间为每天5:00以前，北京市外发货时间为每天7:00以前，客服在线时间：7:30-23:00。","shopTags":[{"likedNum":49,"order":0,"tagId":"8128118","tagName":"超级赞","type":"seller_tag"},{"likedNum":44,"order":0,"tagId":"9802937","tagName":"物美價廉","type":"seller_tag"},{"likedNum":30,"order":0,"tagId":"10270987","tagName":"极棒的店值得爱宠拥有","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"上海","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":true,"tmallShopIcon":false},"itemId":"2010159637","itemMainPic":"https://si.geilicdn.com/vshop216110123-5269390746271481508181-781722.jpg?w=360&h=508","itemName":"透明三明治盒带扣三角盒/牛皮纸三明治盒开窗盒西点烘培包装纸盒","itemOriginalPrice":1800,"itemPointLowPrice":0,"itemPrice":1800,"itemSoldout":0,"itemStatus":0,"itemStock":192,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"216110123","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544345689000","lastActiveTitle":"1天前登录","payedTotal":7,"placeName":"上海","regularCustomerNum":0,"repurRate":"33.33333333333333","repurRatePercent":"33%","sayingNum":0,"sellerId":"216110123","shopGrade":4,"shopId":"216110123","shopName":"上海垒诚国际","shopScore":"0.1261028262354654","similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12}]}
     */

    private StatusBean status;
    private ResultBean result;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class StatusBean {
        /**
         * code : 0
         * message : OK
         * description :
         */

        private int code;
        private String message;
        private String description;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public static class ResultBean {
        /**
         * goodsCardFlood : false
         * items : []
         * navigations : {"brands":[],"categories":[],"increServices":[{"key":"flag_bin","value":"积分抵扣"}],"intelPrices":[{"key":"4-20","value":"4-20\n39%的选择"},{"key":"20-36","value":"20-36\n36%的选择"},{"key":"36-48","value":"36-48\n4%的选择"}],"point":0,"wdFeatures":[{"key":"free_shipping","value":"包邮"},{"key":"global_buy","value":"全球购"},{"key":"half_price","value":"热促"}],"wdServices":[{"key":"1048576","value":"全球购"},{"key":"time_facet","value":"店家最近登录"}]}
         * singleCardFlood : false
         * sortOptions : [{"order":"desc","sortKey":"default","sortName":"综合"}]
         * systemTime : 1544501624570
         * tags : {"navigationTags":["狗狗","手工","透明","日式","营养","大号","diy","夹心","原味","宝宝"],"tagBags":["专用","家用","迷你","儿童","搭配","可爱","补钙","方形"]}
         * totalCount : 8379
         * viewDatas : [{"searchSingleItemDO":{"adsk":"shop;2270472;cpc;smart_search;;3339;1","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":true,"adsk":"shop;2270472;cpc;smart_search;;3339;1","buyNum":0,"city":"长沙","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2656662894","itemMainPic":"https://si.geilicdn.com/wdseller256153882-649d000001677d3755630a028841_800_800.jpg","itemName":"宠德基奶酪三明治500g大礼包","itemOriginalPrice":3200,"itemPointLowPrice":0,"itemPrice":3200,"itemSoldout":0,"itemStatus":0,"itemStock":20,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"256153882","spoor":"1008.263.3339.1","tagList":[],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544437355000","lastActiveTitle":"17小时前登录","payedTotal":1,"placeName":"长沙","regularCustomerNum":0,"repurRate":"0","repurRatePercent":"0%","sayingNum":0,"sellerId":"256153882","shopGrade":1,"shopId":"256153882","shopLogo":"https://si.geilicdn.com/vshop256153882-1427457661.jpg?w=250&h=250","shopName":"宠痴的家","shopScore":"0.07306625867127596","shopSigns":"因为你是它的全世界，这里便是全世界宠它最好的选择，宠痴的家\u2026\u2026本店出售所有产品均为正品，假一罚百～实体店发货，开微店只是为了顾客能更方便的购物体验！请大家收藏本店，不定期会有优惠活动哦！","shopTags":[{"likedNum":1,"order":0,"tagId":"56463071","tagName":"热情","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"56502642","tagName":"爱宠达人","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"56464641","tagName":"养宠专家","type":"seller_tag"}],"similarShopIds":"","spoor":"1008.263.3339.1","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"上海","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"2622208084","itemMainPic":"https://si.geilicdn.com/bj-pc-969129514-1540376225905-185983847_800_800.jpg?w=800&h=800","itemName":"顽皮鸡肉三明治","itemOriginalPrice":6500,"itemPointLowPrice":0,"itemPrice":6500,"itemSoldout":0,"itemStatus":0,"itemStock":998,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"969129514","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544501252000","lastActiveTitle":"刚刚登录","payedTotal":189,"placeName":"上海","regularCustomerNum":0,"repurRate":"35.714285714285715","repurRatePercent":"36%","sayingNum":0,"sellerId":"969129514","shopGrade":8,"shopId":"969129514","shopLogo":"https://si.geilicdn.com/bj-vshop-969129514-1529338137269-1521541403_992_992.jpg?w=992&h=992","shopName":"爱宠时光iPetTime","shopScore":"0.24890727095056425","shopSigns":"分享养宠乐趣，提供宠物美食，有趣用品。\u0003每天发货时间为下午16:30分，之后订单第二天发货哦。","shopTags":[{"likedNum":22,"order":0,"tagId":"13646259","tagName":"比瑞吉狗粮","type":"seller_tag"},{"likedNum":8,"order":0,"tagId":"13646262","tagName":"狗粮猫粮","type":"seller_tag"},{"likedNum":8,"order":0,"tagId":"13646261","tagName":"宠物用品","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"北京","familiarCount":2,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":true,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"2227346572","itemMainPic":"https://si.geilicdn.com/bj-vshop-326041224-1515304376679-809634925_3968_2976.jpg?w=2560&h=1920","itemName":"三明治DIY模具3盒10元","itemOriginalPrice":1000,"itemPointLowPrice":0,"itemPrice":1000,"itemSoldout":4,"itemStatus":0,"itemStock":26,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"326041224","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544454472000","lastActiveTitle":"13小时前登录","payedTotal":292,"placeName":"北京","regularCustomerNum":0,"repurRate":"0","repurRatePercent":"0%","sayingNum":0,"sellerId":"326041224","shopGrade":9,"shopId":"326041224","shopLogo":"https://si.geilicdn.com/vshop1428816056781497423.jpg","shopName":"三德味自主烘焙","shopScore":"0.2513049465196724","shopSigns":"微店下单，到店自提有惊喜","shopTags":[{"likedNum":47,"order":0,"tagId":"17455552","tagName":"周到","type":"seller_tag"},{"likedNum":40,"order":0,"tagId":"17455553","tagName":"热情","type":"seller_tag"},{"likedNum":37,"order":0,"tagId":"26234403","tagName":"耐心","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"天津","familiarCount":15,"freeShipping":1,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":true,"freeShipping":true,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"1721757589","itemMainPic":"https://si.geilicdn.com/vshop798371165-1452910200455-99430-s13.jpg?w=1080&h=1920","itemName":"鳕鱼三明治（5袋起包邮）选用优质的鳕鱼皮和鸡胸肉，做出了味道鲜美的三明治，长期吃，增强爱宠的皮肤免疫力，使皮毛更佳","itemOriginalPrice":1000,"itemPointLowPrice":0,"itemPrice":1000,"itemSoldout":67,"itemStatus":0,"itemStock":10071,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"798371165","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10},{"data":"包邮","name":"包邮标","type":20}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544501383000","lastActiveTitle":"刚刚登录","payedTotal":251,"placeName":"天津","regularCustomerNum":0,"repurRate":"44.680851063829785","repurRatePercent":"45%","sayingNum":0,"sellerId":"798371165","shopGrade":10,"shopId":"798371165","shopLogo":"https://si.geilicdn.com/vshop798371165-1488172972.jpg?w=984&h=984","shopName":"天津三美宠物","shopScore":"0.28731063840225235","shopSigns":"本店只要在微店里拍一定量的产品（除了个别产品外），厂家都是包邮！","shopTags":[{"likedNum":79,"order":0,"tagId":"17568242","tagName":"非常有耐心赞赞赞","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"上海","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":true,"tmallShopIcon":false},"itemId":"2011445009","itemMainPic":"https://si.geilicdn.com/vshop259231374-5243026307711460192192-849092.jpg?w=800&h=800","itemName":"【宠荟网】 宠物三明治零食 狗狗零食 三明治方块 250克/罐","itemOriginalPrice":4600,"itemPointLowPrice":0,"itemPrice":4600,"itemSoldout":1,"itemStatus":0,"itemStock":216,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"215072622","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544430529000","lastActiveTitle":"19小时前登录","payedTotal":66,"placeName":"上海","regularCustomerNum":0,"repurRate":"22.22222222222222","repurRatePercent":"22%","sayingNum":0,"sellerId":"215072622","shopGrade":6,"shopId":"215072622","shopLogo":"https://si.geilicdn.com/vshop215072622-1472728412.jpeg?w=250&h=250","shopName":"宠荟网","shopScore":"0.17871780081606153","shopSigns":"用品满80包邮，咨询宠物活体请加店主微信，【宠荟网】微信1423649297","shopTags":[{"likedNum":7,"order":0,"tagId":"22603753","tagName":"售后保障","type":"seller_tag"},{"likedNum":1,"order":0,"tagId":"22603751","tagName":"首席铲屎官","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"广州","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":true,"tmallShopIcon":false},"itemId":"1907361813","itemMainPic":"https://si.geilicdn.com/vshop314753478-5355648808541468839920-370422.jpg?w=767&h=767","itemName":"鸡肉三明治条散装100g狗零食鸡胸肉鸡肉条泰迪幼犬训练宠物零食","itemOriginalPrice":12100,"itemPointLowPrice":0,"itemPrice":12100,"itemSoldout":0,"itemStatus":0,"itemStock":8487,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"314753478","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544495957000","lastActiveTitle":"1小时前登录","payedTotal":78,"placeName":"广州","regularCustomerNum":0,"repurRate":"12.5","repurRatePercent":"12%","sayingNum":0,"sellerId":"314753478","shopGrade":5,"shopId":"314753478","shopLogo":"https://si.geilicdn.com/vshop314753478-1482752665.jpeg?w=250&h=250","shopName":"中华风水联盟","shopScore":"0.1721926499305405","shopSigns":"本店微信号abc1988020","shopTags":[{"likedNum":3,"order":0,"tagId":"51266512","tagName":"产品好用店长服务","type":"seller_tag"},{"likedNum":1,"order":0,"tagId":"22923379","tagName":"肾虚","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"22923380","tagName":"肾宝","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"shop;1905866;cpc;smart_search;;3339;1","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":true,"adsk":"shop;1905866;cpc;smart_search;;3339;1","buyNum":0,"city":"淮南","familiarCount":0,"freeShipping":1,"hasSafeIcon":false,"highPoint":10,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":true,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"2637285248","itemMainPic":"https://si.geilicdn.com/forward812622485-029d00000167087a33050a02853e_800_800.jpg?w=800&h=800","itemName":"麦富迪宠物狗零食（拍10件230）北美原野牛肉鳕鱼三明治天然宠物零食360g","itemOriginalPrice":2900,"itemPointLowPrice":2780,"itemPrice":2900,"itemSoldout":0,"itemStatus":0,"itemStock":50,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"recommendReason":"积分抵1.2元","shopId":"812622485","spoor":"1008.263.3339.7","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10},{"data":"包邮","name":"包邮标","type":20},{"data":"积分抵1.2元","name":"积分抵扣","type":20}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544501394000","lastActiveTitle":"刚刚登录","payedTotal":31,"placeName":"淮南","regularCustomerNum":0,"repurRate":"34.78260869565217","repurRatePercent":"35%","sayingNum":0,"sellerId":"812622485","shopGrade":4,"shopId":"812622485","shopLogo":"https://si.geilicdn.com/bj-wd-812622485-1526891539753-759999668_250_250.jpg?w=250&h=250","shopName":"豆丁宠物用品企业店","shopScore":"0.20830135848765052","shopSigns":"欢迎光临 豆丁宠物用品店，微店，\u0003【店铺公告】本店精选6大品牌宠物用品：\u00031.欧力优系列宠物冻干狗粮，天然运动型狗粮；\u00032.逸诺PH7宠物沐浴露，消毒液；\u00033.贡维宠物狗粮专宠专用狗粮系列；\u00034.爱贝嘉益生菌宠物保健品系列等；\u00035.Andyou狗粮系列；\u00036.麦富迪系列；\u0003都是经过店主犬舍所有狗狗使用过几年的放心品牌，\u0003性价比很高！\u0003所以店主毅然决然的走向了微店平台，目的是想把好的商品推广给全国各地的家长们，让更多的宠物受益。\u0003【发货说明】店主保证会在48小时之内发货，如果遇到店铺活动促销的商品会在活动促销结束之时算起48小时之内发货，发货地为商品区域总代理的总仓库！\u0003【售后服务】\u0003本店已经开通店长实名认证，\u0003交易资金担保，\u00037天退货服务，\u0003买家保证金保障权益，\u0003请放心购买！\u0003购物过程中如有问题请及时联系店主协商解决！\u0003（买家在微店平台其它店铺综合退货率达到30%的客户，请您绕道）\u0003【客服在线时间】7：00\u2014\u201423：00\u0003 【买家福利】收藏店铺送积分，\u0003关注店铺动态抢福利，本店会员卡折上折\u2026\u2026\u0003本店期待为您和您的爱宠提供无微不至的服务！\u0003店主微信   HH13637119008","shopTags":[{"likedNum":60,"order":0,"tagId":"54087153","tagName":"服务态度非常好","type":"seller_tag"},{"likedNum":55,"order":0,"tagId":"53222215","tagName":"物美价廉","type":"seller_tag"},{"likedNum":52,"order":0,"tagId":"53234216","tagName":"正品","type":"seller_tag"}],"similarShopIds":"","spoor":"1008.263.3339.7","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"厦门","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"2614523288","itemMainPic":"https://si.geilicdn.com/bj-vshop-196297041-1539266019253-1784147666_600_600.jpg?w=600&h=600","itemName":"路斯宠物零食泰迪狗狗零食鸡胸肉干鳕鱼片宠物食品可口三明治200g","itemOriginalPrice":2890,"itemPointLowPrice":0,"itemPrice":2890,"itemSoldout":0,"itemStatus":0,"itemStock":350,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"196297041","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544501113000","lastActiveTitle":"刚刚登录","payedTotal":29,"placeName":"厦门","regularCustomerNum":0,"repurRate":"33.33333333333333","repurRatePercent":"33%","sayingNum":0,"sellerId":"196297041","shopGrade":5,"shopId":"196297041","shopLogo":"https://si.geilicdn.com/bj-vshop-196297041-1537021269601-638182772_984_984.jpg?w=984&h=984","shopName":"边牧宠物用品流浪猫狗心爱义卖","shopScore":"0.2128594041778219","shopSigns":"义卖所得款一部分拿来进货，一部分用来支柱流浪狗猫","shopTags":[{"likedNum":32,"order":0,"tagId":"50268187","tagName":"物美价廉","type":"seller_tag"},{"likedNum":30,"order":0,"tagId":"50268186","tagName":"正品","type":"seller_tag"},{"likedNum":29,"order":0,"tagId":"50268190","tagName":"热情","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"北京","familiarCount":1,"freeShipping":0,"hasSafeIcon":false,"highPoint":11,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":true,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":false,"tmallShopIcon":false},"itemId":"1695917630","itemMainPic":"https://si.geilicdn.com/vshop168847252-1464249610038-99B9C-s1.jpg?w=1080&h=1080","itemName":"黑鼻头狗零食 黑鼻头牛肉紫薯三明治400g/1袋","itemOriginalPrice":3000,"itemPointLowPrice":2730,"itemPrice":3000,"itemSoldout":2,"itemStatus":0,"itemStock":18,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"recommendReason":"积分抵2.7元","shopId":"168847252","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10},{"data":"积分抵2.7元","name":"积分抵扣","type":20}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544488587000","lastActiveTitle":"3小时前登录","payedTotal":190,"placeName":"北京","regularCustomerNum":0,"repurRate":"28.57142857142857","repurRatePercent":"29%","sayingNum":0,"sellerId":"168847252","shopGrade":9,"shopId":"168847252","shopLogo":"https://si.geilicdn.com/vshop168847252-1464598693.jpeg?w=250&h=250","shopName":"北京缘源爱宠宠物店","shopScore":"0.30554427072345114","shopSigns":"实体店铺地址：北京市朝阳区望京中福百货地下一层宠物用品店，营业时间：9:30\u201419:30。店内满百包邮哦！北京市内发货时间为每天5:00以前，北京市外发货时间为每天7:00以前，客服在线时间：7:30-23:00。","shopTags":[{"likedNum":49,"order":0,"tagId":"8128118","tagName":"超级赞","type":"seller_tag"},{"likedNum":44,"order":0,"tagId":"9802937","tagName":"物美價廉","type":"seller_tag"},{"likedNum":30,"order":0,"tagId":"10270987","tagName":"极棒的店值得爱宠拥有","type":"seller_tag"}],"similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12},{"searchSingleItemDO":{"adsk":"","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":false,"buyNum":0,"city":"上海","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemIcon":{"brandAuthentication":false,"brandOfficial":false,"enterpriseAuthentication":false,"entityAuthentication":false,"freeShipping":false,"globalBuy":false,"halfPrice":false,"promotion":false,"taobaoShopIcon":true,"tmallShopIcon":false},"itemId":"2010159637","itemMainPic":"https://si.geilicdn.com/vshop216110123-5269390746271481508181-781722.jpg?w=360&h=508","itemName":"透明三明治盒带扣三角盒/牛皮纸三明治盒开窗盒西点烘培包装纸盒","itemOriginalPrice":1800,"itemPointLowPrice":0,"itemPrice":1800,"itemSoldout":0,"itemStatus":0,"itemStock":192,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"216110123","spoor":"1010.101.3352.0","tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544345689000","lastActiveTitle":"1天前登录","payedTotal":7,"placeName":"上海","regularCustomerNum":0,"repurRate":"33.33333333333333","repurRatePercent":"33%","sayingNum":0,"sellerId":"216110123","shopGrade":4,"shopId":"216110123","shopName":"上海垒诚国际","shopScore":"0.1261028262354654","similarShopIds":"","spoor":"1010.101.3352.0","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""},"viewType":12}]
         */

        private boolean goodsCardFlood;
        private NavigationsBean navigations;
        private boolean singleCardFlood;
        private long systemTime;
        private TagsBean tags;
        private int totalCount;
        private List<?> items;
        private List<SortOptionsBean> sortOptions;
        private List<ViewDatasBean> viewDatas;

        public boolean isGoodsCardFlood() {
            return goodsCardFlood;
        }

        public void setGoodsCardFlood(boolean goodsCardFlood) {
            this.goodsCardFlood = goodsCardFlood;
        }

        public NavigationsBean getNavigations() {
            return navigations;
        }

        public void setNavigations(NavigationsBean navigations) {
            this.navigations = navigations;
        }

        public boolean isSingleCardFlood() {
            return singleCardFlood;
        }

        public void setSingleCardFlood(boolean singleCardFlood) {
            this.singleCardFlood = singleCardFlood;
        }

        public long getSystemTime() {
            return systemTime;
        }

        public void setSystemTime(long systemTime) {
            this.systemTime = systemTime;
        }

        public TagsBean getTags() {
            return tags;
        }

        public void setTags(TagsBean tags) {
            this.tags = tags;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public List<?> getItems() {
            return items;
        }

        public void setItems(List<?> items) {
            this.items = items;
        }

        public List<SortOptionsBean> getSortOptions() {
            return sortOptions;
        }

        public void setSortOptions(List<SortOptionsBean> sortOptions) {
            this.sortOptions = sortOptions;
        }

        public List<ViewDatasBean> getViewDatas() {
            return viewDatas;
        }

        public void setViewDatas(List<ViewDatasBean> viewDatas) {
            this.viewDatas = viewDatas;
        }

        public static class NavigationsBean {
            /**
             * brands : []
             * categories : []
             * increServices : [{"key":"flag_bin","value":"积分抵扣"}]
             * intelPrices : [{"key":"4-20","value":"4-20\n39%的选择"},{"key":"20-36","value":"20-36\n36%的选择"},{"key":"36-48","value":"36-48\n4%的选择"}]
             * point : 0
             * wdFeatures : [{"key":"free_shipping","value":"包邮"},{"key":"global_buy","value":"全球购"},{"key":"half_price","value":"热促"}]
             * wdServices : [{"key":"1048576","value":"全球购"},{"key":"time_facet","value":"店家最近登录"}]
             */

            private int point;
            private List<?> brands;
            private List<?> categories;
            private List<IncreServicesBean> increServices;
            private List<IntelPricesBean> intelPrices;
            private List<WdFeaturesBean> wdFeatures;
            private List<WdServicesBean> wdServices;

            public int getPoint() {
                return point;
            }

            public void setPoint(int point) {
                this.point = point;
            }

            public List<?> getBrands() {
                return brands;
            }

            public void setBrands(List<?> brands) {
                this.brands = brands;
            }

            public List<?> getCategories() {
                return categories;
            }

            public void setCategories(List<?> categories) {
                this.categories = categories;
            }

            public List<IncreServicesBean> getIncreServices() {
                return increServices;
            }

            public void setIncreServices(List<IncreServicesBean> increServices) {
                this.increServices = increServices;
            }

            public List<IntelPricesBean> getIntelPrices() {
                return intelPrices;
            }

            public void setIntelPrices(List<IntelPricesBean> intelPrices) {
                this.intelPrices = intelPrices;
            }

            public List<WdFeaturesBean> getWdFeatures() {
                return wdFeatures;
            }

            public void setWdFeatures(List<WdFeaturesBean> wdFeatures) {
                this.wdFeatures = wdFeatures;
            }

            public List<WdServicesBean> getWdServices() {
                return wdServices;
            }

            public void setWdServices(List<WdServicesBean> wdServices) {
                this.wdServices = wdServices;
            }

            public static class IncreServicesBean {
                /**
                 * key : flag_bin
                 * value : 积分抵扣
                 */

                private String key;
                private String value;

                public String getKey() {
                    return key;
                }

                public void setKey(String key) {
                    this.key = key;
                }

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }
            }

            public static class IntelPricesBean {
                /**
                 * key : 4-20
                 * value : 4-20
                 39%的选择
                 */

                private String key;
                private String value;

                public String getKey() {
                    return key;
                }

                public void setKey(String key) {
                    this.key = key;
                }

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }
            }

            public static class WdFeaturesBean {
                /**
                 * key : free_shipping
                 * value : 包邮
                 */

                private String key;
                private String value;

                public String getKey() {
                    return key;
                }

                public void setKey(String key) {
                    this.key = key;
                }

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }
            }

            public static class WdServicesBean {
                /**
                 * key : 1048576
                 * value : 全球购
                 */

                private String key;
                private String value;

                public String getKey() {
                    return key;
                }

                public void setKey(String key) {
                    this.key = key;
                }

                public String getValue() {
                    return value;
                }

                public void setValue(String value) {
                    this.value = value;
                }
            }
        }

        public static class TagsBean {
            private List<String> navigationTags;
            private List<String> tagBags;

            public List<String> getNavigationTags() {
                return navigationTags;
            }

            public void setNavigationTags(List<String> navigationTags) {
                this.navigationTags = navigationTags;
            }

            public List<String> getTagBags() {
                return tagBags;
            }

            public void setTagBags(List<String> tagBags) {
                this.tagBags = tagBags;
            }
        }

        public static class SortOptionsBean {
            /**
             * order : desc
             * sortKey : default
             * sortName : 综合
             */

            private String order;
            private String sortKey;
            private String sortName;

            public String getOrder() {
                return order;
            }

            public void setOrder(String order) {
                this.order = order;
            }

            public String getSortKey() {
                return sortKey;
            }

            public void setSortKey(String sortKey) {
                this.sortKey = sortKey;
            }

            public String getSortName() {
                return sortName;
            }

            public void setSortName(String sortName) {
                this.sortName = sortName;
            }
        }

        public static class ViewDatasBean {
            /**
             * searchSingleItemDO : {"adsk":"shop;2270472;cpc;smart_search;;3339;1","buyNum":0,"collectCount":0,"collected":false,"cpsIcon":"","distanceToShop":210000000,"fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"item":{"adIcon":0,"adShow":true,"adsk":"shop;2270472;cpc;smart_search;;3339;1","buyNum":0,"city":"长沙","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2656662894","itemMainPic":"https://si.geilicdn.com/wdseller256153882-649d000001677d3755630a028841_800_800.jpg","itemName":"宠德基奶酪三明治500g大礼包","itemOriginalPrice":3200,"itemPointLowPrice":0,"itemPrice":3200,"itemSoldout":0,"itemStatus":0,"itemStock":20,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"256153882","spoor":"1008.263.3339.1","tagList":[],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"lastActiveTime":"1544437355000","lastActiveTitle":"17小时前登录","payedTotal":1,"placeName":"长沙","regularCustomerNum":0,"repurRate":"0","repurRatePercent":"0%","sayingNum":0,"sellerId":"256153882","shopGrade":1,"shopId":"256153882","shopLogo":"https://si.geilicdn.com/vshop256153882-1427457661.jpg?w=250&h=250","shopName":"宠痴的家","shopScore":"0.07306625867127596","shopSigns":"因为你是它的全世界，这里便是全世界宠它最好的选择，宠痴的家\u2026\u2026本店出售所有产品均为正品，假一罚百～实体店发货，开微店只是为了顾客能更方便的购物体验！请大家收藏本店，不定期会有优惠活动哦！","shopTags":[{"likedNum":1,"order":0,"tagId":"56463071","tagName":"热情","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"56502642","tagName":"爱宠达人","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"56464641","tagName":"养宠专家","type":"seller_tag"}],"similarShopIds":"","spoor":"1008.263.3339.1","status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"userActionTag":""}
             * viewType : 12
             */

            private SearchSingleItemDOBean searchSingleItemDO;
            private int viewType;

            public SearchSingleItemDOBean getSearchSingleItemDO() {
                return searchSingleItemDO;
            }

            public void setSearchSingleItemDO(SearchSingleItemDOBean searchSingleItemDO) {
                this.searchSingleItemDO = searchSingleItemDO;
            }

            public int getViewType() {
                return viewType;
            }

            public void setViewType(int viewType) {
                this.viewType = viewType;
            }

            public static class SearchSingleItemDOBean {
                /**
                 * adsk : shop;2270472;cpc;smart_search;;3339;1
                 * buyNum : 0
                 * collectCount : 0
                 * collected : false
                 * cpsIcon :
                 * distanceToShop : 210000000
                 * fx : false
                 * groupNum : 0
                 * isBaking : 0
                 * isCraftsman : 0
                 * isFarmer : 0
                 * isGlobalShop : 0
                 * isKitchen : 0
                 * item : {"adIcon":0,"adShow":true,"adsk":"shop;2270472;cpc;smart_search;;3339;1","buyNum":0,"city":"长沙","familiarCount":0,"freeShipping":0,"hasSafeIcon":false,"highPoint":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2656662894","itemMainPic":"https://si.geilicdn.com/wdseller256153882-649d000001677d3755630a028841_800_800.jpg","itemName":"宠德基奶酪三明治500g大礼包","itemOriginalPrice":3200,"itemPointLowPrice":0,"itemPrice":3200,"itemSoldout":0,"itemStatus":0,"itemStock":20,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"256153882","spoor":"1008.263.3339.1","tagList":[],"taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}
                 * lastActiveTime : 1544437355000
                 * lastActiveTitle : 17小时前登录
                 * payedTotal : 1
                 * placeName : 长沙
                 * regularCustomerNum : 0
                 * repurRate : 0
                 * repurRatePercent : 0%
                 * sayingNum : 0
                 * sellerId : 256153882
                 * shopGrade : 1
                 * shopId : 256153882
                 * shopLogo : https://si.geilicdn.com/vshop256153882-1427457661.jpg?w=250&h=250
                 * shopName : 宠痴的家
                 * shopScore : 0.07306625867127596
                 * shopSigns : 因为你是它的全世界，这里便是全世界宠它最好的选择，宠痴的家……本店出售所有产品均为正品，假一罚百～实体店发货，开微店只是为了顾客能更方便的购物体验！请大家收藏本店，不定期会有优惠活动哦！
                 * shopTags : [{"likedNum":1,"order":0,"tagId":"56463071","tagName":"热情","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"56502642","tagName":"爱宠达人","type":"seller_tag"},{"likedNum":0,"order":0,"tagId":"56464641","tagName":"养宠专家","type":"seller_tag"}]
                 * similarShopIds :
                 * spoor : 1008.263.3339.1
                 * status : 0
                 * taobaoShopIcon : false
                 * tmallShopIcon : false
                 * userActionTag :
                 */

                private String adsk;
                private int buyNum;
                private int collectCount;
                private boolean collected;
                private String cpsIcon;
                private int distanceToShop;
                private boolean fx;
                private int groupNum;
                private int isBaking;
                private int isCraftsman;
                private int isFarmer;
                private int isGlobalShop;
                private int isKitchen;
                private ItemBean item;
                private String lastActiveTime;
                private String lastActiveTitle;
                private int payedTotal;
                private String placeName;
                private int regularCustomerNum;
                private String repurRate;
                private String repurRatePercent;
                private int sayingNum;
                private String sellerId;
                private int shopGrade;
                private String shopId;
                private String shopLogo;
                private String shopName;
                private String shopScore;
                private String shopSigns;
                private String similarShopIds;
                private String spoor;
                private int status;
                private boolean taobaoShopIcon;
                private boolean tmallShopIcon;
                private String userActionTag;
                private List<ShopTagsBean> shopTags;

                public String getAdsk() {
                    return adsk;
                }

                public void setAdsk(String adsk) {
                    this.adsk = adsk;
                }

                public int getBuyNum() {
                    return buyNum;
                }

                public void setBuyNum(int buyNum) {
                    this.buyNum = buyNum;
                }

                public int getCollectCount() {
                    return collectCount;
                }

                public void setCollectCount(int collectCount) {
                    this.collectCount = collectCount;
                }

                public boolean isCollected() {
                    return collected;
                }

                public void setCollected(boolean collected) {
                    this.collected = collected;
                }

                public String getCpsIcon() {
                    return cpsIcon;
                }

                public void setCpsIcon(String cpsIcon) {
                    this.cpsIcon = cpsIcon;
                }

                public int getDistanceToShop() {
                    return distanceToShop;
                }

                public void setDistanceToShop(int distanceToShop) {
                    this.distanceToShop = distanceToShop;
                }

                public boolean isFx() {
                    return fx;
                }

                public void setFx(boolean fx) {
                    this.fx = fx;
                }

                public int getGroupNum() {
                    return groupNum;
                }

                public void setGroupNum(int groupNum) {
                    this.groupNum = groupNum;
                }

                public int getIsBaking() {
                    return isBaking;
                }

                public void setIsBaking(int isBaking) {
                    this.isBaking = isBaking;
                }

                public int getIsCraftsman() {
                    return isCraftsman;
                }

                public void setIsCraftsman(int isCraftsman) {
                    this.isCraftsman = isCraftsman;
                }

                public int getIsFarmer() {
                    return isFarmer;
                }

                public void setIsFarmer(int isFarmer) {
                    this.isFarmer = isFarmer;
                }

                public int getIsGlobalShop() {
                    return isGlobalShop;
                }

                public void setIsGlobalShop(int isGlobalShop) {
                    this.isGlobalShop = isGlobalShop;
                }

                public int getIsKitchen() {
                    return isKitchen;
                }

                public void setIsKitchen(int isKitchen) {
                    this.isKitchen = isKitchen;
                }

                public ItemBean getItem() {
                    return item;
                }

                public void setItem(ItemBean item) {
                    this.item = item;
                }

                public String getLastActiveTime() {
                    return lastActiveTime;
                }

                public void setLastActiveTime(String lastActiveTime) {
                    this.lastActiveTime = lastActiveTime;
                }

                public String getLastActiveTitle() {
                    return lastActiveTitle;
                }

                public void setLastActiveTitle(String lastActiveTitle) {
                    this.lastActiveTitle = lastActiveTitle;
                }

                public int getPayedTotal() {
                    return payedTotal;
                }

                public void setPayedTotal(int payedTotal) {
                    this.payedTotal = payedTotal;
                }

                public String getPlaceName() {
                    return placeName;
                }

                public void setPlaceName(String placeName) {
                    this.placeName = placeName;
                }

                public int getRegularCustomerNum() {
                    return regularCustomerNum;
                }

                public void setRegularCustomerNum(int regularCustomerNum) {
                    this.regularCustomerNum = regularCustomerNum;
                }

                public String getRepurRate() {
                    return repurRate;
                }

                public void setRepurRate(String repurRate) {
                    this.repurRate = repurRate;
                }

                public String getRepurRatePercent() {
                    return repurRatePercent;
                }

                public void setRepurRatePercent(String repurRatePercent) {
                    this.repurRatePercent = repurRatePercent;
                }

                public int getSayingNum() {
                    return sayingNum;
                }

                public void setSayingNum(int sayingNum) {
                    this.sayingNum = sayingNum;
                }

                public String getSellerId() {
                    return sellerId;
                }

                public void setSellerId(String sellerId) {
                    this.sellerId = sellerId;
                }

                public int getShopGrade() {
                    return shopGrade;
                }

                public void setShopGrade(int shopGrade) {
                    this.shopGrade = shopGrade;
                }

                public String getShopId() {
                    return shopId;
                }

                public void setShopId(String shopId) {
                    this.shopId = shopId;
                }

                public String getShopLogo() {
                    return shopLogo;
                }

                public void setShopLogo(String shopLogo) {
                    this.shopLogo = shopLogo;
                }

                public String getShopName() {
                    return shopName;
                }

                public void setShopName(String shopName) {
                    this.shopName = shopName;
                }

                public String getShopScore() {
                    return shopScore;
                }

                public void setShopScore(String shopScore) {
                    this.shopScore = shopScore;
                }

                public String getShopSigns() {
                    return shopSigns;
                }

                public void setShopSigns(String shopSigns) {
                    this.shopSigns = shopSigns;
                }

                public String getSimilarShopIds() {
                    return similarShopIds;
                }

                public void setSimilarShopIds(String similarShopIds) {
                    this.similarShopIds = similarShopIds;
                }

                public String getSpoor() {
                    return spoor;
                }

                public void setSpoor(String spoor) {
                    this.spoor = spoor;
                }

                public int getStatus() {
                    return status;
                }

                public void setStatus(int status) {
                    this.status = status;
                }

                public boolean isTaobaoShopIcon() {
                    return taobaoShopIcon;
                }

                public void setTaobaoShopIcon(boolean taobaoShopIcon) {
                    this.taobaoShopIcon = taobaoShopIcon;
                }

                public boolean isTmallShopIcon() {
                    return tmallShopIcon;
                }

                public void setTmallShopIcon(boolean tmallShopIcon) {
                    this.tmallShopIcon = tmallShopIcon;
                }

                public String getUserActionTag() {
                    return userActionTag;
                }

                public void setUserActionTag(String userActionTag) {
                    this.userActionTag = userActionTag;
                }

                public List<ShopTagsBean> getShopTags() {
                    return shopTags;
                }

                public void setShopTags(List<ShopTagsBean> shopTags) {
                    this.shopTags = shopTags;
                }

                public static class ItemBean {
                    /**
                     * adIcon : 0
                     * adShow : true
                     * adsk : shop;2270472;cpc;smart_search;;3339;1
                     * buyNum : 0
                     * city : 长沙
                     * familiarCount : 0
                     * freeShipping : 0
                     * hasSafeIcon : false
                     * highPoint : 0
                     * isFx : false
                     * isPointPrice : 0
                     * itemDiscount : 0
                     * itemId : 2656662894
                     * itemMainPic : https://si.geilicdn.com/wdseller256153882-649d000001677d3755630a028841_800_800.jpg
                     * itemName : 宠德基奶酪三明治500g大礼包
                     * itemOriginalPrice : 3200
                     * itemPointLowPrice : 0
                     * itemPrice : 3200
                     * itemSoldout : 0
                     * itemStatus : 0
                     * itemStock : 20
                     * itemType : 1
                     * mcSafeSpu : false
                     * newItem : false
                     * pointItem : false
                     * shopId : 256153882
                     * spoor : 1008.263.3339.1
                     * tagList : []
                     * taobaoShopIcon : false
                     * time : 0
                     * timingSold : false
                     * timingSoldDuration : 0
                     * tmallShopIcon : false
                     */

                    private int adIcon;
                    private boolean adShow;
                    private String adsk;
                    private int buyNum;
                    private String city;
                    private int familiarCount;
                    private int freeShipping;
                    private boolean hasSafeIcon;
                    private int highPoint;
                    private boolean isFx;
                    private int isPointPrice;
                    private int itemDiscount;
                    private String itemId;
                    private String itemMainPic;
                    private String itemName;
                    private int itemOriginalPrice;
                    private int itemPointLowPrice;
                    private int itemPrice;
                    private int itemSoldout;
                    private int itemStatus;
                    private int itemStock;
                    private int itemType;
                    private boolean mcSafeSpu;
                    private boolean newItem;
                    private boolean pointItem;
                    private String shopId;
                    private String spoor;
                    private boolean taobaoShopIcon;
                    private int time;
                    private boolean timingSold;
                    private int timingSoldDuration;
                    private boolean tmallShopIcon;
                    private List<?> tagList;

                    public int getAdIcon() {
                        return adIcon;
                    }

                    public void setAdIcon(int adIcon) {
                        this.adIcon = adIcon;
                    }

                    public boolean isAdShow() {
                        return adShow;
                    }

                    public void setAdShow(boolean adShow) {
                        this.adShow = adShow;
                    }

                    public String getAdsk() {
                        return adsk;
                    }

                    public void setAdsk(String adsk) {
                        this.adsk = adsk;
                    }

                    public int getBuyNum() {
                        return buyNum;
                    }

                    public void setBuyNum(int buyNum) {
                        this.buyNum = buyNum;
                    }

                    public String getCity() {
                        return city;
                    }

                    public void setCity(String city) {
                        this.city = city;
                    }

                    public int getFamiliarCount() {
                        return familiarCount;
                    }

                    public void setFamiliarCount(int familiarCount) {
                        this.familiarCount = familiarCount;
                    }

                    public int getFreeShipping() {
                        return freeShipping;
                    }

                    public void setFreeShipping(int freeShipping) {
                        this.freeShipping = freeShipping;
                    }

                    public boolean isHasSafeIcon() {
                        return hasSafeIcon;
                    }

                    public void setHasSafeIcon(boolean hasSafeIcon) {
                        this.hasSafeIcon = hasSafeIcon;
                    }

                    public int getHighPoint() {
                        return highPoint;
                    }

                    public void setHighPoint(int highPoint) {
                        this.highPoint = highPoint;
                    }

                    public boolean isIsFx() {
                        return isFx;
                    }

                    public void setIsFx(boolean isFx) {
                        this.isFx = isFx;
                    }

                    public int getIsPointPrice() {
                        return isPointPrice;
                    }

                    public void setIsPointPrice(int isPointPrice) {
                        this.isPointPrice = isPointPrice;
                    }

                    public int getItemDiscount() {
                        return itemDiscount;
                    }

                    public void setItemDiscount(int itemDiscount) {
                        this.itemDiscount = itemDiscount;
                    }

                    public String getItemId() {
                        return itemId;
                    }

                    public void setItemId(String itemId) {
                        this.itemId = itemId;
                    }

                    public String getItemMainPic() {
                        return itemMainPic;
                    }

                    public void setItemMainPic(String itemMainPic) {
                        this.itemMainPic = itemMainPic;
                    }

                    public String getItemName() {
                        return itemName;
                    }

                    public void setItemName(String itemName) {
                        this.itemName = itemName;
                    }

                    public int getItemOriginalPrice() {
                        return itemOriginalPrice;
                    }

                    public void setItemOriginalPrice(int itemOriginalPrice) {
                        this.itemOriginalPrice = itemOriginalPrice;
                    }

                    public int getItemPointLowPrice() {
                        return itemPointLowPrice;
                    }

                    public void setItemPointLowPrice(int itemPointLowPrice) {
                        this.itemPointLowPrice = itemPointLowPrice;
                    }

                    public int getItemPrice() {
                        return itemPrice;
                    }

                    public void setItemPrice(int itemPrice) {
                        this.itemPrice = itemPrice;
                    }

                    public int getItemSoldout() {
                        return itemSoldout;
                    }

                    public void setItemSoldout(int itemSoldout) {
                        this.itemSoldout = itemSoldout;
                    }

                    public int getItemStatus() {
                        return itemStatus;
                    }

                    public void setItemStatus(int itemStatus) {
                        this.itemStatus = itemStatus;
                    }

                    public int getItemStock() {
                        return itemStock;
                    }

                    public void setItemStock(int itemStock) {
                        this.itemStock = itemStock;
                    }

                    public int getItemType() {
                        return itemType;
                    }

                    public void setItemType(int itemType) {
                        this.itemType = itemType;
                    }

                    public boolean isMcSafeSpu() {
                        return mcSafeSpu;
                    }

                    public void setMcSafeSpu(boolean mcSafeSpu) {
                        this.mcSafeSpu = mcSafeSpu;
                    }

                    public boolean isNewItem() {
                        return newItem;
                    }

                    public void setNewItem(boolean newItem) {
                        this.newItem = newItem;
                    }

                    public boolean isPointItem() {
                        return pointItem;
                    }

                    public void setPointItem(boolean pointItem) {
                        this.pointItem = pointItem;
                    }

                    public String getShopId() {
                        return shopId;
                    }

                    public void setShopId(String shopId) {
                        this.shopId = shopId;
                    }

                    public String getSpoor() {
                        return spoor;
                    }

                    public void setSpoor(String spoor) {
                        this.spoor = spoor;
                    }

                    public boolean isTaobaoShopIcon() {
                        return taobaoShopIcon;
                    }

                    public void setTaobaoShopIcon(boolean taobaoShopIcon) {
                        this.taobaoShopIcon = taobaoShopIcon;
                    }

                    public int getTime() {
                        return time;
                    }

                    public void setTime(int time) {
                        this.time = time;
                    }

                    public boolean isTimingSold() {
                        return timingSold;
                    }

                    public void setTimingSold(boolean timingSold) {
                        this.timingSold = timingSold;
                    }

                    public int getTimingSoldDuration() {
                        return timingSoldDuration;
                    }

                    public void setTimingSoldDuration(int timingSoldDuration) {
                        this.timingSoldDuration = timingSoldDuration;
                    }

                    public boolean isTmallShopIcon() {
                        return tmallShopIcon;
                    }

                    public void setTmallShopIcon(boolean tmallShopIcon) {
                        this.tmallShopIcon = tmallShopIcon;
                    }

                    public List<?> getTagList() {
                        return tagList;
                    }

                    public void setTagList(List<?> tagList) {
                        this.tagList = tagList;
                    }
                }

                public static class ShopTagsBean {
                    /**
                     * likedNum : 1
                     * order : 0
                     * tagId : 56463071
                     * tagName : 热情
                     * type : seller_tag
                     */

                    private int likedNum;
                    private int order;
                    private String tagId;
                    private String tagName;
                    private String type;

                    public int getLikedNum() {
                        return likedNum;
                    }

                    public void setLikedNum(int likedNum) {
                        this.likedNum = likedNum;
                    }

                    public int getOrder() {
                        return order;
                    }

                    public void setOrder(int order) {
                        this.order = order;
                    }

                    public String getTagId() {
                        return tagId;
                    }

                    public void setTagId(String tagId) {
                        this.tagId = tagId;
                    }

                    public String getTagName() {
                        return tagName;
                    }

                    public void setTagName(String tagName) {
                        this.tagName = tagName;
                    }

                    public String getType() {
                        return type;
                    }

                    public void setType(String type) {
                        this.type = type;
                    }
                }
            }
        }
    }
}
