package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SoundVO.java, v 0.1 2018年02月19日 下午7:59 tangdu Exp $
 */
@Setter
@Getter
public class SoundVO {
    private Long id;
    private String src;
    private Long created_at;
    private WaveformVO waveform;
    private SoundMetaVO meta;


    @Setter
    @Getter
    public static class WaveformVO{
        private String samples;
    }

    @Setter
    @Getter
    public static class SoundMetaVO{
        private Double sample_rate;
        private Double duration;
        private Double record_dt;
    }
}
