package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: UserChatHistoryVO.java, v 0.1 2018年02月18日 上午7:05 tangdu Exp $
 */
@Setter
@Getter
public class UserChatHistoryVO {
    private String               fuid;
    private String               tuid;
    private String               mid;
    private Long                 created_at;
    private String               to_uid;
    private String               read;
    private String               sender_name;
    private UserChatHistoryMedia media;
    private String               catalogId;
    private String               is_active;
    private Long                 time;
    private String               catalog_id;
    private Integer              op;
    private Integer              group;
    private Integer              type;


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
