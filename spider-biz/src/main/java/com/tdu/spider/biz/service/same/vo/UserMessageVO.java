package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: UserMessageVO.java, v 0.1 2018年02月18日 下午7:24 tangdu Exp $
 */
@Setter
@Getter
public class UserMessageVO {

    private Integer           badge;
    private String            id;
    private String            type;
    private UserVO            user;
    private UserMessageInfoVO msg;

    @Setter
    @Getter
    public static class UserMessageInfoVO {
        private String               mid;
        private Long                 tuid;
        private Integer              is_active;
        private Integer              type;
        private Long                 time;
        private Long                 fuid;
        private Long                 created_at;
        private String               sender_name;
        private UserChatHistoryMedia media;
        private Integer              group;

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
        }
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
