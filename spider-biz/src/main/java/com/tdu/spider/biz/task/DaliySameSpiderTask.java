package com.tdu.spider.biz.task;

import com.tdu.spider.biz.service.same.SameService;
import com.tdu.spider.biz.service.same.vo.SameSenseQueryVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 扫描定向渠道的内容，点赞数大于1000
 *
 * @author tangdu
 * @version $: DaliySameSpiderTask.java, v 0.1 2018年01月24日 上午9:15 tangdu Exp $
 */
@Component
public class DaliySameSpiderTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(DaliySameSpiderTask.class);


    @Autowired
    private SameService sameService;

    /**
     * 增量抓取文章,点赞超过40
     */
    @Scheduled(cron="0 0 10 * * *")
    public void daliyUpdateDefaultChannelSense() {
        LOGGER.info("Scheduled.DaliySameSpiderTask.daliyUpdateDefaultChannelSense 定时任务:");
        for(int i=0;i<2;i++) {
            try {
                SameSenseQueryVO sameSenseQueryVO = new SameSenseQueryVO();
                sameSenseQueryVO.setFilterLike(40);
                sameSenseQueryVO.setFilterPage(10);
                sameService.daliyUpdateDefaultChannelSense(sameSenseQueryVO);
                break;
            } catch (Exception e) {
                LOGGER.error("Scheduled.DaliySameSpiderTask.Error ", e);
            }
        }
    }

    /**
     * 增量抓取活跃用户文章,点赞超过10,抓取页面为2
     */
    @Scheduled(cron="0 0/10 * * * ?")
    public void daliyUpdateActivityChannelSense() {
        LOGGER.info("Scheduled.DaliySameSpiderTask 定时任务:");
        try {
            SameSenseQueryVO sameSenseQueryVO=new SameSenseQueryVO();
            sameSenseQueryVO.setUserId(15111234L);
            sameSenseQueryVO.setFilterLike(10);
            sameService.daliyUpdateActivityChannelSense(sameSenseQueryVO);
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliySameSpiderTask.daliyUpdateActivityChannelSense.Error ",e);
        }
    }
}
