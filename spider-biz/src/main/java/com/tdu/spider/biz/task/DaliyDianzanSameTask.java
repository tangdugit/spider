package com.tdu.spider.biz.task;

import com.google.common.collect.Lists;
import com.tdu.spider.biz.service.same.SameService;
import com.tdu.spider.biz.service.same.vo.LikeSenseQueryVO;
import com.tdu.spider.model.SameChannelDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by TangDu on 2017/7/9.
 */
@Component
public class DaliyDianzanSameTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(DaliyDianzanSameTask.class);

    @Autowired
    private SameService sameService;


    /**
     * 每2个小时执行任务(我的最重要用户)
     */
    @Scheduled(cron="0 0/50 7-23 * * ?")
    public void executeFirst() {
        LOGGER.info("Scheduled.DaliyDianzanSameTask.executeFirst 定时任务:");
        try {
            List<SameChannelDO> channelInfoVOS = Lists.newArrayList();
            //channelInfoVOS.add(SameChannelDO.build(1011855L, "足控只是会欣赏美"));
            //channelInfoVOS.add(SameChannelDO.build(1032823L,"长腿a杯"));
            //channelInfoVOS.add(SameChannelDO.build(1005871l,"人人都有一双大长腿"));
            channelInfoVOS.add(SameChannelDO.build(1002393L, "每日搭配"));
            channelInfoVOS.add(SameChannelDO.build(1491085L,"Masker"));
            channelInfoVOS.add(SameChannelDO.build(1528593L,"E.T.I.T"));
            channelInfoVOS.add(SameChannelDO.build(1015326L,"我这么美我不能死"));
            channelInfoVOS.add(SameChannelDO.build(1029212L, "不露脸的照片"));//
            channelInfoVOS.add(SameChannelDO.build(1030258L, "大龄samer频道"));
            channelInfoVOS.add(SameChannelDO.build(1026578L, "关于自己的20件事"));
//            channelInfoVOS.add(SameChannelDO.build(1000816L, "每天泡在健身房"));
            channelInfoVOS.add(SameChannelDO.build(1006617L, "因为拍了照没地方发"));
            channelInfoVOS.add(SameChannelDO.build(967L, "要命的自拍控"));
            channelInfoVOS.add(SameChannelDO.build(1011867L, "一星期情侣"));
            channelInfoVOS.add(SameChannelDO.build(1371486L, "比太阳还温暖的是你的笑"));
            LikeSenseQueryVO likeSenseQueryVO = new LikeSenseQueryVO(15111234L,channelInfoVOS);
            sameService.likeUpdateTopSense(likeSenseQueryVO);
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyDianzanSameTask.executeFirst.Error ",e);
        }
    }

    @Scheduled(cron="0 0/50 6-23 * * ?")
    public void 自己点赞() {
        LOGGER.info("Scheduled.DaliyDianzanSameTask.自己点赞 定时任务:");
        try {
            LikeSenseQueryVO likeSenseQueryVO = new LikeSenseQueryVO(15111234L,null);
            likeSenseQueryVO.setFilterLikes(2);
            sameService.randomActivityLikeSense(likeSenseQueryVO);
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyDianzanSameTask.自己点赞.Error ",e);
        }
    }

    /**
     * 每1个小时执行任务(给活跃的群点赞)
     */
    @Scheduled(cron="0 0/20 22-23 * * ?")
    public void 女粉点赞() {
        LOGGER.info("Scheduled.DaliyDianzanSameTask.女粉点赞 定时任务:");
        try {
            LikeSenseQueryVO likeSenseQueryVO = new LikeSenseQueryVO(15752075L,null);
            sameService.randomActivityLikeSense(likeSenseQueryVO);
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyDianzanSameTask.女粉点赞.Error ",e);
        }
    }

    @Scheduled(cron="0 0 6,14,23 * * ?")
    public void 画手点赞() {
        LOGGER.info("Scheduled.DaliyDianzanSameTask.画手点赞 定时任务:");
        try {
            LikeSenseQueryVO likeSenseQueryVO = new LikeSenseQueryVO(16053307L,null);
            sameService.randomActivityLikeSense(likeSenseQueryVO);
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyDianzanSameTask.画手点赞.Error ",e);
        }
    }
}
