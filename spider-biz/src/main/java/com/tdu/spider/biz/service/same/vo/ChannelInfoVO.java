package com.tdu.spider.biz.service.same.vo;

import com.tdu.spider.model.SameChannelDO;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.List;


@Setter
@Getter
public class ChannelInfoVO implements Serializable {
    private Long id;

    private Long touch_time;

    private List<SameChannelDO> channel;

    private Integer badge;

    private Long last_modified_at;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
