package com.tdu.spider.biz.util;

import org.apache.commons.lang3.StringUtils;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: LogUtils.java, v 0.1 2018年01月24日 下午4:21 tangdu Exp $
 */
public class LogUtils {

    public static String curtail(String str){
        if(StringUtils.isBlank(str)){
            return "";
        }
        if(str.length()>400){
            return str.substring(0,400);
        }
        return str;
    }
}
