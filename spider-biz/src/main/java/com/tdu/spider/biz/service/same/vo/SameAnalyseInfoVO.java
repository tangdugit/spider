package com.tdu.spider.biz.service.same.vo;

import com.tdu.spider.model.SameLikeCountVO;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SameAnalyseInfoVO.java, v 0.1 2018年02月03日 下午3:27 tangdu Exp $
 */
@Setter
@Getter
public class SameAnalyseInfoVO {

    /** 关注的渠道数量 **/
    private Integer attentionChannelNum;

    /** 最活跃的频道 **/
    private SameChannelVO activeChannel;

    /** 频道文章统计 **/
    private List<SameChannelVO> channelVOS;

    private SameAnalyseChannelChart sameAnalyseChannelChart;

    /** 发布的文章数量 **/
    private Integer publishSenseNum;

    /** 数据日期 **/
    private Date rowdate;

    /** 最晚发贴时间 **/
    private SenSeVO maxPublishSense;

    /** 最多同感文章 **/
    private SenSeVO moreStarSense;

    /** 注册日期 **/
    private Date registerDate;

    /** 最早Sense发布时间 **/
    private SenSeVO firstPublishSense;

    /** 最关心你的人 **/
    private List<SameLikeCountVO> mustStarUsers;

    /** 当前用户名称 **/
    private String  userName;

    /** 当前用户Id **/
    private Long  userId;

    /** 用户信息标签 **/
    private String tag;

    /** 用户头像 **/
    private String userAvatar;

    /**总赞统计**/
    private Integer dianzanNum;

    /**总赞浏览数**/
    private Integer viewsNum;

    /** 关键词 **/
    private String keyword;

    /** 关键词描述 **/
    private String keywordDesc;

    /** 码云 **/
    private String mayun;

    @Setter
    @Getter
    public static class SameAnalyseChannelChartRow {
        private String name;
        private Integer value;
    }
    @Setter
    @Getter
    public static  class SameAnalyseChannelChart {
        private List<String>        channels;
        private List<SameAnalyseChannelChartRow> channelNums;
    }

}
