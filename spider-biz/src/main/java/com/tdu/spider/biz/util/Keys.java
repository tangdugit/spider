package com.tdu.spider.biz.util;

import com.google.common.base.Joiner;

public class Keys {

    public static String union(String... values) {
        return Joiner.on(":").join(values);
    }

    public static class Same {
        public static final String prefix         = "SAME";
        public static final String USERS_TOKENS   = union(prefix, "USERS_TOKEN");
        public static final String USERS_CHANNELS = union(prefix, "USERS_CHANNEL");

        public static String forUserUploadToken(Long userId) {
            return union(prefix, "USERS_UPLOAD_TOKEN", userId + "");
        }
    }

    public static class Instagram {

    }

    public static class zhihu {

    }
}
