package com.tdu.spider.biz.service.same.vo;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

@Data
public class VideoVO implements Serializable {

    private int id;
    private String src;
    private Long created_at;
    private String cover;
    private String sticker;


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
