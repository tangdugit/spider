package com.tdu.spider.biz.service.qutou;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.google.common.collect.Lists;
import com.tdu.spider.biz.service.qutou.vo.QuTouArticleDataVO;
import com.tdu.spider.biz.service.qutou.vo.QuTouArticleQueryVO;
import com.tdu.spider.biz.service.same.vo.CommonResult;
import com.tdu.spider.biz.util.HttpUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

@Service
public class QuTouApiService {
    private static final Logger LOGGER = LoggerFactory.getLogger(QuTouApiService.class);


    public List<QuTouArticleDataVO> queryArticle(QuTouArticleQueryVO quTouArticleQueryVO) throws Exception {
        Field[] fields = QuTouArticleQueryVO.class.getDeclaredFields();
        Form form = Form.form();
        for (Field field : fields) {
            String fieldName = field.getName();
            Method method = ReflectionUtils.findMethod(QuTouArticleQueryVO.class,
                "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1));
            Object object = ReflectionUtils.invokeMethod(method, quTouArticleQueryVO);
            if (object == null) {
                continue;
            }
            form.add(fieldName, object.toString());
        }
        List<NameValuePair> nameValuePairs = form.build();
        StringBuilder sbf = new StringBuilder();
        for (NameValuePair nameValuePair : nameValuePairs) {
            sbf.append(nameValuePair.getName()).append("=").append(nameValuePair.getValue())
                .append("&");
        }
        String url="https://api.1sapp.com/content/getList?" + sbf.substring(0,sbf.length()-1);
        Request request = Request.Get(url)
                .addHeader("Host","api.1sapp.com")
            .addHeader("Cookie",
                "UM_distinctid=15ff76c5d5b1e5-0cc86da098c8d28-3440143e-3d10d-15ff76c5d5c5e; aliyungf_tc=AQAAAOkVnChr6wkABX2ctzv6kWyMfg5S; qkV2=983b6a7ff377eb6c147a9395b2e87327")
            .addHeader("Accept", "*/*").addHeader("Accept-Language", "zh-Hans-CN;q=1").addHeader(
                "User-Agent", "qu tou tiao/2.3.3 (iPhone; iOS 10.3.2; Scale/2.00)/qukan_ios");
        Response response = Executor.newInstance(HttpUtils.httpClient()).execute(request);
        String content = response.returnContent().asString();
        LOGGER.debug("queryArticle url:{},params:{},result:{}",url, request.toString(),nameValuePairs, content);

        if (StringUtils.hasLength(content)) {
            CommonResult<List<QuTouArticleDataVO>> result = JSON.parseObject(content,
                new TypeReference<CommonResult<List<QuTouArticleDataVO>>>() {
                });
            return result.getData();
        }
        return Lists.newArrayList();
    }
}
