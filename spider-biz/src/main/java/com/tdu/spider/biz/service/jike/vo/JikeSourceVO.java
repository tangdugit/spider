package com.tdu.spider.biz.service.jike.vo;

import lombok.Data;

import java.util.List;


@Data
public class JikeSourceVO {
    private String       thumbnailUrl;
    private String       middlePicUrl;
    private String       picUrl;
    private String       format;
    private String       type;
    private List<String> source;
    private String       duration;

}
