package com.tdu.spider.biz.service.huocaihe.vo;

import lombok.Data;

@Data
public class HuoArcitleListVO {
    private String           type;
    private String           lastid;
    private HuoArcitleInfoVO info;
}
