package com.tdu.spider.biz.service.weidian;

import java.util.List;

public class Result {


    /**
     * status : {"code":0,"message":"OK","description":""}
     * result : {"shops":[{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"老板在烟台","tagBgColor":"0x0097A7","tagId":"500002","tagName":"老板在烟台"}],"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"1024903213","shopGrade":12,"shopId":"1024903213","shopLogo":"https://si.geilicdn.com/bj-vshop-1024903213-1537792340905-2071212837_992_992.jpg?w=992&h=992","shopName":"零食驿站南校店","shopUpdate":{"updateCount":3,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2659741737","itemLowPrice":450,"itemMainPic":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","itemName":"好益味340ml","itemOriginalLowPrice":450,"itemOriginalPrice":450,"itemPointOriginalPrice":450,"itemPrice":450,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":9,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"1024903213","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":39.94},"userId":"1302810931"},"commentNum":0,"createTime":1544176522000,"favourNum":0,"hasFavoured":false,"id":2821209,"private":false,"shopId":1272546125,"showable":true,"status":1,"text":"配送很快，在宿舍买东西超级方便，希望越做越好呀～","top":false},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/passport-0f371ce37144d32024402980f0098f31.jpg","userId":"1119487402","userNickName":"Vera Liu"},{"userHeadUrl":"https://si.geilicdn.com/passport-ef8fff7ce4f04b213ddba1669363a255.jpg","userId":"1276984392","userNickName":"岑故味儿"},{"userHeadUrl":"https://si.geilicdn.com/bj-user-1338403851-1536669793486-945702721_2667_4000.jpg?w=160&h=160&cp=1","userId":"1338403851","userNickName":"小螃蟹"},{"userHeadUrl":"https://si.geilicdn.com/passport-5b42cd72997a15a1ec3d67af9211cd16.jpg","userId":"1302810931","userNickName":"婷婷🎈"},{"userHeadUrl":"https://si.geilicdn.com/passport-99e18933668d46fe4773b7cf8bd596d9.jpg","userId":"1382246607","userNickName":"是三水啊"},{"userHeadUrl":"https://si.geilicdn.com/passport-1e0e69d54a94ae557926299cc99a7cf2.jpg","userId":"1385781866","userNickName":"eiei"}],"sayingNum":31,"sellerId":"1272546125","shopGrade":11,"shopId":"1272546125","shopLogo":"https://si.geilicdn.com/bj-wd-1272546125-1535792089798-729480389_250_250.jpg?w=250&h=250","shopName":"零食驿站北校店","shopUpdate":{"updateCount":9,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2660248098","itemLowPrice":450,"itemMainPic":"https://si.geilicdn.com/wdseller1272546125-0e1f0000016797a4f6d50a02685e_800_800.jpg","itemName":"好益味","itemOriginalLowPrice":450,"itemOriginalPrice":450,"itemPointOriginalPrice":450,"itemPrice":450,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":12,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"1272546125","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"优选好物","tagBgColor":"0x0097A7","tagId":"11001307","tagName":"优选好物"},{"tag":"进口零食馆","tagBgColor":"0xFFA015","tagId":"66036","tagName":"进口零食馆"}],"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"1224858485","shopGrade":9,"shopId":"1224858485","shopLogo":"https://si.geilicdn.com/forward1224858485-7f8c00000167168eae940a02853e_250_250.jpg?w=250&h=250","shopName":"小馋嘴幸运小筑创意零食","status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"吃货收藏系列","tagBgColor":"0xFFA015","tagId":"74005","tagName":"吃货收藏系列"}],"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":32.06},"userId":"1383300130"},"commentNum":0,"createTime":1541151079000,"favourNum":0,"hasFavoured":false,"id":2159446,"imgList":["https://si.geilicdn.com/familiar1383300130-214400000166d3c3387a0a02a3a3_1280_1280.jpg","https://si.geilicdn.com/familiar1383300130-599a00000166d3c3305d0a026860_1280_1280.jpg","https://si.geilicdn.com/familiar1383300130-59e700000166d3c334940a026860_1280_1280.jpg"],"private":false,"shopId":161157203,"showable":true,"status":1,"text":"本来拿到福袋还很不高兴是鱼虾锅巴，个人原因不太喜欢鱼。\n今天下班了闲得无聊，逐一把福袋开了，就这个！小鱼虾！一张口，嚼起来简直惊为天人！\n鱼鲜味和锅巴的香味，米脆脆的还特别耐嚼！绝配啊！\n在我心目中，蛋黄锅巴已经退位了！小鱼虾上位做正宫！\n说真的，不试试后悔！","top":false},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/passport-f8c749bd861483617cd8113620f34166.jpg","userId":"1178002693","userNickName":"我欲因之梦"},{"userHeadUrl":"https://si.geilicdn.com/passport-4f51d8a81a1ebb8275e04c4813899d91.jpg","userId":"778892622","userNickName":"厌."},{"userHeadUrl":"https://si.geilicdn.com/passport-49b07392244d6b777777bdf0a1cbec6c.jpg","userId":"1379832950","userNickName":"赵娟💭"},{"userHeadUrl":"https://si.geilicdn.com/passport-4c9a6437f9f5c9ca2c39307a09034e0a.jpg","userId":"1383300130","userNickName":"川之上"},{"userHeadUrl":"https://si.geilicdn.com/passport-031c6912363f601a57b770ead4a431e8.jpg","userId":"572692505","userNickName":"A婴姿美母婴生活馆"},{"userHeadUrl":"https://si.geilicdn.com/passport-cddc0459b8bbce42ad5ca25a51b4b0b5.jpg","userId":"284102115","userNickName":"SL鹿～"}],"sayingNum":81,"sellerId":"161157203","shopGrade":12,"shopId":"161157203","shopLogo":"https://si.geilicdn.com/bj-wd-161157203-1539680106216-948508534_250_250.jpg?w=250&h=250","shopName":"幸运小筑创意零食","status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"不得不入","tagBgColor":"0xF151A1","tagId":"68007","tagName":"不得不入"},{"tag":"美女店长","tagBgColor":"0x0097A7","tagId":"90016","tagName":"美女店长"}],"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"1208170963","shopGrade":12,"shopId":"1208170963","shopLogo":"https://si.geilicdn.com/bj-vshop-1208170963-1529916417349-1139501601_984_984.jpg?w=984&h=984","shopName":"可可姐健康养生零食馆","shopUpdate":{"updateCount":7,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2659841511","itemLowPrice":52000,"itemMainPic":"https://si.geilicdn.com/wdseller1208170963-52b700000167987eab640a02853e_1920_1203.jpg","itemName":"养肤润肠美容套餐，大姹紫嫣红➕大肠来➕肠美美➕淡斑茶送一大瓶燕麦酥原价628现520","itemOriginalLowPrice":52000,"itemOriginalPrice":52000,"itemPointOriginalPrice":52000,"itemPrice":52000,"itemSkuList":[{"attrIds":"","id":9221993685,"sold":0,"stock":0,"title":"一套"}],"itemSoldout":0,"itemStatus":0,"itemStock":19,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"1208170963","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"零食发现家","tagBgColor":"0xFFA015","tagId":"24134","tagName":"零食发现家"},{"tag":"零食馆","tagBgColor":"0xFFA015","tagId":"313","tagName":"零食馆"},{"tag":"下酒菜","tagBgColor":"0xFFA015","tagId":"520","tagName":"下酒菜"},{"tag":"爱吃的零食","tagBgColor":"0xFFA015","tagId":"68050","tagName":"爱吃的零食"}],"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":42.87},"userId":"1141119575"},"commentNum":0,"createTime":1540888605000,"favourNum":0,"hasFavoured":false,"id":2101106,"imgList":["https://si.geilicdn.com/hz_dynamic_384f00000166c41e2e350a02685e_1200_1200.jpeg","https://si.geilicdn.com/hz_dynamic_384a00000166c41e2d720a02685e_1200_1200.jpeg","https://si.geilicdn.com/hz_dynamic_3ae800000166c41e2f750a02853e_1200_1200.jpeg","https://si.geilicdn.com/hz_dynamic_3c3200000166c41e2d410a028841_1200_1200.jpeg"],"private":false,"shopId":160482652,"showable":true,"status":1,"text":"最能概括的两个字就是品质，包括做工口感味道，真的很赞的零食系列[           心         ][         心       ][         心       ]","top":false},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/login1288677090-0eda0000016675d3da790a02685e_400_400.jpg","userId":"1288677090","userNickName":"琦琙"},{"userHeadUrl":"https://si.geilicdn.com/passport-78f5e63782b50a772226ce42452efdf5.jpg","userId":"836955102","userNickName":"▽some▽"},{"userHeadUrl":"https://si.geilicdn.com/passport-3744a3c4dd9dc23cf2c19b84bcc07298.jpg","userId":"1211248302","userNickName":"はな"},{"userHeadUrl":"https://si.geilicdn.com/passport-c47b0263d0a74866cf48c2d08c6b0926.jpg","userId":"1141119575","userNickName":"🐑🐳。"},{"userHeadUrl":"https://si.geilicdn.com/passport-58c31049a696741e58997fb7a574c19c.jpg","userId":"977785903","userNickName":"不瘦不改名"},{"userHeadUrl":"https://si.geilicdn.com/passport-a676439b465f33e5e10bf99b0820d03e.jpg","userId":"1205994794","userNickName":"A芒果控宝宝🐷 🐷 🐯"}],"sayingNum":26,"sellerId":"160482652","shopGrade":11,"shopId":"160482652","shopLogo":"https://si.geilicdn.com/bj-wd-160482652-1538918050391-1231750176_250_250.jpg?w=250&h=250","shopName":"幸运小筑luckyhome品牌零食","status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"零食百宝箱","tagBgColor":"0xFFA015","tagId":"22003","tagName":"零食百宝箱"},{"tag":"零食发现家","tagBgColor":"0xFFA015","tagId":"24134","tagName":"零食发现家"},{"tag":"零食馆","tagBgColor":"0xFFA015","tagId":"313","tagName":"零食馆"},{"tag":"全球零食捕手","tagBgColor":"0xFFA015","tagId":"512","tagName":"全球零食捕手"},{"tag":"爽口零食","tagBgColor":"0xFFA015","tagId":"68023","tagName":"爽口零食"},{"tag":"爱吃的零食","tagBgColor":"0xFFA015","tagId":"68050","tagName":"爱吃的零食"},{"tag":"老板很热情","tagBgColor":"0x0097A7","tagId":"90030","tagName":"老板很热情"},{"tag":"进口零食","tagBgColor":"0xFFA015","tagId":"90397","tagName":"进口零食"}],"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":27.28},"userId":"1119996143"},"commentNum":0,"createTime":1544169272000,"favourNum":0,"hasFavoured":false,"id":2818807,"private":false,"shopId":887896394,"showable":true,"status":1,"text":"常客啦，常客啦，常客啦，东西都很实惠的，价廉物美!!!","top":false},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/passport-4eb29d44acb4372292482d88df138ad8.jpg","userId":"1130420173","userNickName":"黄顺雯"},{"userHeadUrl":"https://si.geilicdn.com/passport-53c4dd65bfc11b760542cc7ad70c8dca.jpg","userId":"965882041","userNickName":"宁静致远"},{"userHeadUrl":"https://si.geilicdn.com/passport-0030d95ee4a986f9157278d87088cde4.jpg","userId":"1303617311","userNickName":"北风"},{"userHeadUrl":"https://si.geilicdn.com/passport-885be17501301eba49979a60b3843259.jpg","userId":"1119996143","userNickName":"小姚～～工作手机"},{"userHeadUrl":"https://si.geilicdn.com/passport-9f5f8b67bb3aabdc21d076524d674b56.jpg","userId":"1152617672","userNickName":"周 十分部"},{"userHeadUrl":"https://si.geilicdn.com/passport-87665f0ba722e4e1eb88446249a92be0.jpg","userId":"1149395674","userNickName":"阿霞"}],"sayingNum":67,"sellerId":"887896394","shopGrade":13,"shopId":"887896394","shopLogo":"https://si.geilicdn.com/bj-vshop-887896394-1504626272853-1701025271_992_992.jpg?w=992&h=992","shopName":"小面包零食特卖","shopUpdate":{"updateCount":5,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2660126169","itemLowPrice":5800,"itemMainPic":"https://si.geilicdn.com/weidian887896394-0bd3000001679af956350a028841_1488_1984.jpg","itemName":"😋小面包喜欢的卤汁牛肉干\n采用高原珍稀食材熬制\n美味与养生 ","itemOriginalLowPrice":5800,"itemOriginalPrice":5800,"itemPointOriginalPrice":5800,"itemPrice":5800,"itemSkuList":[{"attrIds":"","id":9225427019,"sold":0,"stock":0,"title":"1斤五香卤汁"},{"attrIds":"","id":9225427021,"sold":0,"stock":0,"title":"1斤麻辣卤汁"},{"attrIds":"","id":9225445020,"sold":0,"stock":0,"title":"半斤麻辣卤汁"},{"attrIds":"","id":9225445022,"sold":0,"stock":0,"title":"半斤五香卤汁"}],"itemSoldout":0,"itemStatus":0,"itemStock":400,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"887896394","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":30.49},"userId":"1389415342"},"commentNum":0,"createTime":1541939051000,"favourNum":0,"hasFavoured":false,"id":2334711,"private":false,"shopId":1386869955,"showable":true,"status":1,"text":"挺不错的哦😊，真的很方便的，真的蛮不错的哦","top":false},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/passport-46e9a46beb52dc5ff8fe042af038a2ac.jpg","userId":"1388941907","userNickName":"【】"},{"userHeadUrl":"https://si.geilicdn.com/passport-bea26a7bbbdadc7dfaf53e3660a98c18.jpg","userId":"1390439572","userNickName":"炎之精灵"},{"userHeadUrl":"https://si.geilicdn.com/passport-0c6dacb1a7c7a9e8096bb839720b42ea.jpg","userId":"1389474008","userNickName":"🍊오렌지"},{"userHeadUrl":"https://si.geilicdn.com/passport-24801ce31b5abacd8faa2d3eee42d2b6.jpg","userId":"1389415342","userNickName":"豆腐花"},{"userHeadUrl":"https://si.geilicdn.com/passport-d0dfae512d062ffe84fd81c1c02fcef4.jpg","userId":"1401409872","userNickName":"etc.zz"},{"userHeadUrl":"https://si.geilicdn.com/passport-65d8a9ff9bf811b9ee8b075b87be95f1.jpg","userId":"1395286545","userNickName":"She said"}],"sayingNum":6,"sellerId":"1386869955","shopGrade":10,"shopId":"1386869955","shopLogo":"https://si.geilicdn.com/bj-PC-1386869955-1538221065754-1657868_939_643.jpg?w=110&h=110","shopName":"福职四号楼零食铺","shopUpdate":{"updateCount":4,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2612101822","itemLowPrice":350,"itemMainPic":"https://si.geilicdn.com/bj-pc-1386869955-1538897212133-1924381588_888_1088.jpg","itemName":"盼盼虾条","itemOriginalLowPrice":350,"itemOriginalPrice":350,"itemPointOriginalPrice":350,"itemPrice":350,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":4,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"1386869955","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"1226127169","shopGrade":10,"shopId":"1226127169","shopLogo":"https://si.geilicdn.com/bj-vshop-1226127169-1537208317812-512481191_984_984.jpg?w=984&h=984","shopName":"二期a~咏哥的零食铺","status":0,"tagList":[],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"零食百宝箱","tagBgColor":"0xFFA015","tagId":"22003","tagName":"零食百宝箱"},{"tag":"零食发现家","tagBgColor":"0xFFA015","tagId":"24134","tagName":"零食发现家"},{"tag":"看剧好搭档","tagBgColor":"0xFFA015","tagId":"26006","tagName":"看剧好搭档"},{"tag":"美味零食","tagBgColor":"0xFFA015","tagId":"271","tagName":"美味零食"},{"tag":"深夜食堂","tagBgColor":"0xFFA015","tagId":"29012","tagName":"深夜食堂"},{"tag":"零食馆","tagBgColor":"0xFFA015","tagId":"313","tagName":"零食馆"},{"tag":"全球零食捕手","tagBgColor":"0xFFA015","tagId":"512","tagName":"全球零食捕手"},{"tag":"手工美食","tagBgColor":"0xFFA015","tagId":"517","tagName":"手工美食"},{"tag":"小吃货","tagBgColor":"0xFFA015","tagId":"66005","tagName":"小吃货"},{"tag":"进口零食馆","tagBgColor":"0xFFA015","tagId":"66036","tagName":"进口零食馆"},{"tag":"吃吃喝喝","tagBgColor":"0xFFA015","tagId":"68","tagName":"吃吃喝喝"},{"tag":"爱吃的零食","tagBgColor":"0xFFA015","tagId":"68050","tagName":"爱吃的零食"},{"tag":"宅家零嘴","tagBgColor":"0xFFA015","tagId":"70152","tagName":"宅家零嘴"},{"tag":"甜食人","tagBgColor":"0xFFA015","tagId":"70162","tagName":"甜食人"},{"tag":"精选全球夯货","tagBgColor":"0x4D33A5","tagId":"71131","tagName":"精选全球夯货"},{"tag":"吃货收藏系列","tagBgColor":"0xFFA015","tagId":"74005","tagName":"吃货收藏系列"},{"tag":"Yummy！","tagBgColor":"0xFFA015","tagId":"83005","tagName":"Yummy！"},{"tag":"吃吃的爱","tagBgColor":"0xFFA015","tagId":"89057","tagName":"吃吃的爱"},{"tag":"舌尖之舞","tagBgColor":"0x21A791","tagId":"90241","tagName":"舌尖之舞"},{"tag":"进口零食","tagBgColor":"0xFFA015","tagId":"90397","tagName":"进口零食"}],"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"1168664442","shopGrade":12,"shopId":"1168664442","shopLogo":"https://si.geilicdn.com/vshop1168664442-1488212763.jpg?w=984&h=984","shopName":"大喜进口零食","shopUpdate":{"updateCount":4,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2660095493","itemLowPrice":1300,"itemMainPic":"https://si.geilicdn.com/forward1168664442-6088000001679b0e6bdd0a02853e_1080_1080.jpg","itemName":"8145不二家饮料","itemOriginalLowPrice":1300,"itemOriginalPrice":1300,"itemPointOriginalPrice":1300,"itemPrice":1300,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":100,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"1168664442","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"984755018","shopGrade":13,"shopId":"984755018","shopLogo":"https://si.geilicdn.com/vshop984755018-1472693062.jpeg?w=250&h=250","shopName":"内蒙古财经大学9号楼99零食铺","shopUpdate":{"updateCount":4,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"1966591634","itemLowPrice":350,"itemMainPic":"https://si.geilicdn.com/vshop984755018-1476336617070-8157E-s1.jpg","itemName":"美丹白苏打饼干","itemOriginalLowPrice":350,"itemOriginalPrice":350,"itemPointOriginalPrice":350,"itemPrice":350,"itemSkuList":[{"attrIds":"","id":5748450641,"sold":0,"stock":0,"title":"海苔味（蓝色）"},{"attrIds":"","id":5869081927,"sold":0,"stock":0,"title":"鲜葱味（绿色）"},{"attrIds":"","id":5898508500,"sold":0,"stock":0,"title":"芝麻味（红色）"}],"itemSoldout":0,"itemStatus":0,"itemStock":3,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"984755018","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"零食发现家","tagBgColor":"0xFFA015","tagId":"24134","tagName":"零食发现家"},{"tag":"俄罗斯零食","tagBgColor":"0xFFA015","tagId":"504","tagName":"俄罗斯零食"},{"tag":"爱吃的零食","tagBgColor":"0xFFA015","tagId":"68050","tagName":"爱吃的零食"}],"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"310399442","shopGrade":11,"shopId":"310399442","shopLogo":"https://si.geilicdn.com/vshop310399442-1424783044.jpg?w=250&h=250","shopName":"果妈海外购～俄罗斯食品零食泰国彩妆 乳胶枕","status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":27.14},"userId":"342954838"},"commentNum":0,"createTime":1544007065000,"favourNum":0,"hasFavoured":false,"id":2784268,"private":false,"shopId":981481823,"showable":true,"status":1,"text":"对我这种懒癌晚期的人来说，这家店的确是个不错的选择！","top":false},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/passport-4a7b021514b335378d930f4b691a72c9.jpg","userId":"1400733390","userNickName":"如初、不遇"},{"userHeadUrl":"https://si.geilicdn.com/passport-8462337579978a2b996ded6aa382a25d.jpg","userId":"1400447996","userNickName":"AsKi"},{"userHeadUrl":"https://si.geilicdn.com/passport-eeaa63bc9745aff96ce86266470087b2.jpg","userId":"1397610103","userNickName":"KOBE  BRYANT"},{"userHeadUrl":"https://si.geilicdn.com/passport-5c7f7cbd1d6e381ac5974a33c4ffe462.jpg","userId":"342954838","userNickName":"丿星空*物语"}],"sayingNum":4,"sellerId":"981481823","shopGrade":9,"shopId":"981481823","shopLogo":"https://si.geilicdn.com/vshop981481823-1471358998.jpg?w=984&h=984","shopName":"欧凯琦大学生零食速送","shopUpdate":{"updateCount":9,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2659756337","itemLowPrice":600,"itemMainPic":"https://si.geilicdn.com/pcitem981481823-61110000016797e9609f0a028841_800_800.jpg","itemName":"汤达人（日式豚骨面）","itemOriginalLowPrice":600,"itemOriginalPrice":600,"itemPointOriginalPrice":600,"itemPrice":600,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":111111111,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"981481823","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"零食发现家","tagBgColor":"0xFFA015","tagId":"24134","tagName":"零食发现家"},{"tag":"饼干膨化","tagBgColor":"0xFFA015","tagId":"24140","tagName":"饼干膨化"},{"tag":"美味零食","tagBgColor":"0xFFA015","tagId":"271","tagName":"美味零食"},{"tag":"零食馆","tagBgColor":"0xFFA015","tagId":"313","tagName":"零食馆"},{"tag":"全球零食捕手","tagBgColor":"0xFFA015","tagId":"512","tagName":"全球零食捕手"},{"tag":"地方特产","tagBgColor":"0xFFA015","tagId":"556","tagName":"地方特产"},{"tag":"小吃货","tagBgColor":"0xFFA015","tagId":"66005","tagName":"小吃货"},{"tag":"进口零食馆","tagBgColor":"0xFFA015","tagId":"66036","tagName":"进口零食馆"},{"tag":"出游零食","tagBgColor":"0xFFA015","tagId":"67025","tagName":"出游零食"},{"tag":"不开心吃点啥","tagBgColor":"0xFFA015","tagId":"68037","tagName":"不开心吃点啥"},{"tag":"宅家零嘴","tagBgColor":"0xFFA015","tagId":"70152","tagName":"宅家零嘴"},{"tag":"吃吃的爱","tagBgColor":"0xFFA015","tagId":"89057","tagName":"吃吃的爱"},{"tag":"进口零食","tagBgColor":"0xFFA015","tagId":"90397","tagName":"进口零食"}],"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":28.46},"userId":"364693921"},"commentNum":0,"createTime":1541694664000,"favourNum":0,"hasFavoured":false,"id":2283786,"private":false,"shopId":1179570945,"showable":true,"status":1,"text":"第一次遇见了森田零食，就忍不住要买零食的冲动🤙🤙，已经第五次回购了😊😊😊，东西很赞。","top":false},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/passport-a5b371a01c0b68843416705fc4b9cfdb.jpg","userId":"1275336594","userNickName":"Supreme"},{"userHeadUrl":"https://si.geilicdn.com/passport-6176d72058c7c356963764b3357d9c3c.jpg","userId":"1220765172","userNickName":"钟小饱"},{"userHeadUrl":"https://si.geilicdn.com/passport-a7b34db6d8f5dee80549f8dc8170783f.jpg","userId":"763718398","userNickName":"洁斯-安吉拉美容机构"},{"userHeadUrl":"https://si.geilicdn.com/passport-03bbee54a7dab1b24192641f6772e468.jpg","userId":"364693921","userNickName":"Sāndy、"},{"userHeadUrl":"https://si.geilicdn.com/hz_wdbuyer_4ebd000001657a176efa0a02685e_400_400_unadjust.jpeg","userId":"824091806","userNickName":"I597"},{"userHeadUrl":"https://si.geilicdn.com/udc_20b0d0251b1c9f9c43edaa15f663c45d.jpg","userId":"1293312049","userNickName":"蜜汁微笑😎"}],"sayingNum":12,"sellerId":"1179570945","shopGrade":11,"shopId":"1179570945","shopLogo":"https://si.geilicdn.com/bj-wd-1179570945-1533541449680-1107721872_250_250.jpg?w=250&h=250","shopName":"森田零食","shopUpdate":{"updateCount":2,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2659881041","itemLowPrice":2800,"itemMainPic":"https://si.geilicdn.com/forward1179570945-6bd10000016798a405310a02685e_750_750.jpg","itemName":"日本丸金牛乳蛋糕 欧式小蛋糕16入装","itemOriginalLowPrice":2800,"itemOriginalPrice":2800,"itemPointOriginalPrice":2800,"itemPrice":2800,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":80,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"1179570945","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"土特产","tagBgColor":"0x21A791","tagId":"24030","tagName":"土特产"},{"tag":"美味零食","tagBgColor":"0xFFA015","tagId":"271","tagName":"美味零食"},{"tag":"富平柿饼","tagBgColor":"0xFFA015","tagId":"503","tagName":"富平柿饼"},{"tag":"办公室零食","tagBgColor":"0xFFA015","tagId":"510","tagName":"办公室零食"},{"tag":"宅家零嘴","tagBgColor":"0xFFA015","tagId":"70152","tagName":"宅家零嘴"},{"tag":"吃在农家","tagBgColor":"0x21A791","tagId":"71013","tagName":"吃在农家"},{"tag":"鲜食农作","tagBgColor":"0xFFA015","tagId":"90088","tagName":"鲜食农作"},{"tag":"农产品","tagBgColor":"0x21A791","tagId":"90262","tagName":"农产品"}],"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"901946367","shopGrade":11,"shopId":"901946367","shopLogo":"https://si.geilicdn.com/bj-wd-901946367-1506640937365-1851797308_250_250.jpg?w=250&h=250","shopName":"南北土特产零食","shopUpdate":{"updateCount":1,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":1,"itemDiscount":0,"itemId":"2148441112","itemLowPrice":4500,"itemMainPic":"https://si.geilicdn.com/bj-wd-901946367-1502695628858-1850012572_760_760.jpg","itemName":"【下饭好物】麻辣小香肠400g烧烤火锅 自制特产","itemOriginalLowPrice":4500,"itemOriginalPrice":4500,"itemPoint":21,"itemPointOriginalPrice":4500,"itemPointPrice":3990,"itemPrice":4500,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":0,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":true,"sellerId":"901946367","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"923189560","shopGrade":10,"shopId":"923189560","shopLogo":"https://si.geilicdn.com/bj-vshop-923189560-1498518390610-1782937966_250_250.jpg?w=250&h=250","shopName":"美丽土特产零食","status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"零食百宝箱","tagBgColor":"0xFFA015","tagId":"22003","tagName":"零食百宝箱"},{"tag":"甜蜜烘培坊","tagBgColor":"0xFFA015","tagId":"235","tagName":"甜蜜烘培坊"},{"tag":"零食发现家","tagBgColor":"0xFFA015","tagId":"24134","tagName":"零食发现家"},{"tag":"饼干膨化","tagBgColor":"0xFFA015","tagId":"24140","tagName":"饼干膨化"},{"tag":"美味零食","tagBgColor":"0xFFA015","tagId":"271","tagName":"美味零食"},{"tag":"深夜食堂","tagBgColor":"0xFFA015","tagId":"29012","tagName":"深夜食堂"},{"tag":"治愈系美食","tagBgColor":"0xFFA015","tagId":"29034","tagName":"治愈系美食"},{"tag":"零食馆","tagBgColor":"0xFFA015","tagId":"313","tagName":"零食馆"},{"tag":"深夜追剧必备","tagBgColor":"0xFFA015","tagId":"487","tagName":"深夜追剧必备"},{"tag":"全球零食捕手","tagBgColor":"0xFFA015","tagId":"512","tagName":"全球零食捕手"},{"tag":"食客","tagBgColor":"0xFFA015","tagId":"515007","tagName":"食客"},{"tag":"美食家","tagBgColor":"0xFFA015","tagId":"62012","tagName":"美食家"},{"tag":"小吃货","tagBgColor":"0xFFA015","tagId":"66005","tagName":"小吃货"},{"tag":"进口零食馆","tagBgColor":"0xFFA015","tagId":"66036","tagName":"进口零食馆"},{"tag":"生活太苦加点甜","tagBgColor":"0xFFA015","tagId":"67008","tagName":"生活太苦加点甜"},{"tag":"爽口零食","tagBgColor":"0xFFA015","tagId":"68023","tagName":"爽口零食"},{"tag":"宅家零嘴","tagBgColor":"0xFFA015","tagId":"70152","tagName":"宅家零嘴"},{"tag":"饮食男女","tagBgColor":"0xFFA015","tagId":"73009","tagName":"饮食男女"},{"tag":"Yummy！","tagBgColor":"0xFFA015","tagId":"83005","tagName":"Yummy！"},{"tag":"吃吃的爱","tagBgColor":"0xFFA015","tagId":"89057","tagName":"吃吃的爱"},{"tag":"进口零食","tagBgColor":"0xFFA015","tagId":"90397","tagName":"进口零食"}],"regularCustomerNum":0,"repurRatePercent":"","sayingNum":0,"sellerId":"207723","shopGrade":11,"shopId":"207723","shopLogo":"https://si.geilicdn.com/bj-wd-207723-1509633549181-773786243_250_250.jpg?w=250&h=250","shopName":"零食时间","shopUpdate":{"updateCount":2,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2659925873","itemLowPrice":2450,"itemMainPic":"https://si.geilicdn.com/forwardw207723-25b800000167989f60df0a026860_750_750.jpg","itemName":"日本丸金扎带牛乳欧式小蛋糕","itemOriginalLowPrice":2450,"itemOriginalPrice":2450,"itemPointOriginalPrice":2450,"itemPrice":2450,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":60,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"207723","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":30.24},"userId":"1266533166"},"commentNum":0,"createTime":1544267984000,"favourNum":0,"hasFavoured":false,"id":2840366,"private":false,"shopId":1288270985,"showable":true,"status":2,"text":"可以可以，很棒，很实惠，店主情商高，会做生意，厉害厉害！","top":true,"topTime":1544268542000},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/passport-97ed052057fb13eab322bb8211f94022.jpg","userId":"1266533166","userNickName":"有时很远，有时很近"},{"userHeadUrl":"https://si.geilicdn.com/passport-58cb941580b61cd03e5bd81c00812029.jpg","userId":"1404191046","userNickName":"怪咖"},{"userHeadUrl":"https://si.geilicdn.com/passport-1af1ea6c1061edd4c0676283ea596d8f.jpg","userId":"1389963778","userNickName":"梅小慧"}],"sayingNum":3,"sellerId":"1288270985","shopGrade":7,"shopId":"1288270985","shopLogo":"https://si.geilicdn.com/weidian1288270985-7755000001674bfb97650a028841_984_984.jpg","shopName":"好再来零食铺子","status":0,"tagList":[],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":31.28},"userId":"1393953050"},"commentNum":0,"createTime":1543066550000,"favourNum":0,"hasFavoured":false,"id":2583467,"private":false,"shopId":1387968678,"showable":true,"status":1,"text":"这家店简直太棒了，作为学长我已经在这买了3年了，老板非常大气又努力，大家多多支持","top":false},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/passport-10fbd5094880e14c4c6a6a278742041f.jpg","userId":"1397400332","userNickName":"李修"},{"userHeadUrl":"https://si.geilicdn.com/passport-17178f0fa526eaa94933767f613434f5.jpg","userId":"1019213831","userNickName":"i"},{"userHeadUrl":"https://si.geilicdn.com/passport-254609e491217f183981e47db138b9d2.jpg","userId":"1350449796","userNickName":"姜三省"},{"userHeadUrl":"https://si.geilicdn.com/passport-4c6233a7406a33f287c87d8d01e8d1d5.jpg","userId":"1393953050","userNickName":"鸣"},{"userHeadUrl":"https://si.geilicdn.com/passport-08ee1eac7d0d83ebacc4698f2c75ea36.jpg","userId":"1394453678","userNickName":"西瓜味呀呀"}],"sayingNum":5,"sellerId":"1387968678","shopGrade":9,"shopId":"1387968678","shopLogo":"https://si.geilicdn.com/bj-wd-1387968678-1540297669350-1313925233_250_250.jpg?w=250&h=250","shopName":"天商零食小铺","shopUpdate":{"updateCount":1,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2660105006","itemLowPrice":390,"itemMainPic":"https://si.geilicdn.com/wdseller1387968678-20af00000167972df46c0a02685e_800_800.jpg","itemName":"真果粒(芦荟)","itemOriginalLowPrice":390,"itemOriginalPrice":390,"itemPointOriginalPrice":390,"itemPrice":390,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":12,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"1387968678","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]},"status":0,"tagList":[],"taobaoShopIcon":false,"tmallShopIcon":false},{"buyNum":0,"collectCount":0,"cpsIcon":"","fx":false,"groupNum":0,"isBaking":0,"isCraftsman":0,"isFarmer":0,"isGlobalShop":0,"isKitchen":0,"payedTotal":0,"recommendTags":[{"tag":"唠嗑必备","tagBgColor":"0xFFA015","tagId":"29011","tagName":"唠嗑必备"},{"tag":"舌尖之舞","tagBgColor":"0x21A791","tagId":"90241","tagName":"舌尖之舞"}],"regularCustomerNum":0,"repurRatePercent":"","saying":{"authorInfo":{"memberInfo":{"level":1,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":66.6},"userId":"209955598"},"commentNum":0,"createTime":1530221860000,"favourNum":0,"hasFavoured":false,"id":1322292,"private":false,"shopId":458765781,"showable":true,"status":2,"text":"蛋黄酥太好吃了，我们吃了还想吃，真的，货真价实","top":true,"topTime":1530514085000},"sayingAuthors":[{"userHeadUrl":"https://si.geilicdn.com/passport-73eee98200c15596224afa56bc404890.jpg","userId":"209955598","userNickName":"晓梅"},{"userHeadUrl":"https://si.geilicdn.com/udc_de88538078c1b9188daaaa6b2d633ed1.jpg","userId":"1252943655","userNickName":"LXY"},{"userHeadUrl":"https://si.geilicdn.com/passport-8c3de373e2cc18f105215193fa388899.jpg","userId":"214682281","userNickName":"梅小妞-塞丹迪👜"}],"sayingNum":3,"sellerId":"458765781","shopGrade":8,"shopId":"458765781","shopLogo":"https://si.geilicdn.com/bj-pc-458765781-1536725506391-681845654_640_640.jpg?w=640&h=640","shopName":"馋嘴猫咪零食小铺","status":0,"tagList":[{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10},{"data":"https://si.geilicdn.com/hz_img_5006000001668171031b0a02685e_77_30_unadjust.png","name":"cpn标","type":10}],"taobaoShopIcon":false,"tmallShopIcon":false}]}
     */

    private StatusBean status;
    private ResultBean result;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class StatusBean {
        /**
         * code : 0
         * message : OK
         * description :
         */

        private int code;
        private String message;
        private String description;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public static class ResultBean {
        private List<ShopsBean> shops;

        public List<ShopsBean> getShops() {
            return shops;
        }

        public void setShops(List<ShopsBean> shops) {
            this.shops = shops;
        }

        public static class ShopsBean {
            /**
             * buyNum : 0
             * collectCount : 0
             * cpsIcon :
             * fx : false
             * groupNum : 0
             * isBaking : 0
             * isCraftsman : 0
             * isFarmer : 0
             * isGlobalShop : 0
             * isKitchen : 0
             * payedTotal : 0
             * recommendTags : [{"tag":"老板在烟台","tagBgColor":"0x0097A7","tagId":"500002","tagName":"老板在烟台"}]
             * regularCustomerNum : 0
             * repurRatePercent :
             * sayingNum : 0
             * sellerId : 1024903213
             * shopGrade : 12
             * shopId : 1024903213
             * shopLogo : https://si.geilicdn.com/bj-vshop-1024903213-1537792340905-2071212837_992_992.jpg?w=992&h=992
             * shopName : 零食驿站南校店
             * shopUpdate : {"updateCount":3,"updateItems":[{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2659741737","itemLowPrice":450,"itemMainPic":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","itemName":"好益味340ml","itemOriginalLowPrice":450,"itemOriginalPrice":450,"itemPointOriginalPrice":450,"itemPrice":450,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":9,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"1024903213","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]}
             * status : 0
             * tagList : [{"data":"https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png","name":"cps标","type":10}]
             * taobaoShopIcon : false
             * tmallShopIcon : false
             * saying : {"authorInfo":{"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":39.94},"userId":"1302810931"},"commentNum":0,"createTime":1544176522000,"favourNum":0,"hasFavoured":false,"id":2821209,"private":false,"shopId":1272546125,"showable":true,"status":1,"text":"配送很快，在宿舍买东西超级方便，希望越做越好呀～","top":false}
             * sayingAuthors : [{"userHeadUrl":"https://si.geilicdn.com/passport-0f371ce37144d32024402980f0098f31.jpg","userId":"1119487402","userNickName":"Vera Liu"},{"userHeadUrl":"https://si.geilicdn.com/passport-ef8fff7ce4f04b213ddba1669363a255.jpg","userId":"1276984392","userNickName":"岑故味儿"},{"userHeadUrl":"https://si.geilicdn.com/bj-user-1338403851-1536669793486-945702721_2667_4000.jpg?w=160&h=160&cp=1","userId":"1338403851","userNickName":"小螃蟹"},{"userHeadUrl":"https://si.geilicdn.com/passport-5b42cd72997a15a1ec3d67af9211cd16.jpg","userId":"1302810931","userNickName":"婷婷🎈"},{"userHeadUrl":"https://si.geilicdn.com/passport-99e18933668d46fe4773b7cf8bd596d9.jpg","userId":"1382246607","userNickName":"是三水啊"},{"userHeadUrl":"https://si.geilicdn.com/passport-1e0e69d54a94ae557926299cc99a7cf2.jpg","userId":"1385781866","userNickName":"eiei"}]
             */

            private int buyNum;
            private int collectCount;
            private String cpsIcon;
            private boolean fx;
            private int groupNum;
            private int isBaking;
            private int isCraftsman;
            private int isFarmer;
            private int isGlobalShop;
            private int isKitchen;
            private int payedTotal;
            private int regularCustomerNum;
            private String repurRatePercent;
            private int sayingNum;
            private String sellerId;
            private int shopGrade;
            private String shopId;
            private String shopLogo;
            private String shopName;
            private ShopUpdateBean shopUpdate;
            private int status;
            private boolean taobaoShopIcon;
            private boolean tmallShopIcon;
            private SayingBean saying;
            private List<RecommendTagsBean> recommendTags;
            private List<TagListBean> tagList;
            private List<SayingAuthorsBean> sayingAuthors;

            public static class ShopUpdateBean {
                /**
                 * updateCount : 3
                 * updateItems : [{"adIcon":0,"buyNum":0,"isFx":false,"isPointPrice":0,"itemDiscount":0,"itemId":"2659741737","itemLowPrice":450,"itemMainPic":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","itemName":"好益味340ml","itemOriginalLowPrice":450,"itemOriginalPrice":450,"itemPointOriginalPrice":450,"itemPrice":450,"itemSkuList":[],"itemSoldout":0,"itemStatus":0,"itemStock":9,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"sellerId":"1024903213","spoor":"","taobaoShopIcon":false,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}]
                 */

                private int updateCount;
                private List<UpdateItemsBean> updateItems;

                public int getUpdateCount() {
                    return updateCount;
                }

                public void setUpdateCount(int updateCount) {
                    this.updateCount = updateCount;
                }

                public List<UpdateItemsBean> getUpdateItems() {
                    return updateItems;
                }

                public void setUpdateItems(List<UpdateItemsBean> updateItems) {
                    this.updateItems = updateItems;
                }

                public static class UpdateItemsBean {
                    /**
                     * adIcon : 0
                     * buyNum : 0
                     * isFx : false
                     * isPointPrice : 0
                     * itemDiscount : 0
                     * itemId : 2659741737
                     * itemLowPrice : 450
                     * itemMainPic : https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg
                     * itemName : 好益味340ml
                     * itemOriginalLowPrice : 450
                     * itemOriginalPrice : 450
                     * itemPointOriginalPrice : 450
                     * itemPrice : 450
                     * itemSkuList : []
                     * itemSoldout : 0
                     * itemStatus : 0
                     * itemStock : 9
                     * itemType : 1
                     * mcSafeSpu : false
                     * newItem : false
                     * pointItem : false
                     * sellerId : 1024903213
                     * spoor :
                     * taobaoShopIcon : false
                     * time : 0
                     * timingSold : false
                     * timingSoldDuration : 0
                     * tmallShopIcon : false
                     */

                    private int adIcon;
                    private int buyNum;
                    private boolean isFx;
                    private int isPointPrice;
                    private int itemDiscount;
                    private String itemId;
                    private int itemLowPrice;
                    private String itemMainPic;
                    private String itemName;
                    private int itemOriginalLowPrice;
                    private int itemOriginalPrice;
                    private int itemPointOriginalPrice;
                    private int itemPrice;
                    private int itemSoldout;
                    private int itemStatus;
                    private int itemStock;
                    private int itemType;
                    private boolean mcSafeSpu;
                    private boolean newItem;
                    private boolean pointItem;
                    private String sellerId;
                    private String spoor;
                    private boolean taobaoShopIcon;
                    private int time;
                    private boolean timingSold;
                    private int timingSoldDuration;
                    private boolean tmallShopIcon;
                    private List<?> itemSkuList;

                    public int getAdIcon() {
                        return adIcon;
                    }

                    public void setAdIcon(int adIcon) {
                        this.adIcon = adIcon;
                    }

                    public int getBuyNum() {
                        return buyNum;
                    }

                    public void setBuyNum(int buyNum) {
                        this.buyNum = buyNum;
                    }

                    public boolean isIsFx() {
                        return isFx;
                    }

                    public void setIsFx(boolean isFx) {
                        this.isFx = isFx;
                    }

                    public int getIsPointPrice() {
                        return isPointPrice;
                    }

                    public void setIsPointPrice(int isPointPrice) {
                        this.isPointPrice = isPointPrice;
                    }

                    public int getItemDiscount() {
                        return itemDiscount;
                    }

                    public void setItemDiscount(int itemDiscount) {
                        this.itemDiscount = itemDiscount;
                    }

                    public String getItemId() {
                        return itemId;
                    }

                    public void setItemId(String itemId) {
                        this.itemId = itemId;
                    }

                    public int getItemLowPrice() {
                        return itemLowPrice;
                    }

                    public void setItemLowPrice(int itemLowPrice) {
                        this.itemLowPrice = itemLowPrice;
                    }

                    public String getItemMainPic() {
                        return itemMainPic;
                    }

                    public void setItemMainPic(String itemMainPic) {
                        this.itemMainPic = itemMainPic;
                    }

                    public String getItemName() {
                        return itemName;
                    }

                    public void setItemName(String itemName) {
                        this.itemName = itemName;
                    }

                    public int getItemOriginalLowPrice() {
                        return itemOriginalLowPrice;
                    }

                    public void setItemOriginalLowPrice(int itemOriginalLowPrice) {
                        this.itemOriginalLowPrice = itemOriginalLowPrice;
                    }

                    public int getItemOriginalPrice() {
                        return itemOriginalPrice;
                    }

                    public void setItemOriginalPrice(int itemOriginalPrice) {
                        this.itemOriginalPrice = itemOriginalPrice;
                    }

                    public int getItemPointOriginalPrice() {
                        return itemPointOriginalPrice;
                    }

                    public void setItemPointOriginalPrice(int itemPointOriginalPrice) {
                        this.itemPointOriginalPrice = itemPointOriginalPrice;
                    }

                    public int getItemPrice() {
                        return itemPrice;
                    }

                    public void setItemPrice(int itemPrice) {
                        this.itemPrice = itemPrice;
                    }

                    public int getItemSoldout() {
                        return itemSoldout;
                    }

                    public void setItemSoldout(int itemSoldout) {
                        this.itemSoldout = itemSoldout;
                    }

                    public int getItemStatus() {
                        return itemStatus;
                    }

                    public void setItemStatus(int itemStatus) {
                        this.itemStatus = itemStatus;
                    }

                    public int getItemStock() {
                        return itemStock;
                    }

                    public void setItemStock(int itemStock) {
                        this.itemStock = itemStock;
                    }

                    public int getItemType() {
                        return itemType;
                    }

                    public void setItemType(int itemType) {
                        this.itemType = itemType;
                    }

                    public boolean isMcSafeSpu() {
                        return mcSafeSpu;
                    }

                    public void setMcSafeSpu(boolean mcSafeSpu) {
                        this.mcSafeSpu = mcSafeSpu;
                    }

                    public boolean isNewItem() {
                        return newItem;
                    }

                    public void setNewItem(boolean newItem) {
                        this.newItem = newItem;
                    }

                    public boolean isPointItem() {
                        return pointItem;
                    }

                    public void setPointItem(boolean pointItem) {
                        this.pointItem = pointItem;
                    }

                    public String getSellerId() {
                        return sellerId;
                    }

                    public void setSellerId(String sellerId) {
                        this.sellerId = sellerId;
                    }

                    public String getSpoor() {
                        return spoor;
                    }

                    public void setSpoor(String spoor) {
                        this.spoor = spoor;
                    }

                    public boolean isTaobaoShopIcon() {
                        return taobaoShopIcon;
                    }

                    public void setTaobaoShopIcon(boolean taobaoShopIcon) {
                        this.taobaoShopIcon = taobaoShopIcon;
                    }

                    public int getTime() {
                        return time;
                    }

                    public void setTime(int time) {
                        this.time = time;
                    }

                    public boolean isTimingSold() {
                        return timingSold;
                    }

                    public void setTimingSold(boolean timingSold) {
                        this.timingSold = timingSold;
                    }

                    public int getTimingSoldDuration() {
                        return timingSoldDuration;
                    }

                    public void setTimingSoldDuration(int timingSoldDuration) {
                        this.timingSoldDuration = timingSoldDuration;
                    }

                    public boolean isTmallShopIcon() {
                        return tmallShopIcon;
                    }

                    public void setTmallShopIcon(boolean tmallShopIcon) {
                        this.tmallShopIcon = tmallShopIcon;
                    }

                    public List<?> getItemSkuList() {
                        return itemSkuList;
                    }

                    public void setItemSkuList(List<?> itemSkuList) {
                        this.itemSkuList = itemSkuList;
                    }
                }
            }

            public static class SayingBean {
                /**
                 * authorInfo : {"memberInfo":{"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":39.94},"userId":"1302810931"}
                 * commentNum : 0
                 * createTime : 1544176522000
                 * favourNum : 0
                 * hasFavoured : false
                 * id : 2821209
                 * private : false
                 * shopId : 1272546125
                 * showable : true
                 * status : 1
                 * text : 配送很快，在宿舍买东西超级方便，希望越做越好呀～
                 * top : false
                 */

                private AuthorInfoBean authorInfo;
                private int commentNum;
                private long createTime;
                private int favourNum;
                private boolean hasFavoured;
                private int id;
                private boolean privateX;
                private int shopId;
                private boolean showable;
                private int status;
                private String text;
                private boolean top;

                public AuthorInfoBean getAuthorInfo() {
                    return authorInfo;
                }

                public void setAuthorInfo(AuthorInfoBean authorInfo) {
                    this.authorInfo = authorInfo;
                }

                public int getCommentNum() {
                    return commentNum;
                }

                public void setCommentNum(int commentNum) {
                    this.commentNum = commentNum;
                }

                public long getCreateTime() {
                    return createTime;
                }

                public void setCreateTime(long createTime) {
                    this.createTime = createTime;
                }

                public int getFavourNum() {
                    return favourNum;
                }

                public void setFavourNum(int favourNum) {
                    this.favourNum = favourNum;
                }

                public boolean isHasFavoured() {
                    return hasFavoured;
                }

                public void setHasFavoured(boolean hasFavoured) {
                    this.hasFavoured = hasFavoured;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public boolean isPrivateX() {
                    return privateX;
                }

                public void setPrivateX(boolean privateX) {
                    this.privateX = privateX;
                }

                public int getShopId() {
                    return shopId;
                }

                public void setShopId(int shopId) {
                    this.shopId = shopId;
                }

                public boolean isShowable() {
                    return showable;
                }

                public void setShowable(boolean showable) {
                    this.showable = showable;
                }

                public int getStatus() {
                    return status;
                }

                public void setStatus(int status) {
                    this.status = status;
                }

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public boolean isTop() {
                    return top;
                }

                public void setTop(boolean top) {
                    this.top = top;
                }

                public static class AuthorInfoBean {
                    /**
                     * memberInfo : {"level":0,"memberInfoH5Url":"https://vmspub.weidian.com/gaia/17448/c29ed0e7.html","score":39.94}
                     * userId : 1302810931
                     */

                    private MemberInfoBean memberInfo;
                    private String userId;

                    public MemberInfoBean getMemberInfo() {
                        return memberInfo;
                    }

                    public void setMemberInfo(MemberInfoBean memberInfo) {
                        this.memberInfo = memberInfo;
                    }

                    public String getUserId() {
                        return userId;
                    }

                    public void setUserId(String userId) {
                        this.userId = userId;
                    }

                    public static class MemberInfoBean {
                        /**
                         * level : 0
                         * memberInfoH5Url : https://vmspub.weidian.com/gaia/17448/c29ed0e7.html
                         * score : 39.94
                         */

                        private int level;
                        private String memberInfoH5Url;
                        private double score;

                        public int getLevel() {
                            return level;
                        }

                        public void setLevel(int level) {
                            this.level = level;
                        }

                        public String getMemberInfoH5Url() {
                            return memberInfoH5Url;
                        }

                        public void setMemberInfoH5Url(String memberInfoH5Url) {
                            this.memberInfoH5Url = memberInfoH5Url;
                        }

                        public double getScore() {
                            return score;
                        }

                        public void setScore(double score) {
                            this.score = score;
                        }
                    }
                }
            }

            public static class RecommendTagsBean {
                /**
                 * tag : 老板在烟台
                 * tagBgColor : 0x0097A7
                 * tagId : 500002
                 * tagName : 老板在烟台
                 */

                private String tag;
                private String tagBgColor;
                private String tagId;
                private String tagName;

                public String getTag() {
                    return tag;
                }

                public void setTag(String tag) {
                    this.tag = tag;
                }

                public String getTagBgColor() {
                    return tagBgColor;
                }

                public void setTagBgColor(String tagBgColor) {
                    this.tagBgColor = tagBgColor;
                }

                public String getTagId() {
                    return tagId;
                }

                public void setTagId(String tagId) {
                    this.tagId = tagId;
                }

                public String getTagName() {
                    return tagName;
                }

                public void setTagName(String tagName) {
                    this.tagName = tagName;
                }
            }

            public static class TagListBean {
                /**
                 * data : https://si.geilicdn.com/hz_img_3d0700000166801262880a028841_115_30_unadjust.png
                 * name : cps标
                 * type : 10
                 */

                private String data;
                private String name;
                private int type;

                public String getData() {
                    return data;
                }

                public void setData(String data) {
                    this.data = data;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public int getType() {
                    return type;
                }

                public void setType(int type) {
                    this.type = type;
                }
            }

            public static class SayingAuthorsBean {
                /**
                 * userHeadUrl : https://si.geilicdn.com/passport-0f371ce37144d32024402980f0098f31.jpg
                 * userId : 1119487402
                 * userNickName : Vera Liu
                 */

                private String userHeadUrl;
                private String userId;
                private String userNickName;

                public String getUserHeadUrl() {
                    return userHeadUrl;
                }

                public void setUserHeadUrl(String userHeadUrl) {
                    this.userHeadUrl = userHeadUrl;
                }

                public String getUserId() {
                    return userId;
                }

                public void setUserId(String userId) {
                    this.userId = userId;
                }

                public String getUserNickName() {
                    return userNickName;
                }

                public void setUserNickName(String userNickName) {
                    this.userNickName = userNickName;
                }
            }
        }
    }
}
