package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: UnreadMessageResultVO.java, v 0.1 2018年02月18日 上午8:26 tangdu Exp $
 */
@Getter
@Setter
public class UnreadCatalogResultVO {

    private List<Object> catalogs_to_mids;
}
