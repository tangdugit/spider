package com.tdu.spider.biz.service.same.vo;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

public class SameImResult<T> implements Serializable {
    private Integer code;

    private T       recv;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public T getRecv() {
        return recv;
    }

    public void setRecv(T recv) {
        this.recv = recv;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
