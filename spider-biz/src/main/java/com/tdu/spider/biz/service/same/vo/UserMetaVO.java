package com.tdu.spider.biz.service.same.vo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.List;


public class UserMetaVO implements Serializable {
    private Boolean password_set;
    private String lbs;
    private String sex_ort;
    private Integer verified;
    private Integer sex;
    private Integer city_id;
    private String latest_area_change_at;
    private Long firstblood;
    private List<UserLabel> verified_identities;
    private Firstlove firstlove;

    public Boolean getPassword_set() {
        return password_set;
    }

    public void setPassword_set(Boolean password_set) {
        this.password_set = password_set;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getCity_id() {
        return city_id;
    }

    public void setCity_id(Integer city_id) {
        this.city_id = city_id;
    }

    public String getLatest_area_change_at() {
        return latest_area_change_at;
    }

    public void setLatest_area_change_at(String latest_area_change_at) {
        this.latest_area_change_at = latest_area_change_at;
    }

    public Long getFirstblood() {
        return firstblood;
    }

    public void setFirstblood(Long firstblood) {
        this.firstblood = firstblood;
    }

    public List<UserLabel> getVerified_identities() {
        return verified_identities;
    }

    public void setVerified_identities(List<UserLabel> verified_identities) {
        this.verified_identities = verified_identities;
    }

    public Firstlove getFirstlove() {
        return firstlove;
    }

    public void setFirstlove(Firstlove firstlove) {
        this.firstlove = firstlove;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    class  UserLabel implements Serializable{
        private String title;
        private String bg_color;
        private String url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBg_color() {
            return bg_color;
        }

        public void setBg_color(String bg_color) {
            this.bg_color = bg_color;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
        }

    }
    class Firstlove implements Serializable{
        private Long love_user_id;
        private Long sense_id;

        public Long getLove_user_id() {
            return love_user_id;
        }

        public void setLove_user_id(Long love_user_id) {
            this.love_user_id = love_user_id;
        }

        public Long getSense_id() {
            return sense_id;
        }

        public void setSense_id(Long sense_id) {
            this.sense_id = sense_id;
        }
        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
        }

    }
}
