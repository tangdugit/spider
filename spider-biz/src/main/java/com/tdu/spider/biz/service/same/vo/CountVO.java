package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: CountVO.java, v 0.1 2018年01月24日 下午3:32 tangdu Exp $
 */
@Setter
@Getter
public class CountVO {

    private Integer count;
}
