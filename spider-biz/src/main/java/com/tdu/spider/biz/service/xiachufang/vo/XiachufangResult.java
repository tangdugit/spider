package com.tdu.spider.biz.service.xiachufang.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: XiachufangResult.java, v 0.1 2018年01月27日 上午9:06 tangdu Exp $
 */
@Setter
@Getter
public class XiachufangResult<T> implements Serializable {

    private String status;

    private T content;

}
