package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SenseLikeVO.java, v 0.1 2018年02月01日 下午4:47 tangdu Exp $
 */
@Setter
@Getter
public class SenseLikeVO implements Serializable{
    private Long id;
    private String username;
    private String avatar;
    private Integer created_at;

}
