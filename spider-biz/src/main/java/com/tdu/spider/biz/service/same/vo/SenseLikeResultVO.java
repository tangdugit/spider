package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SenseLikeResultVO.java, v 0.1 2018年02月01日 下午4:56 tangdu Exp $
 */
@Getter
@Setter
public class SenseLikeResultVO {

    private List<SenseLikeVO>  results;
}
