package com.tdu.spider.biz.service.same.vo;

import com.tdu.spider.model.SameChannelDO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: ChannelResultVO.java, v 0.1 2018年02月21日 下午2:08 tangdu Exp $
 */
@Setter
@Getter
public class ChannelResultVO {
    private List<SameChannelDO> results;
}
