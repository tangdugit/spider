package com.tdu.spider.biz.service.zhihu.vo;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: QuestionVO.java, v 0.1 2017年04月2017/4/21日 下午11:40 tangdu Exp $
 * @name TODO: QuestionVO
 */
public class QuestionVO {
    private Long    created;
    private String  url;
    private String  title;
    private Long    updated_time;
    private String  type;
    private Long    id;

    private String  content;
    private Integer comment_count;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getComment_count() {
        return comment_count;
    }

    public void setComment_count(Integer comment_count) {
        this.comment_count = comment_count;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Long updated_time) {
        this.updated_time = updated_time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
