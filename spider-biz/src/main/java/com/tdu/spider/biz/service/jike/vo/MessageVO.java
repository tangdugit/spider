package com.tdu.spider.biz.service.jike.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MessageVO {
    private String  id;

    private String  content;
    private String  title;
    private Long    messageId;
    private Integer topicId;
    private String  linkUrl;
    private String  videoLink;
    private String  sourceRawValue;
    private String  iconUrl;
    private Integer collectCount;
    private Integer commentCount;
    private Integer popularity;
    private String  author;
    private boolean withPush;
    private boolean read;
    private boolean collected;
    private String  createdAt;
    private String  updatedAt;
    private TopicVO topic;

}
