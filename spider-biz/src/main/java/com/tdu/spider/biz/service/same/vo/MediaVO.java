package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

@Setter
@Getter
public class MediaVO implements Serializable {

    private MovieVO movie;

    private VideoVO video;

    private SoundVO sound;

    private MusicVO music;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
