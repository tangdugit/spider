package com.tdu.spider.biz.service.douban;

import com.tdu.spider.dao.MovieInfoRepository;
import com.tdu.spider.dao.MovieResourceRepository;
import com.tdu.spider.model.MovieResourceDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieService.java, v 0.1 2018年04月15日 上午7:16 tangdu Exp $
 */
@Service
public class MovieResourceService {
    @Autowired
    private MovieResourceRepository movieResourceRepository;

    @Autowired
    private MovieInfoRepository movieInfoRepository;


    public MovieResourceDO findByDoubanId(String doubanId){
        return movieResourceRepository.findByDoubanId(doubanId);
    }


    public void spiderResource(){

    }
}
