package com.tdu.spider.biz.service.huocaihe.vo;


import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TopicDataVO implements Serializable {
    private List<HuoTopicVO> list;
}
