package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: OrderResultVO.java, v 0.1 2018年02月18日 下午9:01 tangdu Exp $
 */
@Getter
@Setter
public class OrderResultVO {
    private Long user_id;
    private Long amount_id;
    private Integer amount;
    private Long created_at;
    private Integer status;
    private String ip;
    private String draw_account_name;
    private String draw_account_bank_id;
    private String draw_account_card_no;
    private Integer draw_account_type;
    private String draw_notice_mobilel;
    private Long package_id;
    private Long id;
    private Long payment_id;
}
