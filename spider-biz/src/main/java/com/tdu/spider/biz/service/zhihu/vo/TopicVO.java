package com.tdu.spider.biz.service.zhihu.vo;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: ToppicVO.java, v 0.1 2017年04月2017/4/21日 下午11:55 tangdu Exp $
 * @name TODO: ToppicVO
 */
public class TopicVO {

    private Long id;

    private String name;

    private String photo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
