package com.tdu.spider.biz.service.weidian;

import com.google.common.collect.Lists;

import java.util.List;

public class ItemPageQuery {
    /**
     * tags : []
     * page : 1
     * limit : 10
     * order : desc
     * sortKey : default
     * keyWord : 三明治
     * coreWord :
     * tagSwitch : true
     * navigationSwitch : true
     * source : smart_search
     * businessId : 216
     * viewModel : 1
     */

    private int page=1;
    private int limit=100;
    private String order="desc";
    private String sortKey="soldout";
    private String keyWord;
    private String coreWord;
    private boolean tagSwitch=true;
    private boolean navigationSwitch=true;
    private String source="smart_search";
    private int businessId=216;
    private int viewModel=1;
    private List<?> tags= Lists.newArrayList();

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSortKey() {
        return sortKey;
    }

    public void setSortKey(String sortKey) {
        this.sortKey = sortKey;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getCoreWord() {
        return coreWord;
    }

    public void setCoreWord(String coreWord) {
        this.coreWord = coreWord;
    }

    public boolean isTagSwitch() {
        return tagSwitch;
    }

    public void setTagSwitch(boolean tagSwitch) {
        this.tagSwitch = tagSwitch;
    }

    public boolean isNavigationSwitch() {
        return navigationSwitch;
    }

    public void setNavigationSwitch(boolean navigationSwitch) {
        this.navigationSwitch = navigationSwitch;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId) {
        this.businessId = businessId;
    }

    public int getViewModel() {
        return viewModel;
    }

    public void setViewModel(int viewModel) {
        this.viewModel = viewModel;
    }

    public List<?> getTags() {
        return tags;
    }

    public void setTags(List<?> tags) {
        this.tags = tags;
    }
}
