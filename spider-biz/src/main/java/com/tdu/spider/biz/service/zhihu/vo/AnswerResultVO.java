package com.tdu.spider.biz.service.zhihu.vo;

import java.util.List;


public class AnswerResultVO {

    private Page           paging;
    private List<AnswerVO> data;

    public Page getPaging() {
        return paging;
    }

    public void setPaging(Page paging) {
        this.paging = paging;
    }

    public List<AnswerVO> getData() {
        return data;
    }

    public void setData(List<AnswerVO> data) {
        this.data = data;
    }

    private class Page {
        private Boolean is_end;
        private Integer totals;
        private Boolean is_start;
        private String  previous;
        private String  next;

        public Boolean getIs_end() {
            return is_end;
        }

        public void setIs_end(Boolean is_end) {
            this.is_end = is_end;
        }

        public Integer getTotals() {
            return totals;
        }

        public void setTotals(Integer totals) {
            this.totals = totals;
        }

        public Boolean getIs_start() {
            return is_start;
        }

        public void setIs_start(Boolean is_start) {
            this.is_start = is_start;
        }

        public String getPrevious() {
            return previous;
        }

        public void setPrevious(String previous) {
            this.previous = previous;
        }

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }
    }
}
