package com.tdu.spider.biz.service.xiachufang;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.tdu.spider.biz.service.ProxyService;
import com.tdu.spider.biz.service.xiachufang.vo.FeedsDataVO;
import com.tdu.spider.biz.service.xiachufang.vo.FeedsVO;
import com.tdu.spider.biz.service.xiachufang.vo.XiachufangResult;
import com.tdu.spider.biz.util.HttpUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.tdu.spider.biz.util.LogUtils.curtail;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: XiachufangService.java, v 0.1 2018年01月27日 上午9:14 tangdu Exp $
 */
@Service
public class XiachufangService {

    @Autowired
    private ProxyService proxyService;

    private static Logger logger = LoggerFactory.getLogger(XiachufangService.class);
    private static List<Header> headers = Lists.newArrayList();

    ThreadLocal<HttpHost> hostThreadLocal=new ThreadLocal<>();

    static {
        headers.add(new BasicHeader("Cookie", "bid=IFM0WL3E; Hm_lvt_ecd4feb5c351cc02583045a5813b5142=1528616518; __utmc=177678124; __utmz=177678124.1528616519.1.1.utmcsr=baidu|utmccn=(organic)|utmcmd=organic; gr_user_id=b0295646-3581-4b6f-b27c-803f30705a4a; gr_session_id_8187ff886f0929da=a64a4f66-4fb2-4be9-9dbe-dc4e7ee83533_true; __utma=177678124.531925403.1528616519.1528616519.1528644420.2; S=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mjg2NzM4NDIsImp0aSI6MTE0ODY1OTUsInVpZCI6MTE3OTAxMzAxLCJvIjowfQ.WdEdp1aH_YwXlJ_ooqC-deyRrpjsXZJCiGgubJJWjZY; Hm_lpvt_ecd4feb5c351cc02583045a5813b5142=1528648462; __utmt=1; __utmb=177678124.136.9.1528648462564"));
        headers.add(new BasicHeader("Accept-Language", "zh-Hans-CN;q=1"));
        headers.add(new BasicHeader("Accept-Encoding", "br, gzip, deflate"));
        headers.add(new BasicHeader("X-Requested-With", "XMLHttpRequest"));
        headers.add(new BasicHeader("User-Agent", "Mozilla/5.0 (iPhone; CPU iOS 11.2.5 like Mac OS X) xiachufang-iphone/6.4.4 Build/163"));
    }


    public FeedsDataVO queryFrieldFeeds() throws Exception {
        String url = "http://www.xiachufang.com/activity/async_feeds/?start=0&limit=20&format=json";
        Request request = Request.Get(url).setHeaders(headers.toArray(new Header[]{}));
        if(hostThreadLocal.get()!=null){
            request.viaProxy(hostThreadLocal.get());
        }

        try {
            for (int i = 0; i < 5; i++) {
                Response response = Executor.newInstance(HttpUtils.httpClient()).execute(request);
                String   content  = response.returnContent().asString();
                logger.info("queryFeeds url:{},params:{},result:{}", url, "", curtail(content));

                if (StringUtils.hasLength(content)) {
                    XiachufangResult<FeedsDataVO> result = JSON.parseObject(content, new TypeReference<XiachufangResult<FeedsDataVO>>() {
                    });
                    return result.getContent();
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<String> queryHotFeeds(Integer page) throws Exception {
        List<String> list     = Lists.newArrayList();
        String       url      = "http://www.xiachufang.com/activity/site/?page="+page;
        Request      request  = Request.Get(url).setHeaders(headers.toArray(new Header[]{}));
        if(hostThreadLocal.get()!=null){
            request.viaProxy(hostThreadLocal.get());
        }
        try {
            for (int i = 0; i < 2; i++) {
                Response     response = Executor.newInstance(HttpUtils.httpClient()).execute(request);
                String       content  = response.returnContent().asString();
                logger.info("queryHotFeeds url:{},params:{},result:{}", url, page,"true");
                if (!StringUtils.hasLength(content)) {
                    return null;
                }
                Elements elements = Jsoup.parse(content).select("div.ias-container div.has-border");
                for (Element element : elements) {
                    if (element.hasAttr("data-id")) {
                        list.add((element.attr("data-id")));
                    }
                }
                return list;
            }
        } catch (Exception e) {
        }
        return list;
    }

    public void digg(String feedId) throws Exception {
        String              url        = "http://www.xiachufang.com/dish/" + feedId + "/digg/";
        List<NameValuePair> valuePairs = Lists.newArrayList();
        valuePairs.add(new BasicNameValuePair("xf", "JWjZY"));
        Request  request  = Request.Post(url).bodyForm(valuePairs).setHeaders(headers.toArray(new Header[]{}));
        if(hostThreadLocal.get()!=null){
            request.viaProxy(hostThreadLocal.get());
        }

        for (int i = 0; i < 3; i++) {
            try {
                Response response = Executor.newInstance(HttpUtils.httpClient()).execute(request);
                String   content  = response.returnContent().asString();
                logger.info("digg_success url:{},params:{},result:{}", url, "", curtail(content));
                break;
            } catch (Exception e) {
                logger.error("digg_error url:{},params:{},result:{}", url, feedId, "false");
            }
        }
    }


    public void autoHotDdigg() throws Exception {
        long         start = System.currentTimeMillis();
        checkHost();
        List<String> list  =Lists.newArrayList();
        for (int i = 0; i < 3; i++) {
            list.addAll(queryHotFeeds(i+1));
        }
        if (list == null) {
            logger.info("autoHotDdigg.空数据");
        }else{
            logger.info("autoHotDdigg.任务池:{}",list.size());
        }
        for (String aLong : list) {
            TimeUnit.SECONDS.sleep(2L);
            digg(aLong);
        }
        logger.info("autoDdigg success cost:{}", (System.currentTimeMillis() - start) / 100);
    }


    public void autoDdigg() throws Exception {
        long        start       = System.currentTimeMillis();
        checkHost();
        FeedsDataVO feedsDataVO = queryFrieldFeeds();
        if (feedsDataVO == null || CollectionUtils.isEmpty(feedsDataVO.getFeeds())) {
            logger.info("autoDdigg.空数据");
        }
        for (FeedsVO feedsVO : feedsDataVO.getFeeds()) {
            TimeUnit.MICROSECONDS.sleep(60L);
            if (feedsVO.getTarget() == null) {
                continue;
            }
            digg(feedsVO.getTarget().getId());
        }
        logger.info("autoDdigg success cost:{}", (System.currentTimeMillis() - start) / 100);
    }


    public void checkHost(){
        boolean flag=false;
        for (int i = 0; i < 5; i++) {
            HttpHost     httpProxy = proxyService.getHttpProxy();
            if(httpProxy!=null) {
                hostThreadLocal.set(httpProxy);
                try {
                    queryFrieldFeeds();
                    flag=true;
                    break;
                } catch (Exception e) {
                    hostThreadLocal.remove();
                    logger.error("不可用代理-> {}",httpProxy);
                }
            }
        }
        if(flag){
            logger.info("可用代理-> {}",hostThreadLocal.get());
        }
        Preconditions.checkState(flag,"没有可用端口.");
    }
}
