package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: UserChatHistoryMedia.java, v 0.1 2018年02月18日 下午7:26 tangdu Exp $
 */
@Setter
@Getter
public class UserChatHistoryMedia {
    private String txt;

    private SenSeVO sense;

    private StickerScaleVO sticker_scale;

    private StickerVO sticker;


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
