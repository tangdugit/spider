package com.tdu.spider.biz.service.movie;

import com.google.common.collect.Lists;
import com.tdu.spider.biz.util.ConvertUtils;
import com.tdu.spider.dao.tv.CategoryRepository;
import com.tdu.spider.dao.tv.MediaRepository;
import com.tdu.spider.model.tv.CategoryDO;
import com.tdu.spider.model.tv.MediaDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieApiService.java, v 0.1 2018年07月11日 下午11:12 tangdu Exp $
 */
@Service
public class MovieApiService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MediaRepository mediaRepository;

    public List<MovieInfoVO> getMainMovieList() {
        List<MovieInfoVO> data = Lists.newArrayList();
        Sort              sort = new Sort(Sort.Direction.ASC, "sno");
        List<CategoryDO>  list = categoryRepository.findAll(sort);
        for (CategoryDO categoryDO : list) {
            MovieInfoVO movieInfoVO = new MovieInfoVO();
            movieInfoVO.setCategoryCode(categoryDO.getCategoryCode());
            movieInfoVO.setCategoryTitle(categoryDO.getCategoryTitle());
            Pageable      pageable = new PageRequest(0, 20);
            Page<MediaDO> mediaDOS = mediaRepository.findByCategoryCode(categoryDO.getCategoryCode(), pageable);
            List<MediaVO> result   = ConvertUtils.copyList(mediaDOS.getContent(), MediaVO.class);
            movieInfoVO.setMediaModelList(result);
            data.add(movieInfoVO);
        }
//        MovieInfoVO       movieInfoVO = new MovieInfoVO();
//        movieInfoVO.setCategoryTitle("本周榜单");
//        List<MediaVO> mediaVOS = Lists.newArrayList();
//        MediaVO       mediaVO  = new MediaVO();
//        mediaVO.setContent("狐闹干探狐闹干探狐闹干探狐闹干探");
//        mediaVO.setImageUrl(Lists.newArrayList("https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2519070834.webp"));
//        mediaVO.setTitle("狐闹干探");
//        mediaVO.setDirection("麦兜");
//        mediaVO.setType(1);
//        mediaVO.setYear(2019);
//        mediaVO.setVideoUrl("http://vip.yingshidaqian.com/20180712/IBE5TQSU/index.m3u8");
//        mediaVOS.add(mediaVO);
//
//        MovieInfoVO movieInfoVO1 = new MovieInfoVO();
//        movieInfoVO1.setCategoryTitle("豆瓣精选");
//        List<MediaVO> mediaVOS1 = Lists.newArrayList();
//        MediaVO       mediaVO1  = new MediaVO();
//        mediaVO1.setContent("狐闹干探狐闹干探狐闹干探狐闹干探");
//        mediaVO1.setImageUrl(Lists.newArrayList("https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2519070834.webp"));
//        mediaVO1.setTitle("狐闹干探");
//        mediaVO1.setDirection("麦兜");
//        mediaVO1.setType(1);
//        mediaVO1.setYear(2019);
//        mediaVO1.setVideoUrl("http://vip.yingshidaqian.com/20180712/IBE5TQSU/index.m3u8");
//        mediaVOS1.add(mediaVO);
//
//        movieInfoVO1.setMediaModelList(mediaVOS1);
//        movieInfoVO.setMediaModelList(mediaVOS);
//        list.add(movieInfoVO);
//        list.add(movieInfoVO1);
        return data;
    }

    public List<CategoryVO> getCategory() {
        Sort             sort   = new Sort(Sort.Direction.ASC, "sno");
        List<CategoryDO> list   = categoryRepository.findAll(sort);
        List<CategoryVO> result = ConvertUtils.copyList(list, CategoryVO.class);
        return result;
    }

}
