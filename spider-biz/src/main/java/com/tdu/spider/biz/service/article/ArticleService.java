package com.tdu.spider.biz.service.article;

import com.tdu.spider.biz.service.article.vo.ArtcleTag;
import com.tdu.spider.biz.service.article.vo.ArticleQueryVO;
import com.tdu.spider.biz.service.article.vo.ArticleVO;
import com.tdu.spider.biz.service.article.vo.Page;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ArticleService {
    public Page<ArticleVO> page(ArticleQueryVO photoQueryVO) {
        return null;
    }

    public ArticleVO view(Long articleId) {
        return null;
    }

    public List<ArtcleTag> tags() {
        return null;
    }

    public List<ArticleVO> rList(Long articleId) {
        return null;
    }
}
