package com.tdu.spider.biz.service.same.vo;
import com.tdu.spider.model.SameChannelDO;
import lombok.Data;

import java.util.List;


@Data
public class SameSenseQueryVO {

    private Long userId;

    private Long channelId;

    private List<SameChannelDO> channelInfoVOS;

    private Long          lastSenenId;

    /**分页偏移量**/
    private Long          offset;

    /**设计分页数**/
    private Integer totalPage;

    //抓取占赞数
    private Integer filterLike;

    //抓取页数
    private Integer filterPage;

}
