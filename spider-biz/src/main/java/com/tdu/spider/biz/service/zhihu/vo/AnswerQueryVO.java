package com.tdu.spider.biz.service.zhihu.vo;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: AnswerQueryVO.java, v 0.1 2017年04月2017/4/13日 上午8:21 tangdu Exp $
 * @name TODO: AnswerQueryVO
 */
public class AnswerQueryVO {

    private Long    topicId;

    private Integer page;

    private Long    questionId;

    private String  authCode;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }
}
