package com.tdu.spider.biz.service.douban.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieInfoVO.java, v 0.1 2018年04月15日 上午7:09 tangdu Exp $
 */
@Setter
@Getter
public class DouMovieInfoResultVO {
   private Integer count;
   private Integer total;
   private Integer start;
   private List<DouMovieInfoVO> subjects;
}
