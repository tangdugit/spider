package com.tdu.spider.biz.service.same.vo;

import com.tdu.spider.model.SameChannelDO;
import lombok.Data;

import java.util.List;

@Data
public class LikeSenseQueryVO {
    private Long userId;

    private Long channelId;

    /**点赞数**/
    private Integer filterLikes=4;

    List<SameChannelDO> channelInfoVOS;

    public LikeSenseQueryVO() {

    }
    public LikeSenseQueryVO(Long userId, List<SameChannelDO> channelInfoVOS) {
        this.userId = userId;
        this.channelInfoVOS = channelInfoVOS;
    }
}
