package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: MessageResultVO.java, v 0.1 2017年04月2017/4/21日 上午11:19 tangdu Exp $
 * @name TODO: MessageResultVO
 */
@Setter
@Getter
public class SendMessageVO {
    private String cmd;
    private Integer op = 1;
    private MessageBody body;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    @Setter
    @Getter
    public class MessageBody implements Serializable {
        private Long    fuid;
        private Long    time;
        private String  sender_name;
        private String  msg;
        private Integer type;
        private Integer is_active;
        private String  seq;
        private String  tuid;
        private String  mid;
        private Integer union;

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
        }

    }
}
