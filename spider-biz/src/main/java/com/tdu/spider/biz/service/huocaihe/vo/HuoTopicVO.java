package com.tdu.spider.biz.service.huocaihe.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class HuoTopicVO implements Serializable {
    String topic_id;
    String topic_name;
    String topic_type;
    String  view;
    String user_id;
    String user_name;
    String thread_count;

}
