package com.tdu.spider.biz.task;

import com.google.common.collect.Lists;
import com.tdu.spider.biz.service.wsloan.WsLoanApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 温商贷每日任务
 */
@Component
public class DaliyWsLoanTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(DaliyWsLoanTask.class);

    @Autowired
    private WsLoanApiService wsLoanApiService;

    /**
     * http://appapi.wsloan.com/V3/Others/FinishPageAD?TDID=h6f797654e868f96c01fee485c340febb&bundleid=com.wsloan&channelid=AppStore&comefrom=1&sign=7d2a7b121c2d3a2a9e6394553c938704&timestamp=1528694223&type=14&udid=f9614ca26305763fd38ae68a4678655da9008d6b030b2fbf9ec30c22c09854fd&uuid=a1GTivN2kFXl9l%2FQ2h5Fxw%3D%3D&ver=4708
     * 每天8点执行任务
     */
    @Scheduled(cron="0 0 8 * * ?")
    public void execute() {
        LOGGER.info("Scheduled.DaliyWsLoanTask 定时任务:");
        try {
            List<String> userIds= Lists.newArrayList(
                    "OTUaqkQeIIvvwhoDzqE+6w==",//18516280229
                    "a1GTivN2kFXl9l/Q2h5Fxw==",      //17557292868
                    "FOePDIzii8e8i4nqyJMIVw==",       //13282806286
                    "HJHenuR9ZK7/odK72CLCCw=="   //13216609495
            );
            for (String userId : userIds) {
                wsLoanApiService.qiandao(userId);
                wsLoanApiService.choujiang(userId);
                wsLoanApiService.choujiangAfter(userId);
            }
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyWsLoanTask.Error ",e);
        }
    }
}
