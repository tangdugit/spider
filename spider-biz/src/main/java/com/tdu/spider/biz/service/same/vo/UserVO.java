package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: UserVO.java, v 0.1 2017年04月2017/4/9日 上午9:07 tangdu Exp $
 * @name TODO: UserVO
 */
@Setter
@Getter
public class UserVO implements Serializable {
    private Long id;
    private String username;
    private String avatar;
    private Long created_at;
    private String latest;
    private String timezone;
    private Boolean is_active;
    /**1男，2女**/
    private Integer sex;
    private String token;
    private String auth_token;
    private String mobile;

    private UserMetaVO meta;
    private String email;
    private String is_download;//是否可以下载图片
    private String push_token;

    private Integer senses;//文章数
    private Long join_at;
    private Integer channels;//频道数
    private Integer bobo_value;


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
