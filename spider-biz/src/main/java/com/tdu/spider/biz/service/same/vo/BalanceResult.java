package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: BalanceVO.java, v 0.1 2018年01月27日 上午10:38 tangdu Exp $
 */
@Getter
@Setter
public class BalanceResult implements Serializable {
    private Long userId;

    private Integer balance;

}
