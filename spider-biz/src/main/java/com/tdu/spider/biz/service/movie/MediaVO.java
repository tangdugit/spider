package com.tdu.spider.biz.service.movie;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MediaModel.java, v 0.1 2018年07月11日 下午11:13 tangdu Exp $
 */
@Data
public class MediaVO {
    private int id;
    //电影标题
    private String title;
    //电影简介
    private String content;
    //封图
    private String cover;
    //电影图片
    private List<String> imageUrl;
    //电影路径
    private String videoUrl;
    //电影类型(1:电影,2:美剧,3:连续剧)
    private int type;
    //电影年份
    private int year;
    //电影产地
    private String belong;
    //电影导演
    private String direction;
    //电影主演
    private String lead;
    //电影时长(秒)
    private int totalTime;
    //当前观看时间(秒)
    private int currentViewTime;

}
