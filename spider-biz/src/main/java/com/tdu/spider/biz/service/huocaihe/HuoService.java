package com.tdu.spider.biz.service.huocaihe;

import com.google.common.collect.Lists;
import com.tdu.spider.biz.service.huocaihe.vo.*;
import com.tdu.spider.dao.HuoArcitleRepository;
import com.tdu.spider.dao.HuoTopicDORepository;
import com.tdu.spider.model.HuoArcitleDO;
import com.tdu.spider.model.HuoTopicDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class HuoService{
    private static final Logger LOGGER= LoggerFactory.getLogger(HuoService.class);

    @Autowired
    private HuoApiService        huoApiService;

    @Autowired
    private HuoTopicDORepository huoTopicDORepository;

    @Autowired
    private HuoArcitleRepository huoArcitleRepository;

    
    public Boolean batchSpiderTopic(HuoQueryVO queryVO) throws Exception {
        List<String> categorys = Lists.newArrayList("1", "2", "3", "4", "5", "6", "7", "8", "9",
            "10", "11", "12", "13", "14", "15", "16", "17", "18", "19");
        for (String category : categorys) {
            queryVO.setCategory_id(category);
            boolean f = true;
            while (f) {
                TimeUnit.MILLISECONDS.sleep(1000);
                List<HuoTopicVO> huoTopicVOList = huoApiService.queryTopic(queryVO);
                if (!CollectionUtils.isEmpty(huoTopicVOList)) {
                    List<HuoTopicVO> huoTopicVOListNew= Lists.newArrayList();
                    for (HuoTopicVO huoTopicVO : huoTopicVOList) {
                        if(Integer.valueOf(huoTopicVO.getThread_count())>2000){
                            huoTopicVOListNew.add(huoTopicVO);
                        }
                    }
                    huoTopicDORepository.saveAll(formate(huoTopicVOListNew));
                    queryVO.setLastid(huoTopicVOList.get(huoTopicVOList.size() - 1).getTopic_id());
                } else {
                    f = false;
                }
            }
            TimeUnit.SECONDS.sleep(1);
            queryVO.setLastid(null);
        }
        return true;
    }

    
    public Boolean batchSpiderArcitle(HuoQueryVO queryVO) throws Exception {
        for(int j=0;j<12;j++) {
            Pageable pageable = PageRequest.of(j, 100);
            Page<HuoTopicDO> topicDOPage = huoTopicDORepository.findAll(pageable);
            for (HuoTopicDO topicDO : topicDOPage.getContent()) {
                queryVO.setTopic_id(topicDO.getTopic_id() + "");
                for (int i = 0; i < 100; i++) {
                    TimeUnit.MILLISECONDS.sleep(1000);
                    queryVO.setP(i + "");
                    HuoArcitleVO huoArcitleVO = huoApiService.queryArticle(queryVO);
                    List<HuoArcitleDO> huoArcitleDOS = formateInfo(huoArcitleVO);
                    if (!CollectionUtils.isEmpty(huoArcitleDOS)) {
                        huoArcitleRepository.saveAll(huoArcitleDOS);
                    } else {
                        LOGGER.info("spider-{}-end",topicDO.getTopic_name());
                        break;
                    }
                }
                LOGGER.info("spider-{}-run",topicDO.getTopic_name());
                TimeUnit.SECONDS.sleep(5);
            }
            TimeUnit.MINUTES.sleep(1);
        }
        return true;
    }

    public List<HuoArcitleDO> formateInfo(HuoArcitleVO huoArcitleVO) {
        if (huoArcitleVO == null || CollectionUtils.isEmpty(huoArcitleVO.getList())) {
            return Lists.newArrayList();
        }
        List<HuoArcitleDO> list = Lists.newArrayList();
        for (HuoArcitleListVO huoArcitleList : huoArcitleVO.getList()) {
            HuoArcitleInfoVO huoArcitleListInfo = huoArcitleList.getInfo();
            HuoArcitleDO huoArcitleDO = new HuoArcitleDO();
            huoArcitleDO.setAuthor(huoArcitleListInfo.getAuthor());
            huoArcitleDO.setAuthor_id(Long.valueOf(huoArcitleListInfo.getAuthor_id()));
            huoArcitleDO.setAvatar(huoArcitleListInfo.getAvatar());
            huoArcitleDO.setComment(Integer.valueOf(huoArcitleListInfo.getComment()));
            huoArcitleDO.setContent(huoArcitleListInfo.getContent());
            huoArcitleDO.setCreated(huoArcitleListInfo.getCreated());
            huoArcitleDO.setForward(Integer.valueOf(huoArcitleListInfo.getForward()));
            huoArcitleDO.setHeart(Integer.valueOf(huoArcitleListInfo.getHeart()));
            huoArcitleDO.setHeight(huoArcitleListInfo.getHeight());
            huoArcitleDO.setId(Long.valueOf(huoArcitleListInfo.getId()));
            huoArcitleDO.setIs_fav(Integer.valueOf(huoArcitleListInfo.getIs_fav()));
            huoArcitleDO.setIs_like(Integer.valueOf(huoArcitleListInfo.getIs_like()));
            huoArcitleDO.setIs_private(Integer.valueOf(huoArcitleListInfo.getIs_private()));
            huoArcitleDO.setIsself(Integer.valueOf(huoArcitleListInfo.getIsself()));
            huoArcitleDO.setLink_url(huoArcitleListInfo.getLink_url());
            huoArcitleDO.setPubtime2(huoArcitleListInfo.getPubtime2());
            huoArcitleDO.setRole(huoArcitleListInfo.getRole());
            huoArcitleDO.setRelation(huoArcitleListInfo.getRelation());
            huoArcitleDO.setRepost(StringUtils.hasLength(huoArcitleListInfo.getRepost()) ? Integer.valueOf(huoArcitleListInfo.getRepost()):0);
            huoArcitleDO.setThumb(huoArcitleListInfo.getThumb());
            huoArcitleDO.setWidth(huoArcitleListInfo.getWidth());
            huoArcitleDO.setThumb_org(huoArcitleListInfo.getThumb_org());
            huoArcitleDO.setTopic_id(Integer.valueOf(huoArcitleListInfo.getTopic_id()));
            huoArcitleDO.setTopic_type(huoArcitleListInfo.getTopic_type());
            huoArcitleDO.setTopic_name(huoArcitleListInfo.getTopic_name());
            huoArcitleDO.setVip_type(huoArcitleListInfo.getVip_type());
            huoArcitleDO.setVoice_long_time(huoArcitleListInfo.getVoice_long_time());
            huoArcitleDO.setVoice_sample_rate(huoArcitleListInfo.getVoice_sample_rate());
            huoArcitleDO.setVoice_wave_data(huoArcitleListInfo.getVoice_wave_data());
            huoArcitleDO.setVoice_thumb(huoArcitleListInfo.getVoice_thumb());
            huoArcitleDO.setVoice_thumb_org(huoArcitleListInfo.getVoice_thumb_org());
            huoArcitleDO.setVoice_url_aac(huoArcitleListInfo.getVoice_url_aac());
            huoArcitleDO.setVoice_url_mp3(huoArcitleListInfo.getVoice_url_mp3());
            huoArcitleDO.setVoice_view(StringUtils.hasLength(huoArcitleListInfo.getVoice_view()) ? Integer.valueOf(huoArcitleListInfo.getVoice_view()):0);
            huoArcitleDO.setView(StringUtils.hasLength(huoArcitleListInfo.getView()) ? Integer.valueOf(huoArcitleListInfo.getView()):0);
            list.add(huoArcitleDO);
        }
        return list;
    }

    public List<HuoTopicDO> formate(List<HuoTopicVO> huoTopicVOList) {
        List<HuoTopicDO> list = Lists.newArrayList();
        for (HuoTopicVO huoTopicVO : huoTopicVOList) {
            HuoTopicDO huoTopicDO = new HuoTopicDO();
            huoTopicDO.setThread_count(Integer.valueOf(huoTopicVO.getThread_count()));
            huoTopicDO.setTopic_id(Integer.valueOf(huoTopicVO.getTopic_id()));
            huoTopicDO.setTopic_name((huoTopicVO.getTopic_name()));
            huoTopicDO.setUser_id(Integer.valueOf(huoTopicVO.getUser_id()));
            huoTopicDO.setTopic_type(huoTopicVO.getTopic_type());
            huoTopicDO.setView(Integer.valueOf(huoTopicVO.getView()));
            list.add(huoTopicDO);
        }
        return list;
    }
}
