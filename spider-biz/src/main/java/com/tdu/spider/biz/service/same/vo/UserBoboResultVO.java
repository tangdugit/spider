package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: UserBoboVO.java, v 0.1 2018年02月11日 下午1:20 tangdu Exp $
 */
@Setter
@Getter
public class UserBoboResultVO {

    private List<UserVO> rank;
    private UserVO user;
}
