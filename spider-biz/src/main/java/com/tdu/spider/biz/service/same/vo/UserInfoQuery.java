package com.tdu.spider.biz.service.same.vo;


import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class UserInfoQuery implements Serializable {

    private Long senseId;

    private Long channelId;

    private Long userId;
    /** 分页偏移量 **/
    private Long offset;
    /** 最热范围(天数) 默认为最近一天 **/
    private Integer days = 1;
    /** 设置路径 **/
    private String url;

    private List<Long> senseIds;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
