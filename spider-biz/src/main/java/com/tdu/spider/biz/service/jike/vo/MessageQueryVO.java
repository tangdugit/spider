package com.tdu.spider.biz.service.jike.vo;

public class MessageQueryVO {

    private Long    messageIdLessThan;

    private String  topic;

    private Integer limit;

    public Long getMessageIdLessThan() {
        return messageIdLessThan;
    }

    public void setMessageIdLessThan(Long messageIdLessThan) {
        this.messageIdLessThan = messageIdLessThan;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
