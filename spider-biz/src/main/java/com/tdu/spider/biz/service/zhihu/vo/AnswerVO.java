package com.tdu.spider.biz.service.zhihu.vo;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: AnswerVO.java, v 0.1 2017年04月2017/4/12日 下午11:59 tangdu Exp $
 * @name TODO: AnswerVO
 */
public class AnswerVO {
    private String     editable_content;
    private String     excerpt;
    private String     thumbnail;
    private Long       created_time;
    private Long       id;
    private Integer    voteup_count;
    private AuthorVO   author;
    private QuestionVO question;

    private String     content;
    private Integer    comment_count;
    private String     reshipment_settings;
    private String     type;
    private String     url;
    private Long       updated_time;
    private Boolean    is_normal;
    private String     comment_permission;
    private Boolean    is_copyable;
    private Boolean    is_collapsed;

    public String getEditable_content() {
        return editable_content;
    }

    public void setEditable_content(String editable_content) {
        this.editable_content = editable_content;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Long getCreated_time() {
        return created_time;
    }

    public void setCreated_time(Long created_time) {
        this.created_time = created_time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVoteup_count() {
        return voteup_count;
    }

    public void setVoteup_count(Integer voteup_count) {
        this.voteup_count = voteup_count;
    }

    public AuthorVO getAuthor() {
        return author;
    }

    public void setAuthor(AuthorVO author) {
        this.author = author;
    }

    public QuestionVO getQuestion() {
        return question;
    }

    public void setQuestion(QuestionVO question) {
        this.question = question;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getComment_count() {
        return comment_count;
    }

    public void setComment_count(Integer comment_count) {
        this.comment_count = comment_count;
    }

    public String getReshipment_settings() {
        return reshipment_settings;
    }

    public void setReshipment_settings(String reshipment_settings) {
        this.reshipment_settings = reshipment_settings;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getUpdated_time() {
        return updated_time;
    }

    public void setUpdated_time(Long updated_time) {
        this.updated_time = updated_time;
    }

    public Boolean getIs_normal() {
        return is_normal;
    }

    public void setIs_normal(Boolean is_normal) {
        this.is_normal = is_normal;
    }

    public String getComment_permission() {
        return comment_permission;
    }

    public void setComment_permission(String comment_permission) {
        this.comment_permission = comment_permission;
    }

    public Boolean getIs_copyable() {
        return is_copyable;
    }

    public void setIs_copyable(Boolean is_copyable) {
        this.is_copyable = is_copyable;
    }

    public Boolean getIs_collapsed() {
        return is_collapsed;
    }

    public void setIs_collapsed(Boolean is_collapsed) {
        this.is_collapsed = is_collapsed;
    }

    private class AuthorVO {
        private String avatar_url_template;
        private String name;
        private String url;
        private String headline;
        private String user_type;
        private String url_token;
        private String is_advertiser;
        private String avatar_url;
        private String is_org;
        private String gender;
        private String type;
        private String id;

        public String getAvatar_url_template() {
            return avatar_url_template;
        }

        public void setAvatar_url_template(String avatar_url_template) {
            this.avatar_url_template = avatar_url_template;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getHeadline() {
            return headline;
        }

        public void setHeadline(String headline) {
            this.headline = headline;
        }

        public String getUser_type() {
            return user_type;
        }

        public void setUser_type(String user_type) {
            this.user_type = user_type;
        }

        public String getUrl_token() {
            return url_token;
        }

        public void setUrl_token(String url_token) {
            this.url_token = url_token;
        }

        public String getIs_advertiser() {
            return is_advertiser;
        }

        public void setIs_advertiser(String is_advertiser) {
            this.is_advertiser = is_advertiser;
        }

        public String getAvatar_url() {
            return avatar_url;
        }

        public void setAvatar_url(String avatar_url) {
            this.avatar_url = avatar_url;
        }

        public String getIs_org() {
            return is_org;
        }

        public void setIs_org(String is_org) {
            this.is_org = is_org;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

}
