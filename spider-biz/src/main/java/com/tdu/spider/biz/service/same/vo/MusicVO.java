package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MusicVO.java, v 0.1 2018年02月19日 下午8:54 tangdu Exp $
 */
@Setter
@Getter
public class MusicVO {
    private Long id;
    private Long song_id;
    private String title;
    private String src;
    private String author;
    private String album_name;
    private String cover;
    private Long sid;
    private String source;
    private String album_art_url;
    private String artist_name;
    private String name;
    private String source_url;
}
