package com.tdu.spider.biz.service.huocaihe.vo;

import lombok.Data;

import java.util.List;

@Data
public class HuoArcitleVO {
    private String               topic_id;
    private String               topic_type;
    private String               follow_count;
    private String               name;
    private String               intro;
    private String               view;
    private String               is_follow;
    private List<HuoArcitleListVO> list;

}
