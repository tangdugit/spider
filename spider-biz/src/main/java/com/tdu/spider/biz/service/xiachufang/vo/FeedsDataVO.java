package com.tdu.spider.biz.service.xiachufang.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: FeedsDataVO.java, v 0.1 2018年01月27日 上午9:13 tangdu Exp $
 */
@Getter
@Setter
public class FeedsDataVO implements Serializable {

    private List<FeedsVO> feeds;
    private int count;
}
