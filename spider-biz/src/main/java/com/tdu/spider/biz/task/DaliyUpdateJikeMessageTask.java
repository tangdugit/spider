package com.tdu.spider.biz.task;

import com.tdu.spider.biz.service.jike.JikeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by TangDu on 2017/7/2.
 */
@Component
public class DaliyUpdateJikeMessageTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(DaliyUpdateJikeMessageTask.class);

    @Autowired
    private JikeService jikeService;

    /**
     * 每天23点执行任务
     */
    @Scheduled(cron="0 0 23 * * ?")
    public void execute() {
        LOGGER.info("Scheduled.DaliyUpdateJikeMessageTask 定时任务:");
        try {
           // jikeService.daliyUpdate();
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyUpdateJikeMessageTask.Error ",e);
        }
    }
}
