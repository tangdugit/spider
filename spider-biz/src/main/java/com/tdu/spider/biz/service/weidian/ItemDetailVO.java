package com.tdu.spider.biz.service.weidian;

import lombok.Getter;
import lombok.Setter;

/**
 * @Auther: lubin.su
 * @Date: 2018/12/11 21:00
 * @Description: TODO
 */
@Setter
@Getter
public class ItemDetailVO {

	private String shopName;
	private String ShopScore;
	private String repurRatePercent;
	private int payedTotal;
	private String shopUrl;
	private String itemName;
	private Double itemPrice;
	private int commentCount;
	private String favorRate;
	private int favoriteCount;
	private int itemSoldout;
	private int itemStock;
	private String freeDelivery;

}
