package com.tdu.spider.biz.service.article.vo;

public class ArticleVO {

    private Long    articleId;
    /**类型:(图片1-图片，2-图片+文字,3-文字)**/
    private Integer type;
    /**内容**/
    private String  text;
    /**外链图片**/
    private String  linkImageUrl;
    /**内部图片**/
    private String  imageUrl;
    /**创建时间(yyyy-MM-dd HH:mm:ss)**/
    private String  createTime;
    /**更新时间(yyyy-MM-dd HH:mm:ss)**/
    private String  updateTime;
    /**扩展字段**/
    private String  extProp;

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLinkImageUrl() {
        return linkImageUrl;
    }

    public void setLinkImageUrl(String linkImageUrl) {
        this.linkImageUrl = linkImageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getExtProp() {
        return extProp;
    }

    public void setExtProp(String extProp) {
        this.extProp = extProp;
    }
}
