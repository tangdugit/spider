package com.tdu.spider.biz.service.zhihu.vo;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: QuestionResultVO.java, v 0.1 2017年04月2017/4/21日 下午11:34 tangdu Exp $
 * @name TODO: QuestionResultVO
 */
public class QuestionResultVO {
    private Integer          page = 1;
    private List<QuestionVO> data;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<QuestionVO> getData() {
        return data;
    }

    public void setData(List<QuestionVO> data) {
        this.data = data;
    }
}
