package com.tdu.spider.biz.service.movie;

import lombok.Data;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieInfoVO.java, v 0.1 2018年07月11日 下午11:13 tangdu Exp $
 */
@Data
public class MovieInfoVO {

    private String categoryCode;
    private String categoryTitle;
    private List<MediaVO> mediaModelList;
}
