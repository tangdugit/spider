package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: UserChatHistoryVO.java, v 0.1 2018年02月18日 上午7:00 tangdu Exp $
 */
@Getter
@Setter
public class UserChatHistoryQueryVO {
    private Long userId;
    private Integer limit;
    private Long toUserId;
}
