package com.tdu.spider.biz.service.same.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class DataResultVO<T> implements Serializable {

    private Integer code;

    private Long  uid;

    private String  err;

    private T       data;


}
