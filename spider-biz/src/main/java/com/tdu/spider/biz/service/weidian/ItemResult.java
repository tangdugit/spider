package com.tdu.spider.biz.service.weidian;

import java.util.List;

public class ItemResult {

    /**
     * status : {"code":0,"message":"OK","description":""}
     * result : {"currentUserInfo":{"gender":0,"isItemFavorite":0,"isShopFavorite":0,"userId":"336429028"},"itemInfo":{"baseItemInfo":{"adIcon":0,"allowBuy":true,"buyNum":0,"discountOrNot":0,"expressFee":"0.0","favoriteCount":1,"freeDelivery":0,"globalPurchase":false,"isFx":false,"isItemStoreDelivery":0,"isPointPrice":0,"isScore":0,"isStoreDelivery":0,"itemDesc":"好益味340ml","itemDiscount":0,"itemHighPrice":450,"itemId":"2659741737","itemLowPrice":450,"itemMainPic":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","itemMainPics":["https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","https://si.geilicdn.com/wdseller1024903213-2fd60000016797a9058f0a02853e_800_800.jpg","https://si.geilicdn.com/wdseller1024903213-575e0000016797a9043c0a02685e_1080_1440.jpg"],"itemName":"好益味340ml","itemOriginalHighPrice":450,"itemOriginalLowPrice":450,"itemOriginalPrice":450,"itemPrice":450,"itemShowHighPrice":450,"itemShowLowPrice":450,"itemSkuList":[],"itemSoldout":2,"itemStatus":0,"itemStock":9,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"1024903213","showCart":true,"showOriginalPrice":false,"spoor":"","taobaoShopIcon":false,"taxRate":0,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"itemAssistantInfo":{},"itemExtendInfo":{"holidayMaterial":{},"itemContent":[{"contentType":1,"contentTypeDesc":"文本","text":"好益味340ml"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-2fd60000016797a9058f0a02853e_800_800.jpg"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-575e0000016797a9043c0a02685e_1080_1440.jpg"}],"itemShareUrl":"https://weidian.com/item.html?itemID=2659741737"},"marketingTools":{"deliveryServices":[],"itemPreferences":[]}},"shopInfo":{"shopBaseInfo":{"buyNum":0,"collectCount":0,"cpsIcon":"","deliveryTime":"24小时内","expressFee":"0.0","favoriteCount":1752,"fx":false,"groupNum":0,"hasCoupon":-1,"hasFavorite":false,"imH5ChatUrl":"https://im.weidian.com/?wfr=miniprogramitem/chat/1024903213","isBaking":0,"isCraftsman":0,"isFarmer":0,"isForwardIm":1,"isFreePostage":0,"isGlobalShop":0,"isKitchen":0,"lastActiveIcon":"https://si.geilicdn.com/hz_img_487100000164fd7388550a02853e_15_15_unadjust.png","lastActiveTime":"2018-12-11 11:33:26","lastActiveTitle":"刚刚","nation":"中国","nickName":"零食驿站","payedTotal":0,"placeName":"烟台","regularCustomerNum":0,"remoteFreePostage":0,"repurRatePercent":"56%","sayingNum":0,"sellerId":"1024903213","sellerLogo":"https://si.geilicdn.com/bj-vshop-1024903213-1521383874883-869558953_996_996.jpg?w=996&h=996","shopBgImgUrl":"https://si.geilicdn.com/bj-vshop-1024903213-1508767849948-494365178_984_615.jpg?w=640&h=330&cp=1","shopGrade":12,"shopId":"1024903213","shopLogo":"https://si.geilicdn.com/bj-vshop-1024903213-1537792340905-2071212837_992_992.jpg?w=250&h=250&cp=1","shopName":"零食驿站南校店","shopNote":"首次关注\u201c烟大零食驿站\u201d的朋友们依然可以领取2元无门槛红包呦。零食驿站专注校园零食，每晚七点配送，19:00 19:30 20:00 20:30 21:00 21:30 21:45分别送出，半小时内送达（女生下单时间截止到21:45，男生下单时间截止到22:00并在此刻送出，逾时订单第二天配送）。若未及时送到，请在微店说明或拨打电话17606476072（仅山东烟台地区）","sortType":1,"status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"updateCount":0,"wechatId":"17865562933"},"shopExtendInfo":{"coupons":[],"filter":{"categories":[{"childCates":[],"id":111397797,"name":"牛奶"},{"childCates":[],"id":111398057,"name":"全部"}]},"leagueShopInfo":{"isLeagueShop":0},"shopShareUrl":"https://weidian.com/?shopId=1024903213","shopTags":[]},"shopQualifications":{"bigIcons":[],"identifyCerts":[{"desc":"","icon":"https://si.geilicdn.com/hz_img_06e70000015cafb7eeef0a026860_48_48_unadjust.png","jumpUrl":"","name":"realName","order":4,"serviceType":0,"showType":"0","title":"实名认证","type":2,"typeName":"身份类认证"}],"qualifiCerts":[],"serviceCerts":[{"desc":"承诺在买家签收后7天内，可以在符合7天退货的条件下退款。如果没有明确签收时间的，以买家收货时间为准","icon":"https://si.geilicdn.com/hz_img_03d20000015d7ded64b60a026860_51_51_unadjust.png","jumpUrl":"https://vmspub.weidian.com/gaia/3879/973c7263.html","name":"refundFlag7d","order":5,"serviceType":0,"showType":"1","title":"7天退货","type":1,"typeName":"服务类认证"},{"desc":"该卖家已开通了交易资金担保服务，交易将由微店提供资金担保，在买家确认收货后（或自卖家确认发货之日起，买卖双方约定的自动确认收货时间到期日后）结算给卖家","icon":"https://si.geilicdn.com/hz_img_03d40000015d7ded65630a026860_51_51_unadjust.png","jumpUrl":"","name":"guarantee","order":7,"serviceType":0,"showType":"1","title":"交易资金担保","type":1,"typeName":"服务类认证"}],"shopCertificationH5Url":"","talentTags":[]}},"systemTime":1544499363271}
     */

    private StatusBean status;
    private ResultBean result;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public static class StatusBean {
        /**
         * code : 0
         * message : OK
         * description :
         */

        private int code;
        private String message;
        private String description;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public static class ResultBean {
        /**
         * currentUserInfo : {"gender":0,"isItemFavorite":0,"isShopFavorite":0,"userId":"336429028"}
         * itemInfo : {"baseItemInfo":{"adIcon":0,"allowBuy":true,"buyNum":0,"discountOrNot":0,"expressFee":"0.0","favoriteCount":1,"freeDelivery":0,"globalPurchase":false,"isFx":false,"isItemStoreDelivery":0,"isPointPrice":0,"isScore":0,"isStoreDelivery":0,"itemDesc":"好益味340ml","itemDiscount":0,"itemHighPrice":450,"itemId":"2659741737","itemLowPrice":450,"itemMainPic":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","itemMainPics":["https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","https://si.geilicdn.com/wdseller1024903213-2fd60000016797a9058f0a02853e_800_800.jpg","https://si.geilicdn.com/wdseller1024903213-575e0000016797a9043c0a02685e_1080_1440.jpg"],"itemName":"好益味340ml","itemOriginalHighPrice":450,"itemOriginalLowPrice":450,"itemOriginalPrice":450,"itemPrice":450,"itemShowHighPrice":450,"itemShowLowPrice":450,"itemSkuList":[],"itemSoldout":2,"itemStatus":0,"itemStock":9,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"1024903213","showCart":true,"showOriginalPrice":false,"spoor":"","taobaoShopIcon":false,"taxRate":0,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false},"itemAssistantInfo":{},"itemExtendInfo":{"holidayMaterial":{},"itemContent":[{"contentType":1,"contentTypeDesc":"文本","text":"好益味340ml"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-2fd60000016797a9058f0a02853e_800_800.jpg"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-575e0000016797a9043c0a02685e_1080_1440.jpg"}],"itemShareUrl":"https://weidian.com/item.html?itemID=2659741737"},"marketingTools":{"deliveryServices":[],"itemPreferences":[]}}
         * shopInfo : {"shopBaseInfo":{"buyNum":0,"collectCount":0,"cpsIcon":"","deliveryTime":"24小时内","expressFee":"0.0","favoriteCount":1752,"fx":false,"groupNum":0,"hasCoupon":-1,"hasFavorite":false,"imH5ChatUrl":"https://im.weidian.com/?wfr=miniprogramitem/chat/1024903213","isBaking":0,"isCraftsman":0,"isFarmer":0,"isForwardIm":1,"isFreePostage":0,"isGlobalShop":0,"isKitchen":0,"lastActiveIcon":"https://si.geilicdn.com/hz_img_487100000164fd7388550a02853e_15_15_unadjust.png","lastActiveTime":"2018-12-11 11:33:26","lastActiveTitle":"刚刚","nation":"中国","nickName":"零食驿站","payedTotal":0,"placeName":"烟台","regularCustomerNum":0,"remoteFreePostage":0,"repurRatePercent":"56%","sayingNum":0,"sellerId":"1024903213","sellerLogo":"https://si.geilicdn.com/bj-vshop-1024903213-1521383874883-869558953_996_996.jpg?w=996&h=996","shopBgImgUrl":"https://si.geilicdn.com/bj-vshop-1024903213-1508767849948-494365178_984_615.jpg?w=640&h=330&cp=1","shopGrade":12,"shopId":"1024903213","shopLogo":"https://si.geilicdn.com/bj-vshop-1024903213-1537792340905-2071212837_992_992.jpg?w=250&h=250&cp=1","shopName":"零食驿站南校店","shopNote":"首次关注\u201c烟大零食驿站\u201d的朋友们依然可以领取2元无门槛红包呦。零食驿站专注校园零食，每晚七点配送，19:00 19:30 20:00 20:30 21:00 21:30 21:45分别送出，半小时内送达（女生下单时间截止到21:45，男生下单时间截止到22:00并在此刻送出，逾时订单第二天配送）。若未及时送到，请在微店说明或拨打电话17606476072（仅山东烟台地区）","sortType":1,"status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"updateCount":0,"wechatId":"17865562933"},"shopExtendInfo":{"coupons":[],"filter":{"categories":[{"childCates":[],"id":111397797,"name":"牛奶"},{"childCates":[],"id":111398057,"name":"全部"}]},"leagueShopInfo":{"isLeagueShop":0},"shopShareUrl":"https://weidian.com/?shopId=1024903213","shopTags":[]},"shopQualifications":{"bigIcons":[],"identifyCerts":[{"desc":"","icon":"https://si.geilicdn.com/hz_img_06e70000015cafb7eeef0a026860_48_48_unadjust.png","jumpUrl":"","name":"realName","order":4,"serviceType":0,"showType":"0","title":"实名认证","type":2,"typeName":"身份类认证"}],"qualifiCerts":[],"serviceCerts":[{"desc":"承诺在买家签收后7天内，可以在符合7天退货的条件下退款。如果没有明确签收时间的，以买家收货时间为准","icon":"https://si.geilicdn.com/hz_img_03d20000015d7ded64b60a026860_51_51_unadjust.png","jumpUrl":"https://vmspub.weidian.com/gaia/3879/973c7263.html","name":"refundFlag7d","order":5,"serviceType":0,"showType":"1","title":"7天退货","type":1,"typeName":"服务类认证"},{"desc":"该卖家已开通了交易资金担保服务，交易将由微店提供资金担保，在买家确认收货后（或自卖家确认发货之日起，买卖双方约定的自动确认收货时间到期日后）结算给卖家","icon":"https://si.geilicdn.com/hz_img_03d40000015d7ded65630a026860_51_51_unadjust.png","jumpUrl":"","name":"guarantee","order":7,"serviceType":0,"showType":"1","title":"交易资金担保","type":1,"typeName":"服务类认证"}],"shopCertificationH5Url":"","talentTags":[]}}
         * systemTime : 1544499363271
         */

        private CurrentUserInfoBean currentUserInfo;
        private ItemInfoBean itemInfo;
        private ShopInfoBean shopInfo;
        private long systemTime;

        public CurrentUserInfoBean getCurrentUserInfo() {
            return currentUserInfo;
        }

        public void setCurrentUserInfo(CurrentUserInfoBean currentUserInfo) {
            this.currentUserInfo = currentUserInfo;
        }

        public ItemInfoBean getItemInfo() {
            return itemInfo;
        }

        public void setItemInfo(ItemInfoBean itemInfo) {
            this.itemInfo = itemInfo;
        }

        public ShopInfoBean getShopInfo() {
            return shopInfo;
        }

        public void setShopInfo(ShopInfoBean shopInfo) {
            this.shopInfo = shopInfo;
        }

        public long getSystemTime() {
            return systemTime;
        }

        public void setSystemTime(long systemTime) {
            this.systemTime = systemTime;
        }

        public static class CurrentUserInfoBean {
            /**
             * gender : 0
             * isItemFavorite : 0
             * isShopFavorite : 0
             * userId : 336429028
             */

            private int gender;
            private int isItemFavorite;
            private int isShopFavorite;
            private String userId;

            public int getGender() {
                return gender;
            }

            public void setGender(int gender) {
                this.gender = gender;
            }

            public int getIsItemFavorite() {
                return isItemFavorite;
            }

            public void setIsItemFavorite(int isItemFavorite) {
                this.isItemFavorite = isItemFavorite;
            }

            public int getIsShopFavorite() {
                return isShopFavorite;
            }

            public void setIsShopFavorite(int isShopFavorite) {
                this.isShopFavorite = isShopFavorite;
            }

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }
        }

        public static class ItemInfoBean {
            /**
             * baseItemInfo : {"adIcon":0,"allowBuy":true,"buyNum":0,"discountOrNot":0,"expressFee":"0.0","favoriteCount":1,"freeDelivery":0,"globalPurchase":false,"isFx":false,"isItemStoreDelivery":0,"isPointPrice":0,"isScore":0,"isStoreDelivery":0,"itemDesc":"好益味340ml","itemDiscount":0,"itemHighPrice":450,"itemId":"2659741737","itemLowPrice":450,"itemMainPic":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","itemMainPics":["https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","https://si.geilicdn.com/wdseller1024903213-2fd60000016797a9058f0a02853e_800_800.jpg","https://si.geilicdn.com/wdseller1024903213-575e0000016797a9043c0a02685e_1080_1440.jpg"],"itemName":"好益味340ml","itemOriginalHighPrice":450,"itemOriginalLowPrice":450,"itemOriginalPrice":450,"itemPrice":450,"itemShowHighPrice":450,"itemShowLowPrice":450,"itemSkuList":[],"itemSoldout":2,"itemStatus":0,"itemStock":9,"itemType":1,"mcSafeSpu":false,"newItem":false,"pointItem":false,"shopId":"1024903213","showCart":true,"showOriginalPrice":false,"spoor":"","taobaoShopIcon":false,"taxRate":0,"time":0,"timingSold":false,"timingSoldDuration":0,"tmallShopIcon":false}
             * itemAssistantInfo : {}
             * itemExtendInfo : {"holidayMaterial":{},"itemContent":[{"contentType":1,"contentTypeDesc":"文本","text":"好益味340ml"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-2fd60000016797a9058f0a02853e_800_800.jpg"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-575e0000016797a9043c0a02685e_1080_1440.jpg"}],"itemShareUrl":"https://weidian.com/item.html?itemID=2659741737"}
             * marketingTools : {"deliveryServices":[],"itemPreferences":[]}
             */

            private BaseItemInfoBean baseItemInfo;
            private ItemAssistantInfoBean itemAssistantInfo;
            private ItemExtendInfoBean itemExtendInfo;
            private MarketingToolsBean marketingTools;

            public BaseItemInfoBean getBaseItemInfo() {
                return baseItemInfo;
            }

            public void setBaseItemInfo(BaseItemInfoBean baseItemInfo) {
                this.baseItemInfo = baseItemInfo;
            }

            public ItemAssistantInfoBean getItemAssistantInfo() {
                return itemAssistantInfo;
            }

            public void setItemAssistantInfo(ItemAssistantInfoBean itemAssistantInfo) {
                this.itemAssistantInfo = itemAssistantInfo;
            }

            public ItemExtendInfoBean getItemExtendInfo() {
                return itemExtendInfo;
            }

            public void setItemExtendInfo(ItemExtendInfoBean itemExtendInfo) {
                this.itemExtendInfo = itemExtendInfo;
            }

            public MarketingToolsBean getMarketingTools() {
                return marketingTools;
            }

            public void setMarketingTools(MarketingToolsBean marketingTools) {
                this.marketingTools = marketingTools;
            }

            public static class BaseItemInfoBean {
                /**
                 * adIcon : 0
                 * allowBuy : true
                 * buyNum : 0
                 * discountOrNot : 0
                 * expressFee : 0.0
                 * favoriteCount : 1
                 * freeDelivery : 0
                 * globalPurchase : false
                 * isFx : false
                 * isItemStoreDelivery : 0
                 * isPointPrice : 0
                 * isScore : 0
                 * isStoreDelivery : 0
                 * itemDesc : 好益味340ml
                 * itemDiscount : 0
                 * itemHighPrice : 450
                 * itemId : 2659741737
                 * itemLowPrice : 450
                 * itemMainPic : https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg
                 * itemMainPics : ["https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg","https://si.geilicdn.com/wdseller1024903213-2fd60000016797a9058f0a02853e_800_800.jpg","https://si.geilicdn.com/wdseller1024903213-575e0000016797a9043c0a02685e_1080_1440.jpg"]
                 * itemName : 好益味340ml
                 * itemOriginalHighPrice : 450
                 * itemOriginalLowPrice : 450
                 * itemOriginalPrice : 450
                 * itemPrice : 450
                 * itemShowHighPrice : 450
                 * itemShowLowPrice : 450
                 * itemSkuList : []
                 * itemSoldout : 2
                 * itemStatus : 0
                 * itemStock : 9
                 * itemType : 1
                 * mcSafeSpu : false
                 * newItem : false
                 * pointItem : false
                 * shopId : 1024903213
                 * showCart : true
                 * showOriginalPrice : false
                 * spoor :
                 * taobaoShopIcon : false
                 * taxRate : 0
                 * time : 0
                 * timingSold : false
                 * timingSoldDuration : 0
                 * tmallShopIcon : false
                 */

                private int adIcon;
                private boolean allowBuy;
                private int buyNum;
                private int discountOrNot;
                private String expressFee;
                private int favoriteCount;
                private int freeDelivery;
                private boolean globalPurchase;
                private boolean isFx;
                private int isItemStoreDelivery;
                private int isPointPrice;
                private int isScore;
                private int isStoreDelivery;
                private String itemDesc;
                private int itemDiscount;
                private int itemHighPrice;
                private String itemId;
                private int itemLowPrice;
                private String itemMainPic;
                private String itemName;
                private int itemOriginalHighPrice;
                private int itemOriginalLowPrice;
                private int itemOriginalPrice;
                private int itemPrice;
                private int itemShowHighPrice;
                private int itemShowLowPrice;
                private int itemSoldout;
                private int itemStatus;
                private int itemStock;
                private int itemType;
                private boolean mcSafeSpu;
                private boolean newItem;
                private boolean pointItem;
                private String shopId;
                private boolean showCart;
                private boolean showOriginalPrice;
                private String spoor;
                private boolean taobaoShopIcon;
                private int taxRate;
                private int time;
                private boolean timingSold;
                private int timingSoldDuration;
                private boolean tmallShopIcon;
                private List<String> itemMainPics;
                private List<?> itemSkuList;

                public int getAdIcon() {
                    return adIcon;
                }

                public void setAdIcon(int adIcon) {
                    this.adIcon = adIcon;
                }

                public boolean isAllowBuy() {
                    return allowBuy;
                }

                public void setAllowBuy(boolean allowBuy) {
                    this.allowBuy = allowBuy;
                }

                public int getBuyNum() {
                    return buyNum;
                }

                public void setBuyNum(int buyNum) {
                    this.buyNum = buyNum;
                }

                public int getDiscountOrNot() {
                    return discountOrNot;
                }

                public void setDiscountOrNot(int discountOrNot) {
                    this.discountOrNot = discountOrNot;
                }

                public String getExpressFee() {
                    return expressFee;
                }

                public void setExpressFee(String expressFee) {
                    this.expressFee = expressFee;
                }

                public int getFavoriteCount() {
                    return favoriteCount;
                }

                public void setFavoriteCount(int favoriteCount) {
                    this.favoriteCount = favoriteCount;
                }

                public int getFreeDelivery() {
                    return freeDelivery;
                }

                public void setFreeDelivery(int freeDelivery) {
                    this.freeDelivery = freeDelivery;
                }

                public boolean isGlobalPurchase() {
                    return globalPurchase;
                }

                public void setGlobalPurchase(boolean globalPurchase) {
                    this.globalPurchase = globalPurchase;
                }

                public boolean isIsFx() {
                    return isFx;
                }

                public void setIsFx(boolean isFx) {
                    this.isFx = isFx;
                }

                public int getIsItemStoreDelivery() {
                    return isItemStoreDelivery;
                }

                public void setIsItemStoreDelivery(int isItemStoreDelivery) {
                    this.isItemStoreDelivery = isItemStoreDelivery;
                }

                public int getIsPointPrice() {
                    return isPointPrice;
                }

                public void setIsPointPrice(int isPointPrice) {
                    this.isPointPrice = isPointPrice;
                }

                public int getIsScore() {
                    return isScore;
                }

                public void setIsScore(int isScore) {
                    this.isScore = isScore;
                }

                public int getIsStoreDelivery() {
                    return isStoreDelivery;
                }

                public void setIsStoreDelivery(int isStoreDelivery) {
                    this.isStoreDelivery = isStoreDelivery;
                }

                public String getItemDesc() {
                    return itemDesc;
                }

                public void setItemDesc(String itemDesc) {
                    this.itemDesc = itemDesc;
                }

                public int getItemDiscount() {
                    return itemDiscount;
                }

                public void setItemDiscount(int itemDiscount) {
                    this.itemDiscount = itemDiscount;
                }

                public int getItemHighPrice() {
                    return itemHighPrice;
                }

                public void setItemHighPrice(int itemHighPrice) {
                    this.itemHighPrice = itemHighPrice;
                }

                public String getItemId() {
                    return itemId;
                }

                public void setItemId(String itemId) {
                    this.itemId = itemId;
                }

                public int getItemLowPrice() {
                    return itemLowPrice;
                }

                public void setItemLowPrice(int itemLowPrice) {
                    this.itemLowPrice = itemLowPrice;
                }

                public String getItemMainPic() {
                    return itemMainPic;
                }

                public void setItemMainPic(String itemMainPic) {
                    this.itemMainPic = itemMainPic;
                }

                public String getItemName() {
                    return itemName;
                }

                public void setItemName(String itemName) {
                    this.itemName = itemName;
                }

                public int getItemOriginalHighPrice() {
                    return itemOriginalHighPrice;
                }

                public void setItemOriginalHighPrice(int itemOriginalHighPrice) {
                    this.itemOriginalHighPrice = itemOriginalHighPrice;
                }

                public int getItemOriginalLowPrice() {
                    return itemOriginalLowPrice;
                }

                public void setItemOriginalLowPrice(int itemOriginalLowPrice) {
                    this.itemOriginalLowPrice = itemOriginalLowPrice;
                }

                public int getItemOriginalPrice() {
                    return itemOriginalPrice;
                }

                public void setItemOriginalPrice(int itemOriginalPrice) {
                    this.itemOriginalPrice = itemOriginalPrice;
                }

                public int getItemPrice() {
                    return itemPrice;
                }

                public void setItemPrice(int itemPrice) {
                    this.itemPrice = itemPrice;
                }

                public int getItemShowHighPrice() {
                    return itemShowHighPrice;
                }

                public void setItemShowHighPrice(int itemShowHighPrice) {
                    this.itemShowHighPrice = itemShowHighPrice;
                }

                public int getItemShowLowPrice() {
                    return itemShowLowPrice;
                }

                public void setItemShowLowPrice(int itemShowLowPrice) {
                    this.itemShowLowPrice = itemShowLowPrice;
                }

                public int getItemSoldout() {
                    return itemSoldout;
                }

                public void setItemSoldout(int itemSoldout) {
                    this.itemSoldout = itemSoldout;
                }

                public int getItemStatus() {
                    return itemStatus;
                }

                public void setItemStatus(int itemStatus) {
                    this.itemStatus = itemStatus;
                }

                public int getItemStock() {
                    return itemStock;
                }

                public void setItemStock(int itemStock) {
                    this.itemStock = itemStock;
                }

                public int getItemType() {
                    return itemType;
                }

                public void setItemType(int itemType) {
                    this.itemType = itemType;
                }

                public boolean isMcSafeSpu() {
                    return mcSafeSpu;
                }

                public void setMcSafeSpu(boolean mcSafeSpu) {
                    this.mcSafeSpu = mcSafeSpu;
                }

                public boolean isNewItem() {
                    return newItem;
                }

                public void setNewItem(boolean newItem) {
                    this.newItem = newItem;
                }

                public boolean isPointItem() {
                    return pointItem;
                }

                public void setPointItem(boolean pointItem) {
                    this.pointItem = pointItem;
                }

                public String getShopId() {
                    return shopId;
                }

                public void setShopId(String shopId) {
                    this.shopId = shopId;
                }

                public boolean isShowCart() {
                    return showCart;
                }

                public void setShowCart(boolean showCart) {
                    this.showCart = showCart;
                }

                public boolean isShowOriginalPrice() {
                    return showOriginalPrice;
                }

                public void setShowOriginalPrice(boolean showOriginalPrice) {
                    this.showOriginalPrice = showOriginalPrice;
                }

                public String getSpoor() {
                    return spoor;
                }

                public void setSpoor(String spoor) {
                    this.spoor = spoor;
                }

                public boolean isTaobaoShopIcon() {
                    return taobaoShopIcon;
                }

                public void setTaobaoShopIcon(boolean taobaoShopIcon) {
                    this.taobaoShopIcon = taobaoShopIcon;
                }

                public int getTaxRate() {
                    return taxRate;
                }

                public void setTaxRate(int taxRate) {
                    this.taxRate = taxRate;
                }

                public int getTime() {
                    return time;
                }

                public void setTime(int time) {
                    this.time = time;
                }

                public boolean isTimingSold() {
                    return timingSold;
                }

                public void setTimingSold(boolean timingSold) {
                    this.timingSold = timingSold;
                }

                public int getTimingSoldDuration() {
                    return timingSoldDuration;
                }

                public void setTimingSoldDuration(int timingSoldDuration) {
                    this.timingSoldDuration = timingSoldDuration;
                }

                public boolean isTmallShopIcon() {
                    return tmallShopIcon;
                }

                public void setTmallShopIcon(boolean tmallShopIcon) {
                    this.tmallShopIcon = tmallShopIcon;
                }

                public List<String> getItemMainPics() {
                    return itemMainPics;
                }

                public void setItemMainPics(List<String> itemMainPics) {
                    this.itemMainPics = itemMainPics;
                }

                public List<?> getItemSkuList() {
                    return itemSkuList;
                }

                public void setItemSkuList(List<?> itemSkuList) {
                    this.itemSkuList = itemSkuList;
                }
            }

            public static class ItemAssistantInfoBean {
                /**
                 * itemDetailComment : {"commentCount":5,"count":5,"defCommentCount":0,"favorRate":"100.00%","filterCommentCount":0,"itemCommentStats":{"bad":0,"good":5,"medium":0,"total":5},"itemComments":[{"buyerLogo":"https://si.geilicdn.com/passport-b6ffd2ecbd8c9f2dd61891e78f33225d.jpg","buyerName":"婷***","buyerPhone":"137****9630","commentCreateTime":1537865811000,"commentReply":"","comment_id":482449566,"isShow":1,"itemComment":"商品好评！","itemCommentImgs":[],"itemTags":[{"tagId":"2","tagName":"性价比高","tagNum":0},{"tagId":"1","tagName":"质量好","tagNum":0}],"itemTypeVersion":"猴子 ","member":{"level":1,"score":0},"score":5,"sellerReplyDate":0,"sellerReplyTime":"","sortKey":1}],"score":5,"showComment":true}
                 */

                private ItemDetailCommentBean itemDetailComment;

                public ItemDetailCommentBean getItemDetailComment() {
                    return itemDetailComment;
                }

                public void setItemDetailComment(ItemDetailCommentBean itemDetailComment) {
                    this.itemDetailComment = itemDetailComment;
                }

                public static class ItemDetailCommentBean {
                    /**
                     * commentCount : 5
                     * count : 5
                     * defCommentCount : 0
                     * favorRate : 100.00%
                     * filterCommentCount : 0
                     * itemCommentStats : {"bad":0,"good":5,"medium":0,"total":5}
                     * itemComments : [{"buyerLogo":"https://si.geilicdn.com/passport-b6ffd2ecbd8c9f2dd61891e78f33225d.jpg","buyerName":"婷***","buyerPhone":"137****9630","commentCreateTime":1537865811000,"commentReply":"","comment_id":482449566,"isShow":1,"itemComment":"商品好评！","itemCommentImgs":[],"itemTags":[{"tagId":"2","tagName":"性价比高","tagNum":0},{"tagId":"1","tagName":"质量好","tagNum":0}],"itemTypeVersion":"猴子 ","member":{"level":1,"score":0},"score":5,"sellerReplyDate":0,"sellerReplyTime":"","sortKey":1}]
                     * score : 5
                     * showComment : true
                     */

                    private int commentCount;
                    private int count;
                    private int defCommentCount;
                    private String favorRate;
                    private int filterCommentCount;
                    private ItemCommentStatsBean itemCommentStats;
                    private int score;
                    private boolean showComment;
                    private List<ItemCommentsBean> itemComments;

                    public int getCommentCount() {
                        return commentCount;
                    }

                    public void setCommentCount(int commentCount) {
                        this.commentCount = commentCount;
                    }

                    public int getCount() {
                        return count;
                    }

                    public void setCount(int count) {
                        this.count = count;
                    }

                    public int getDefCommentCount() {
                        return defCommentCount;
                    }

                    public void setDefCommentCount(int defCommentCount) {
                        this.defCommentCount = defCommentCount;
                    }

                    public String getFavorRate() {
                        return favorRate;
                    }

                    public void setFavorRate(String favorRate) {
                        this.favorRate = favorRate;
                    }

                    public int getFilterCommentCount() {
                        return filterCommentCount;
                    }

                    public void setFilterCommentCount(int filterCommentCount) {
                        this.filterCommentCount = filterCommentCount;
                    }

                    public ItemCommentStatsBean getItemCommentStats() {
                        return itemCommentStats;
                    }

                    public void setItemCommentStats(ItemCommentStatsBean itemCommentStats) {
                        this.itemCommentStats = itemCommentStats;
                    }

                    public int getScore() {
                        return score;
                    }

                    public void setScore(int score) {
                        this.score = score;
                    }

                    public boolean isShowComment() {
                        return showComment;
                    }

                    public void setShowComment(boolean showComment) {
                        this.showComment = showComment;
                    }

                    public List<ItemCommentsBean> getItemComments() {
                        return itemComments;
                    }

                    public void setItemComments(List<ItemCommentsBean> itemComments) {
                        this.itemComments = itemComments;
                    }

                    public static class ItemCommentStatsBean {
                        /**
                         * bad : 0
                         * good : 5
                         * medium : 0
                         * total : 5
                         */

                        private int bad;
                        private int good;
                        private int medium;
                        private int total;

                        public int getBad() {
                            return bad;
                        }

                        public void setBad(int bad) {
                            this.bad = bad;
                        }

                        public int getGood() {
                            return good;
                        }

                        public void setGood(int good) {
                            this.good = good;
                        }

                        public int getMedium() {
                            return medium;
                        }

                        public void setMedium(int medium) {
                            this.medium = medium;
                        }

                        public int getTotal() {
                            return total;
                        }

                        public void setTotal(int total) {
                            this.total = total;
                        }
                    }

                    public static class ItemCommentsBean {
                        /**
                         * buyerLogo : https://si.geilicdn.com/passport-b6ffd2ecbd8c9f2dd61891e78f33225d.jpg
                         * buyerName : 婷***
                         * buyerPhone : 137****9630
                         * commentCreateTime : 1537865811000
                         * commentReply :
                         * comment_id : 482449566
                         * isShow : 1
                         * itemComment : 商品好评！
                         * itemCommentImgs : []
                         * itemTags : [{"tagId":"2","tagName":"性价比高","tagNum":0},{"tagId":"1","tagName":"质量好","tagNum":0}]
                         * itemTypeVersion : 猴子
                         * member : {"level":1,"score":0}
                         * score : 5
                         * sellerReplyDate : 0
                         * sellerReplyTime :
                         * sortKey : 1
                         */

                        private String buyerLogo;
                        private String buyerName;
                        private String buyerPhone;
                        private long commentCreateTime;
                        private String commentReply;
                        private int comment_id;
                        private int isShow;
                        private String itemComment;
                        private String itemTypeVersion;
                        private MemberBean member;
                        private int score;
                        private long sellerReplyDate;
                        private String sellerReplyTime;
                        private int sortKey;
                        private List<?> itemCommentImgs;
                        private List<ItemTagsBean> itemTags;

                        public String getBuyerLogo() {
                            return buyerLogo;
                        }

                        public void setBuyerLogo(String buyerLogo) {
                            this.buyerLogo = buyerLogo;
                        }

                        public String getBuyerName() {
                            return buyerName;
                        }

                        public void setBuyerName(String buyerName) {
                            this.buyerName = buyerName;
                        }

                        public String getBuyerPhone() {
                            return buyerPhone;
                        }

                        public void setBuyerPhone(String buyerPhone) {
                            this.buyerPhone = buyerPhone;
                        }

                        public long getCommentCreateTime() {
                            return commentCreateTime;
                        }

                        public void setCommentCreateTime(long commentCreateTime) {
                            this.commentCreateTime = commentCreateTime;
                        }

                        public String getCommentReply() {
                            return commentReply;
                        }

                        public void setCommentReply(String commentReply) {
                            this.commentReply = commentReply;
                        }

                        public int getComment_id() {
                            return comment_id;
                        }

                        public void setComment_id(int comment_id) {
                            this.comment_id = comment_id;
                        }

                        public int getIsShow() {
                            return isShow;
                        }

                        public void setIsShow(int isShow) {
                            this.isShow = isShow;
                        }

                        public String getItemComment() {
                            return itemComment;
                        }

                        public void setItemComment(String itemComment) {
                            this.itemComment = itemComment;
                        }

                        public String getItemTypeVersion() {
                            return itemTypeVersion;
                        }

                        public void setItemTypeVersion(String itemTypeVersion) {
                            this.itemTypeVersion = itemTypeVersion;
                        }

                        public MemberBean getMember() {
                            return member;
                        }

                        public void setMember(MemberBean member) {
                            this.member = member;
                        }

                        public int getScore() {
                            return score;
                        }

                        public void setScore(int score) {
                            this.score = score;
                        }

                        public long getSellerReplyDate() {
                            return sellerReplyDate;
                        }

                        public void setSellerReplyDate(long sellerReplyDate) {
                            this.sellerReplyDate = sellerReplyDate;
                        }

                        public String getSellerReplyTime() {
                            return sellerReplyTime;
                        }

                        public void setSellerReplyTime(String sellerReplyTime) {
                            this.sellerReplyTime = sellerReplyTime;
                        }

                        public int getSortKey() {
                            return sortKey;
                        }

                        public void setSortKey(int sortKey) {
                            this.sortKey = sortKey;
                        }

                        public List<?> getItemCommentImgs() {
                            return itemCommentImgs;
                        }

                        public void setItemCommentImgs(List<?> itemCommentImgs) {
                            this.itemCommentImgs = itemCommentImgs;
                        }

                        public List<ItemTagsBean> getItemTags() {
                            return itemTags;
                        }

                        public void setItemTags(List<ItemTagsBean> itemTags) {
                            this.itemTags = itemTags;
                        }

                        public static class MemberBean {
                            /**
                             * level : 1
                             * score : 0
                             */

                            private int level;
                            private int score;

                            public int getLevel() {
                                return level;
                            }

                            public void setLevel(int level) {
                                this.level = level;
                            }

                            public int getScore() {
                                return score;
                            }

                            public void setScore(int score) {
                                this.score = score;
                            }
                        }

                        public static class ItemTagsBean {
                            /**
                             * tagId : 2
                             * tagName : 性价比高
                             * tagNum : 0
                             */

                            private String tagId;
                            private String tagName;
                            private int tagNum;

                            public String getTagId() {
                                return tagId;
                            }

                            public void setTagId(String tagId) {
                                this.tagId = tagId;
                            }

                            public String getTagName() {
                                return tagName;
                            }

                            public void setTagName(String tagName) {
                                this.tagName = tagName;
                            }

                            public int getTagNum() {
                                return tagNum;
                            }

                            public void setTagNum(int tagNum) {
                                this.tagNum = tagNum;
                            }
                        }
                    }
                }
            }

            public static class ItemExtendInfoBean {
                /**
                 * holidayMaterial : {}
                 * itemContent : [{"contentType":1,"contentTypeDesc":"文本","text":"好益味340ml"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-2fd60000016797a9058f0a02853e_800_800.jpg"},{"contentType":2,"contentTypeDesc":"图片","url":"https://si.geilicdn.com/wdseller1024903213-575e0000016797a9043c0a02685e_1080_1440.jpg"}]
                 * itemShareUrl : https://weidian.com/item.html?itemID=2659741737
                 */

                private HolidayMaterialBean holidayMaterial;
                private String itemShareUrl;
                private List<ItemContentBean> itemContent;

                public HolidayMaterialBean getHolidayMaterial() {
                    return holidayMaterial;
                }

                public void setHolidayMaterial(HolidayMaterialBean holidayMaterial) {
                    this.holidayMaterial = holidayMaterial;
                }

                public String getItemShareUrl() {
                    return itemShareUrl;
                }

                public void setItemShareUrl(String itemShareUrl) {
                    this.itemShareUrl = itemShareUrl;
                }

                public List<ItemContentBean> getItemContent() {
                    return itemContent;
                }

                public void setItemContent(List<ItemContentBean> itemContent) {
                    this.itemContent = itemContent;
                }

                public static class HolidayMaterialBean {
                }

                public static class ItemContentBean {
                    /**
                     * contentType : 1
                     * contentTypeDesc : 文本
                     * text : 好益味340ml
                     * url : https://si.geilicdn.com/wdseller1024903213-5e210000016797a905850a026860_750_750.jpg
                     */

                    private int contentType;
                    private String contentTypeDesc;
                    private String text;
                    private String url;

                    public int getContentType() {
                        return contentType;
                    }

                    public void setContentType(int contentType) {
                        this.contentType = contentType;
                    }

                    public String getContentTypeDesc() {
                        return contentTypeDesc;
                    }

                    public void setContentTypeDesc(String contentTypeDesc) {
                        this.contentTypeDesc = contentTypeDesc;
                    }

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }
                }
            }

            public static class MarketingToolsBean {
                private List<?> deliveryServices;
                private List<?> itemPreferences;

                public List<?> getDeliveryServices() {
                    return deliveryServices;
                }

                public void setDeliveryServices(List<?> deliveryServices) {
                    this.deliveryServices = deliveryServices;
                }

                public List<?> getItemPreferences() {
                    return itemPreferences;
                }

                public void setItemPreferences(List<?> itemPreferences) {
                    this.itemPreferences = itemPreferences;
                }
            }
        }

        public static class ShopInfoBean {
            /**
             * shopBaseInfo : {"buyNum":0,"collectCount":0,"cpsIcon":"","deliveryTime":"24小时内","expressFee":"0.0","favoriteCount":1752,"fx":false,"groupNum":0,"hasCoupon":-1,"hasFavorite":false,"imH5ChatUrl":"https://im.weidian.com/?wfr=miniprogramitem/chat/1024903213","isBaking":0,"isCraftsman":0,"isFarmer":0,"isForwardIm":1,"isFreePostage":0,"isGlobalShop":0,"isKitchen":0,"lastActiveIcon":"https://si.geilicdn.com/hz_img_487100000164fd7388550a02853e_15_15_unadjust.png","lastActiveTime":"2018-12-11 11:33:26","lastActiveTitle":"刚刚","nation":"中国","nickName":"零食驿站","payedTotal":0,"placeName":"烟台","regularCustomerNum":0,"remoteFreePostage":0,"repurRatePercent":"56%","sayingNum":0,"sellerId":"1024903213","sellerLogo":"https://si.geilicdn.com/bj-vshop-1024903213-1521383874883-869558953_996_996.jpg?w=996&h=996","shopBgImgUrl":"https://si.geilicdn.com/bj-vshop-1024903213-1508767849948-494365178_984_615.jpg?w=640&h=330&cp=1","shopGrade":12,"shopId":"1024903213","shopLogo":"https://si.geilicdn.com/bj-vshop-1024903213-1537792340905-2071212837_992_992.jpg?w=250&h=250&cp=1","shopName":"零食驿站南校店","shopNote":"首次关注\u201c烟大零食驿站\u201d的朋友们依然可以领取2元无门槛红包呦。零食驿站专注校园零食，每晚七点配送，19:00 19:30 20:00 20:30 21:00 21:30 21:45分别送出，半小时内送达（女生下单时间截止到21:45，男生下单时间截止到22:00并在此刻送出，逾时订单第二天配送）。若未及时送到，请在微店说明或拨打电话17606476072（仅山东烟台地区）","sortType":1,"status":0,"taobaoShopIcon":false,"tmallShopIcon":false,"updateCount":0,"wechatId":"17865562933"}
             * shopExtendInfo : {"coupons":[],"filter":{"categories":[{"childCates":[],"id":111397797,"name":"牛奶"},{"childCates":[],"id":111398057,"name":"全部"}]},"leagueShopInfo":{"isLeagueShop":0},"shopShareUrl":"https://weidian.com/?shopId=1024903213","shopTags":[]}
             * shopQualifications : {"bigIcons":[],"identifyCerts":[{"desc":"","icon":"https://si.geilicdn.com/hz_img_06e70000015cafb7eeef0a026860_48_48_unadjust.png","jumpUrl":"","name":"realName","order":4,"serviceType":0,"showType":"0","title":"实名认证","type":2,"typeName":"身份类认证"}],"qualifiCerts":[],"serviceCerts":[{"desc":"承诺在买家签收后7天内，可以在符合7天退货的条件下退款。如果没有明确签收时间的，以买家收货时间为准","icon":"https://si.geilicdn.com/hz_img_03d20000015d7ded64b60a026860_51_51_unadjust.png","jumpUrl":"https://vmspub.weidian.com/gaia/3879/973c7263.html","name":"refundFlag7d","order":5,"serviceType":0,"showType":"1","title":"7天退货","type":1,"typeName":"服务类认证"},{"desc":"该卖家已开通了交易资金担保服务，交易将由微店提供资金担保，在买家确认收货后（或自卖家确认发货之日起，买卖双方约定的自动确认收货时间到期日后）结算给卖家","icon":"https://si.geilicdn.com/hz_img_03d40000015d7ded65630a026860_51_51_unadjust.png","jumpUrl":"","name":"guarantee","order":7,"serviceType":0,"showType":"1","title":"交易资金担保","type":1,"typeName":"服务类认证"}],"shopCertificationH5Url":"","talentTags":[]}
             */

            private ShopBaseInfoBean shopBaseInfo;
            private ShopExtendInfoBean shopExtendInfo;
            private ShopQualificationsBean shopQualifications;

            public ShopBaseInfoBean getShopBaseInfo() {
                return shopBaseInfo;
            }

            public void setShopBaseInfo(ShopBaseInfoBean shopBaseInfo) {
                this.shopBaseInfo = shopBaseInfo;
            }

            public ShopExtendInfoBean getShopExtendInfo() {
                return shopExtendInfo;
            }

            public void setShopExtendInfo(ShopExtendInfoBean shopExtendInfo) {
                this.shopExtendInfo = shopExtendInfo;
            }

            public ShopQualificationsBean getShopQualifications() {
                return shopQualifications;
            }

            public void setShopQualifications(ShopQualificationsBean shopQualifications) {
                this.shopQualifications = shopQualifications;
            }

            public static class ShopBaseInfoBean {
                /**
                 * buyNum : 0
                 * collectCount : 0
                 * cpsIcon :
                 * deliveryTime : 24小时内
                 * expressFee : 0.0
                 * favoriteCount : 1752
                 * fx : false
                 * groupNum : 0
                 * hasCoupon : -1
                 * hasFavorite : false
                 * imH5ChatUrl : https://im.weidian.com/?wfr=miniprogramitem/chat/1024903213
                 * isBaking : 0
                 * isCraftsman : 0
                 * isFarmer : 0
                 * isForwardIm : 1
                 * isFreePostage : 0
                 * isGlobalShop : 0
                 * isKitchen : 0
                 * lastActiveIcon : https://si.geilicdn.com/hz_img_487100000164fd7388550a02853e_15_15_unadjust.png
                 * lastActiveTime : 2018-12-11 11:33:26
                 * lastActiveTitle : 刚刚
                 * nation : 中国
                 * nickName : 零食驿站
                 * payedTotal : 0
                 * placeName : 烟台
                 * regularCustomerNum : 0
                 * remoteFreePostage : 0
                 * repurRatePercent : 56%
                 * sayingNum : 0
                 * sellerId : 1024903213
                 * sellerLogo : https://si.geilicdn.com/bj-vshop-1024903213-1521383874883-869558953_996_996.jpg?w=996&h=996
                 * shopBgImgUrl : https://si.geilicdn.com/bj-vshop-1024903213-1508767849948-494365178_984_615.jpg?w=640&h=330&cp=1
                 * shopGrade : 12
                 * shopId : 1024903213
                 * shopLogo : https://si.geilicdn.com/bj-vshop-1024903213-1537792340905-2071212837_992_992.jpg?w=250&h=250&cp=1
                 * shopName : 零食驿站南校店
                 * shopNote : 首次关注“烟大零食驿站”的朋友们依然可以领取2元无门槛红包呦。零食驿站专注校园零食，每晚七点配送，19:00 19:30 20:00 20:30 21:00 21:30 21:45分别送出，半小时内送达（女生下单时间截止到21:45，男生下单时间截止到22:00并在此刻送出，逾时订单第二天配送）。若未及时送到，请在微店说明或拨打电话17606476072（仅山东烟台地区）
                 * sortType : 1
                 * status : 0
                 * taobaoShopIcon : false
                 * tmallShopIcon : false
                 * updateCount : 0
                 * wechatId : 17865562933
                 */

                private int buyNum;
                private int collectCount;
                private String cpsIcon;
                private String deliveryTime;
                private String expressFee;
                private int favoriteCount;
                private boolean fx;
                private int groupNum;
                private int hasCoupon;
                private boolean hasFavorite;
                private String imH5ChatUrl;
                private int isBaking;
                private int isCraftsman;
                private int isFarmer;
                private int isForwardIm;
                private int isFreePostage;
                private int isGlobalShop;
                private int isKitchen;
                private String lastActiveIcon;
                private String lastActiveTime;
                private String lastActiveTitle;
                private String nation;
                private String nickName;
                private int payedTotal;
                private String placeName;
                private int regularCustomerNum;
                private int remoteFreePostage;
                private String repurRatePercent;
                private int sayingNum;
                private String sellerId;
                private String sellerLogo;
                private String shopBgImgUrl;
                private int shopGrade;
                private String shopId;
                private String shopLogo;
                private String shopName;
                private String shopNote;
                private int sortType;
                private int status;
                private boolean taobaoShopIcon;
                private boolean tmallShopIcon;
                private int updateCount;
                private String wechatId;

                public int getBuyNum() {
                    return buyNum;
                }

                public void setBuyNum(int buyNum) {
                    this.buyNum = buyNum;
                }

                public int getCollectCount() {
                    return collectCount;
                }

                public void setCollectCount(int collectCount) {
                    this.collectCount = collectCount;
                }

                public String getCpsIcon() {
                    return cpsIcon;
                }

                public void setCpsIcon(String cpsIcon) {
                    this.cpsIcon = cpsIcon;
                }

                public String getDeliveryTime() {
                    return deliveryTime;
                }

                public void setDeliveryTime(String deliveryTime) {
                    this.deliveryTime = deliveryTime;
                }

                public String getExpressFee() {
                    return expressFee;
                }

                public void setExpressFee(String expressFee) {
                    this.expressFee = expressFee;
                }

                public int getFavoriteCount() {
                    return favoriteCount;
                }

                public void setFavoriteCount(int favoriteCount) {
                    this.favoriteCount = favoriteCount;
                }

                public boolean isFx() {
                    return fx;
                }

                public void setFx(boolean fx) {
                    this.fx = fx;
                }

                public int getGroupNum() {
                    return groupNum;
                }

                public void setGroupNum(int groupNum) {
                    this.groupNum = groupNum;
                }

                public int getHasCoupon() {
                    return hasCoupon;
                }

                public void setHasCoupon(int hasCoupon) {
                    this.hasCoupon = hasCoupon;
                }

                public boolean isHasFavorite() {
                    return hasFavorite;
                }

                public void setHasFavorite(boolean hasFavorite) {
                    this.hasFavorite = hasFavorite;
                }

                public String getImH5ChatUrl() {
                    return imH5ChatUrl;
                }

                public void setImH5ChatUrl(String imH5ChatUrl) {
                    this.imH5ChatUrl = imH5ChatUrl;
                }

                public int getIsBaking() {
                    return isBaking;
                }

                public void setIsBaking(int isBaking) {
                    this.isBaking = isBaking;
                }

                public int getIsCraftsman() {
                    return isCraftsman;
                }

                public void setIsCraftsman(int isCraftsman) {
                    this.isCraftsman = isCraftsman;
                }

                public int getIsFarmer() {
                    return isFarmer;
                }

                public void setIsFarmer(int isFarmer) {
                    this.isFarmer = isFarmer;
                }

                public int getIsForwardIm() {
                    return isForwardIm;
                }

                public void setIsForwardIm(int isForwardIm) {
                    this.isForwardIm = isForwardIm;
                }

                public int getIsFreePostage() {
                    return isFreePostage;
                }

                public void setIsFreePostage(int isFreePostage) {
                    this.isFreePostage = isFreePostage;
                }

                public int getIsGlobalShop() {
                    return isGlobalShop;
                }

                public void setIsGlobalShop(int isGlobalShop) {
                    this.isGlobalShop = isGlobalShop;
                }

                public int getIsKitchen() {
                    return isKitchen;
                }

                public void setIsKitchen(int isKitchen) {
                    this.isKitchen = isKitchen;
                }

                public String getLastActiveIcon() {
                    return lastActiveIcon;
                }

                public void setLastActiveIcon(String lastActiveIcon) {
                    this.lastActiveIcon = lastActiveIcon;
                }

                public String getLastActiveTime() {
                    return lastActiveTime;
                }

                public void setLastActiveTime(String lastActiveTime) {
                    this.lastActiveTime = lastActiveTime;
                }

                public String getLastActiveTitle() {
                    return lastActiveTitle;
                }

                public void setLastActiveTitle(String lastActiveTitle) {
                    this.lastActiveTitle = lastActiveTitle;
                }

                public String getNation() {
                    return nation;
                }

                public void setNation(String nation) {
                    this.nation = nation;
                }

                public String getNickName() {
                    return nickName;
                }

                public void setNickName(String nickName) {
                    this.nickName = nickName;
                }

                public int getPayedTotal() {
                    return payedTotal;
                }

                public void setPayedTotal(int payedTotal) {
                    this.payedTotal = payedTotal;
                }

                public String getPlaceName() {
                    return placeName;
                }

                public void setPlaceName(String placeName) {
                    this.placeName = placeName;
                }

                public int getRegularCustomerNum() {
                    return regularCustomerNum;
                }

                public void setRegularCustomerNum(int regularCustomerNum) {
                    this.regularCustomerNum = regularCustomerNum;
                }

                public int getRemoteFreePostage() {
                    return remoteFreePostage;
                }

                public void setRemoteFreePostage(int remoteFreePostage) {
                    this.remoteFreePostage = remoteFreePostage;
                }

                public String getRepurRatePercent() {
                    return repurRatePercent;
                }

                public void setRepurRatePercent(String repurRatePercent) {
                    this.repurRatePercent = repurRatePercent;
                }

                public int getSayingNum() {
                    return sayingNum;
                }

                public void setSayingNum(int sayingNum) {
                    this.sayingNum = sayingNum;
                }

                public String getSellerId() {
                    return sellerId;
                }

                public void setSellerId(String sellerId) {
                    this.sellerId = sellerId;
                }

                public String getSellerLogo() {
                    return sellerLogo;
                }

                public void setSellerLogo(String sellerLogo) {
                    this.sellerLogo = sellerLogo;
                }

                public String getShopBgImgUrl() {
                    return shopBgImgUrl;
                }

                public void setShopBgImgUrl(String shopBgImgUrl) {
                    this.shopBgImgUrl = shopBgImgUrl;
                }

                public int getShopGrade() {
                    return shopGrade;
                }

                public void setShopGrade(int shopGrade) {
                    this.shopGrade = shopGrade;
                }

                public String getShopId() {
                    return shopId;
                }

                public void setShopId(String shopId) {
                    this.shopId = shopId;
                }

                public String getShopLogo() {
                    return shopLogo;
                }

                public void setShopLogo(String shopLogo) {
                    this.shopLogo = shopLogo;
                }

                public String getShopName() {
                    return shopName;
                }

                public void setShopName(String shopName) {
                    this.shopName = shopName;
                }

                public String getShopNote() {
                    return shopNote;
                }

                public void setShopNote(String shopNote) {
                    this.shopNote = shopNote;
                }

                public int getSortType() {
                    return sortType;
                }

                public void setSortType(int sortType) {
                    this.sortType = sortType;
                }

                public int getStatus() {
                    return status;
                }

                public void setStatus(int status) {
                    this.status = status;
                }

                public boolean isTaobaoShopIcon() {
                    return taobaoShopIcon;
                }

                public void setTaobaoShopIcon(boolean taobaoShopIcon) {
                    this.taobaoShopIcon = taobaoShopIcon;
                }

                public boolean isTmallShopIcon() {
                    return tmallShopIcon;
                }

                public void setTmallShopIcon(boolean tmallShopIcon) {
                    this.tmallShopIcon = tmallShopIcon;
                }

                public int getUpdateCount() {
                    return updateCount;
                }

                public void setUpdateCount(int updateCount) {
                    this.updateCount = updateCount;
                }

                public String getWechatId() {
                    return wechatId;
                }

                public void setWechatId(String wechatId) {
                    this.wechatId = wechatId;
                }
            }

            public static class ShopExtendInfoBean {
                /**
                 * coupons : []
                 * filter : {"categories":[{"childCates":[],"id":111397797,"name":"牛奶"},{"childCates":[],"id":111398057,"name":"全部"}]}
                 * leagueShopInfo : {"isLeagueShop":0}
                 * shopShareUrl : https://weidian.com/?shopId=1024903213
                 * shopTags : []
                 */

                private FilterBean filter;
                private LeagueShopInfoBean leagueShopInfo;
                private String shopShareUrl;
                private List<?> coupons;
                private List<?> shopTags;

                public FilterBean getFilter() {
                    return filter;
                }

                public void setFilter(FilterBean filter) {
                    this.filter = filter;
                }

                public LeagueShopInfoBean getLeagueShopInfo() {
                    return leagueShopInfo;
                }

                public void setLeagueShopInfo(LeagueShopInfoBean leagueShopInfo) {
                    this.leagueShopInfo = leagueShopInfo;
                }

                public String getShopShareUrl() {
                    return shopShareUrl;
                }

                public void setShopShareUrl(String shopShareUrl) {
                    this.shopShareUrl = shopShareUrl;
                }

                public List<?> getCoupons() {
                    return coupons;
                }

                public void setCoupons(List<?> coupons) {
                    this.coupons = coupons;
                }

                public List<?> getShopTags() {
                    return shopTags;
                }

                public void setShopTags(List<?> shopTags) {
                    this.shopTags = shopTags;
                }

                public static class FilterBean {
                    private List<CategoriesBean> categories;

                    public List<CategoriesBean> getCategories() {
                        return categories;
                    }

                    public void setCategories(List<CategoriesBean> categories) {
                        this.categories = categories;
                    }

                    public static class CategoriesBean {
                        /**
                         * childCates : []
                         * id : 111397797
                         * name : 牛奶
                         */

                        private int id;
                        private String name;
                        private List<?> childCates;

                        public int getId() {
                            return id;
                        }

                        public void setId(int id) {
                            this.id = id;
                        }

                        public String getName() {
                            return name;
                        }

                        public void setName(String name) {
                            this.name = name;
                        }

                        public List<?> getChildCates() {
                            return childCates;
                        }

                        public void setChildCates(List<?> childCates) {
                            this.childCates = childCates;
                        }
                    }
                }

                public static class LeagueShopInfoBean {
                    /**
                     * isLeagueShop : 0
                     */

                    private int isLeagueShop;

                    public int getIsLeagueShop() {
                        return isLeagueShop;
                    }

                    public void setIsLeagueShop(int isLeagueShop) {
                        this.isLeagueShop = isLeagueShop;
                    }
                }
            }

            public static class ShopQualificationsBean {
                /**
                 * bigIcons : []
                 * identifyCerts : [{"desc":"","icon":"https://si.geilicdn.com/hz_img_06e70000015cafb7eeef0a026860_48_48_unadjust.png","jumpUrl":"","name":"realName","order":4,"serviceType":0,"showType":"0","title":"实名认证","type":2,"typeName":"身份类认证"}]
                 * qualifiCerts : []
                 * serviceCerts : [{"desc":"承诺在买家签收后7天内，可以在符合7天退货的条件下退款。如果没有明确签收时间的，以买家收货时间为准","icon":"https://si.geilicdn.com/hz_img_03d20000015d7ded64b60a026860_51_51_unadjust.png","jumpUrl":"https://vmspub.weidian.com/gaia/3879/973c7263.html","name":"refundFlag7d","order":5,"serviceType":0,"showType":"1","title":"7天退货","type":1,"typeName":"服务类认证"},{"desc":"该卖家已开通了交易资金担保服务，交易将由微店提供资金担保，在买家确认收货后（或自卖家确认发货之日起，买卖双方约定的自动确认收货时间到期日后）结算给卖家","icon":"https://si.geilicdn.com/hz_img_03d40000015d7ded65630a026860_51_51_unadjust.png","jumpUrl":"","name":"guarantee","order":7,"serviceType":0,"showType":"1","title":"交易资金担保","type":1,"typeName":"服务类认证"}]
                 * shopCertificationH5Url :
                 * talentTags : []
                 */

                private String shopCertificationH5Url;
                private List<?> bigIcons;
                private List<IdentifyCertsBean> identifyCerts;
                private List<?> qualifiCerts;
                private List<ServiceCertsBean> serviceCerts;
                private List<?> talentTags;

                public String getShopCertificationH5Url() {
                    return shopCertificationH5Url;
                }

                public void setShopCertificationH5Url(String shopCertificationH5Url) {
                    this.shopCertificationH5Url = shopCertificationH5Url;
                }

                public List<?> getBigIcons() {
                    return bigIcons;
                }

                public void setBigIcons(List<?> bigIcons) {
                    this.bigIcons = bigIcons;
                }

                public List<IdentifyCertsBean> getIdentifyCerts() {
                    return identifyCerts;
                }

                public void setIdentifyCerts(List<IdentifyCertsBean> identifyCerts) {
                    this.identifyCerts = identifyCerts;
                }

                public List<?> getQualifiCerts() {
                    return qualifiCerts;
                }

                public void setQualifiCerts(List<?> qualifiCerts) {
                    this.qualifiCerts = qualifiCerts;
                }

                public List<ServiceCertsBean> getServiceCerts() {
                    return serviceCerts;
                }

                public void setServiceCerts(List<ServiceCertsBean> serviceCerts) {
                    this.serviceCerts = serviceCerts;
                }

                public List<?> getTalentTags() {
                    return talentTags;
                }

                public void setTalentTags(List<?> talentTags) {
                    this.talentTags = talentTags;
                }

                public static class IdentifyCertsBean {
                    /**
                     * desc :
                     * icon : https://si.geilicdn.com/hz_img_06e70000015cafb7eeef0a026860_48_48_unadjust.png
                     * jumpUrl :
                     * name : realName
                     * order : 4
                     * serviceType : 0
                     * showType : 0
                     * title : 实名认证
                     * type : 2
                     * typeName : 身份类认证
                     */

                    private String desc;
                    private String icon;
                    private String jumpUrl;
                    private String name;
                    private int order;
                    private int serviceType;
                    private String showType;
                    private String title;
                    private int type;
                    private String typeName;

                    public String getDesc() {
                        return desc;
                    }

                    public void setDesc(String desc) {
                        this.desc = desc;
                    }

                    public String getIcon() {
                        return icon;
                    }

                    public void setIcon(String icon) {
                        this.icon = icon;
                    }

                    public String getJumpUrl() {
                        return jumpUrl;
                    }

                    public void setJumpUrl(String jumpUrl) {
                        this.jumpUrl = jumpUrl;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public int getOrder() {
                        return order;
                    }

                    public void setOrder(int order) {
                        this.order = order;
                    }

                    public int getServiceType() {
                        return serviceType;
                    }

                    public void setServiceType(int serviceType) {
                        this.serviceType = serviceType;
                    }

                    public String getShowType() {
                        return showType;
                    }

                    public void setShowType(String showType) {
                        this.showType = showType;
                    }

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }

                    public int getType() {
                        return type;
                    }

                    public void setType(int type) {
                        this.type = type;
                    }

                    public String getTypeName() {
                        return typeName;
                    }

                    public void setTypeName(String typeName) {
                        this.typeName = typeName;
                    }
                }

                public static class ServiceCertsBean {
                    /**
                     * desc : 承诺在买家签收后7天内，可以在符合7天退货的条件下退款。如果没有明确签收时间的，以买家收货时间为准
                     * icon : https://si.geilicdn.com/hz_img_03d20000015d7ded64b60a026860_51_51_unadjust.png
                     * jumpUrl : https://vmspub.weidian.com/gaia/3879/973c7263.html
                     * name : refundFlag7d
                     * order : 5
                     * serviceType : 0
                     * showType : 1
                     * title : 7天退货
                     * type : 1
                     * typeName : 服务类认证
                     */

                    private String desc;
                    private String icon;
                    private String jumpUrl;
                    private String name;
                    private int order;
                    private int serviceType;
                    private String showType;
                    private String title;
                    private int type;
                    private String typeName;

                    public String getDesc() {
                        return desc;
                    }

                    public void setDesc(String desc) {
                        this.desc = desc;
                    }

                    public String getIcon() {
                        return icon;
                    }

                    public void setIcon(String icon) {
                        this.icon = icon;
                    }

                    public String getJumpUrl() {
                        return jumpUrl;
                    }

                    public void setJumpUrl(String jumpUrl) {
                        this.jumpUrl = jumpUrl;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public int getOrder() {
                        return order;
                    }

                    public void setOrder(int order) {
                        this.order = order;
                    }

                    public int getServiceType() {
                        return serviceType;
                    }

                    public void setServiceType(int serviceType) {
                        this.serviceType = serviceType;
                    }

                    public String getShowType() {
                        return showType;
                    }

                    public void setShowType(String showType) {
                        this.showType = showType;
                    }

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }

                    public int getType() {
                        return type;
                    }

                    public void setType(int type) {
                        this.type = type;
                    }

                    public String getTypeName() {
                        return typeName;
                    }

                    public void setTypeName(String typeName) {
                        this.typeName = typeName;
                    }
                }
            }
        }
    }
}
