package com.tdu.spider.biz.service.same.vo;


import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

public class ChannelConfigVO implements Serializable {

    private Long latest_sense_id;
    private Long latest_sense_updated_at;
    private String latest;

    public Long getLatest_sense_id() {
        return latest_sense_id;
    }

    public void setLatest_sense_id(Long latest_sense_id) {
        this.latest_sense_id = latest_sense_id;
    }

    public Long getLatest_sense_updated_at() {
        return latest_sense_updated_at;
    }

    public void setLatest_sense_updated_at(Long latest_sense_updated_at) {
        this.latest_sense_updated_at = latest_sense_updated_at;
    }

    public String getLatest() {
        return latest;
    }

    public void setLatest(String latest) {
        this.latest = latest;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
