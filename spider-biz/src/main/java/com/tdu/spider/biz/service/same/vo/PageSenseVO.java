package com.tdu.spider.biz.service.same.vo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.List;

public class PageSenseVO implements Serializable {

    private String        next;
    private List<SenSeVO> results;
    private Long          lastSenenId;
    /**分页偏移量**/
    private Long          offset;

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public List<SenSeVO> getResults() {
        return results;
    }

    public void setResults(List<SenSeVO> results) {
        this.results = results;
    }

    public Long getLastSenenId() {
        return lastSenenId;
    }

    public void setLastSenenId(Long lastSenenId) {
        this.lastSenenId = lastSenenId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
