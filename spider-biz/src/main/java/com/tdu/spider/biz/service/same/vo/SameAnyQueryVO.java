package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SameAnyQueryVO.java, v 0.1 2018年02月01日 下午5:10 tangdu Exp $
 */
@Setter
@Getter
public class SameAnyQueryVO {
    private Long userId;

    /**是否统计详情**/
    private Boolean getLikes;
}
