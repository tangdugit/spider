package com.tdu.spider.biz.service.tome;

import com.tdu.spider.biz.util.HttpUtils;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

@Service
public class ToFoService {


    public void run() throws Exception{
        Request request = Request.Get("https://tofo.me/nohavewater")
                .addHeader("authority","tofo.me")
                .addHeader("cache-control","max-age=0")
                .addHeader("upgrade-insecure-requests","1")
                .addHeader("accept-encoding","gzip, deflate, br")
                .addHeader("cookie","userViewMode=S; _ga=GA1.2.1879680306.1507426679; _gid=GA1.2.1887288073.1510963706; __atuvc=0%7C42%2C0%7C43%2C0%7C44%2C0%7C45%2C6%7C46")
                .addHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8")
                .addHeader("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
        Response response = Executor.newInstance(HttpUtils.httpClient()).execute(request);
        String string = response.returnContent().asString();
        Document document = Jsoup.parse(string);
        System.out.println(string);
        Elements elements = document.select("img.ui");
        if(elements.isEmpty()){
            return;
        }
        for (Element element : elements) {
            System.out.println(element.attr("src"));
        }
    }
}
