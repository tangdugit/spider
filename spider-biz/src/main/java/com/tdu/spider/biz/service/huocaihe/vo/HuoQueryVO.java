package com.tdu.spider.biz.service.huocaihe.vo;

import lombok.Data;

@Data
public class HuoQueryVO {

    String lastid;
    String p;
    String uid;
    String version;
    String token_key;
    String topic_id;
    String source_type;
    String user_id;
    String source;
    String type;
    String register_id;
    String platform;
    String udid;
    String category_id;

}
