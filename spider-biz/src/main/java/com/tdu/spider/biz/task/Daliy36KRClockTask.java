package com.tdu.spider.biz.task;

import com.google.common.collect.Lists;
import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 36每日早上打卡
 *
 * @author tangdu
 * @version $: Daliy36KRClockTas.java, v 0.1 2018年03月21日 上午9:25 tangdu Exp $
 */
@Component
public class Daliy36KRClockTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(Daliy36KRClockTask.class);

    @Scheduled(cron="0 0 8 * * ?")
    public void 早上8点参加活动() {
        LOGGER.info("Scheduled.Daliy36KRClockTask.早上8点参加活动 定时任务:");
        try {
            Request request = Request.Post("https://36kr.com/lapi/trade")
                    .addHeader("X-Tingyun-Id", "Dio1ZtdC5G4;c=2;r=1216064660;u=13c8c53c08b0666ff640c604276c751b8e8ababa4c30e167e36cfb868c523d6534fc5affe82e3f3d252616a23b546897::35E8958B20771B2F")
                    .addHeader("timestamp", (System.currentTimeMillis() + "").substring(0,10))
                    .addHeader("User-Agent", "36kr-iOS/7.1 (iPhone6S); Build 449; iOS 11.2.6; Scale/2.0;")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                    .addHeader("device", "D7EE3152-6034-4331-98B1-2DB3A0303C28")
                    .addHeader("Cookie", "M-XSRF-TOKEN=2b1ef7a9b7596bf9c4ef79ea9d2760a48569308955ebb03e2b8f22d8964b3db8; M-XSRF-TOKEN.sig=iJ1-rIhvRcRQZrlissxRVT5zvKyyrisphJmuAvwf-4g; Hm_lvt_713123c60a0e86982326bae1a51083e1=1521595086,1521595104,1521595109,1521595216; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22162365a5d004de-0f91703c8a4d07-7f313624-250125-162365a5d0144c%22%2C%22%24device_id%22%3A%22162365a5d004de-0f91703c8a4d07-7f313624-250125-162365a5d0144c%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24latest_referrer_host%22%3A%22%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%7D%7D; kr_device_id=D7EE3152-6034-4331-98B1-2DB3A0303C28; kr_device_id.sig=MPA-qmG4UvuBEFuFrQCVXOQRqCzI5lJI_-NnMacBZPs; kr_pinyou_device_uuid=D7EE3152-6034-4331-98B1-2DB3A0303C28; kr_pinyou_device_uuid.sig=o4nVw9jK9tSJ7-NSo1CZL5VkI_6HAzy4SZIEtDCRojg; Hm_lpvt_713123c60a0e86982326bae1a51083e1=1521595109; krnewsfrontss=9b1677f29460818e0cfd2613897770d1; krnewsfrontss.sig=rO1a0uhKJpEHAEkcukRBezo9s-zjUGAUfJeliYaNsBk; acw_tc=AQAAAEwRPhcYDAwAYajNc52HSx/1Alrc; kr_plus_utype=0; device-uid=3d0a0a80-2a3d-11e8-9bf6-f3627c75f1e7; kr_stat_uuid=6mCYs25355505; kr_plus_id=1560825831; kr_plus_token=SUfWBC1_OslBHz5vboKHCGBdbZHf25146159____; krid_user_version=1; uitoken=oei4ti3is6mmo0uromrhn0hzmy0k2n3tpormz77k8gwu6m6rkic7xc3nrz0cejor")
                    .addHeader("sign", "e29a23df2d65e32558b7b76fdca0fbda");
//            JSONObject jsonObject=new JSONObject();
//            jsonObject.put("goods_id",10922);
//            jsonObject.put("goods_price_id",66);
//            jsonObject.put("goods_price_number",1);
//            jsonObject.put("platform_type",14);
//            StringEntity entity =new StringEntity(jsonObject.toJSONString(),"UTF-8");
//            entity.setContentType("application/json");

            List<NameValuePair> params= Lists.newArrayList();
            params.add(new BasicNameValuePair("goods_id","10922"));
            params.add(new BasicNameValuePair("goods_price_id","66"));
            params.add(new BasicNameValuePair("goods_price_number","1"));
            params.add(new BasicNameValuePair("platform_type","14"));
            Response response = request.bodyForm(params).execute();
            String result = response.returnContent().asString();
            LOGGER.info("早上8点参加活动 {}",result);
        } catch (Exception e) {
            LOGGER.error("Scheduled.Daliy36KRClockTask.早上8点参加活动.Error ",e);
        }
    }

    @Scheduled(cron="0 0 5 * * ?")
    public void 早上5点打卡() {
        LOGGER.info("Scheduled.Daliy36KRClockTask.早上5点打卡 定时任务:");
        try {
            Request request = Request.Post("https://36kr.com/lapi/user-clock")
                    .addHeader("X-Tingyun-Id", "Dio1ZtdC5G4;r=589114756")
                    .addHeader("timestamp", System.currentTimeMillis() + "")
                    .addHeader("User-Agent", "36kr-iOS/7.1 (iPhone6S); Build 449; iOS 11.2.6; Scale/2.0;")
                    .addHeader("device", "D7EE3152-6034-4331-98B1-2DB3A0303C28")
                    .addHeader("Referer","https://36kr.com/clock/home")
                    .addHeader("Referer","2b1502b5f46217668408d35acc5a48b4abacfaf64652ed75aca30b0471123d0d")
                    .addHeader("Cookie", "kr_device_id=D7EE3152-6034-4331-98B1-2DB3A0303C28; kr_device_id.sig=MPA-qmG4UvuBEFuFrQCVXOQRqCzI5lJI_-NnMacBZPs; kr_pinyou_device_uuid=D7EE3152-6034-4331-98B1-2DB3A0303C28; kr_pinyou_device_uuid.sig=o4nVw9jK9tSJ7-NSo1CZL5VkI_6HAzy4SZIEtDCRojg; M-XSRF-TOKEN=2b1502b5f46217668408d35acc5a48b4abacfaf64652ed75aca30b0471123d0d; M-XSRF-TOKEN.sig=TNt2_BapLmWBQoZQ-ASvlpyRuuyto-N7HU66gEVyP0E; kr_plus_utype=0; Hm_lpvt_713123c60a0e86982326bae1a51083e1=1521587529; Hm_lvt_713123c60a0e86982326bae1a51083e1=1521506379,1521554863,1521554873,1521587529; sensorsdata2015jssdkcross=%7B%22distinct_id%22%3A%22162365a5d004de-0f91703c8a4d07-7f313624-250125-162365a5d0144c%22%2C%22%24device_id%22%3A%22162365a5d004de-0f91703c8a4d07-7f313624-250125-162365a5d0144c%22%2C%22props%22%3A%7B%22%24latest_traffic_source_type%22%3A%22%E7%9B%B4%E6%8E%A5%E6%B5%81%E9%87%8F%22%2C%22%24latest_referrer%22%3A%22%22%2C%22%24latest_referrer_host%22%3A%22%22%2C%22%24latest_search_keyword%22%3A%22%E6%9C%AA%E5%8F%96%E5%88%B0%E5%80%BC_%E7%9B%B4%E6%8E%A5%E6%89%93%E5%BC%80%22%7D%7D; acw_tc=AQAAAPpjvx/TWQwAYajNcxHjGrVrad4P; krnewsfrontss=f24f42c392e0dca7a5d6973d0aa21bfc; krnewsfrontss.sig=MJgsGEaNFnVUjg70AqELZZ_Mq8HaU9D0T41TzOF_3wI; device-uid=3d0a0a80-2a3d-11e8-9bf6-f3627c75f1e7; kr_stat_uuid=6mCYs25355505; kr_plus_id=1560825831; kr_plus_token=SUfWBC1_OslBHz5vboKHCGBdbZHf25146159____; krid_user_version=1; uitoken=oei4ti3is6mmo0uromrhn0hzmy0k2n3tpormz77k8gwu6m6rkic7xc3nrz0cejor");
            Response response = request.execute();
            String result = response.returnContent().asString();
            LOGGER.info("早上5点打卡 {}",result);
        } catch (Exception e) {
            LOGGER.error("Scheduled.Daliy36KRClockTask.早上5点打卡.Error ",e);
        }
    }

}
