package com.tdu.spider.biz.task;

import com.tdu.spider.biz.service.same.SameService;
import com.tdu.spider.biz.service.same.vo.LikeSenseQueryVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class DaliyViewSameTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(DaliyViewSameTask.class);

    @Autowired
    private SameService sameService;

    @Scheduled(cron="0 0 0/10 * * ?")
    public void 自己查看() {
        LOGGER.info("Scheduled.DaliyViewSameTask.自己查看 定时任务:");
        try {
            LikeSenseQueryVO likeSenseQueryVO = new LikeSenseQueryVO(15111234L,null);
            likeSenseQueryVO.setFilterLikes(2);
            sameService.viewUserSense(likeSenseQueryVO);
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyViewSameTask.自己查看.Error ",e);
        }
    }


    @Scheduled(cron="0 0 6,9,14,15,18,20,22,23 * * ?")
    public void 女粉查看() {
        LOGGER.info("Scheduled.DaliyViewSameTask.女粉查看 定时任务:");
        try {
              LikeSenseQueryVO likeSenseQueryVO = new LikeSenseQueryVO(15752075L,null);
              sameService.viewUserSense(likeSenseQueryVO);
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyViewSameTask.女粉查看.Error ",e);
        }
    }

    @Scheduled(cron="0 0 6,8,10,18,20,23 * * ?")
    public void 画手查看() {
        LOGGER.info("Scheduled.DaliyViewSameTask.画手查看 定时任务:");
        try {
            LikeSenseQueryVO likeSenseQueryVO = new LikeSenseQueryVO(16053307L,null);
            sameService.viewUserSense(likeSenseQueryVO);
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyViewSameTask.画手查看.Error ",e);
        }
    }
}
