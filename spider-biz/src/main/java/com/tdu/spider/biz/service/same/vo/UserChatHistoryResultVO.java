package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: UserChatHistoryResultVO.java, v 0.1 2018年02月18日 上午7:19 tangdu Exp $
 */
@Getter
@Setter
public class UserChatHistoryResultVO {
    private List<UserChatHistoryVO> results;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }
}
