package com.tdu.spider.biz.service.douban.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieInfoVO.java, v 0.1 2018年04月15日 上午7:09 tangdu Exp $
 */
@Setter
@Getter
public class DouMovieInfoVO {
    private RatingVO rating;
    private List<String> genres;
    private String title;
    private List<DirectorsVO> casts;
    private Integer collect_count;
    private String original_title;
    private String subtype;
    private String year;
    private String alt;
    private String id;
    private MovieImageVO images;

    @Setter
    @Getter
    public static class RatingVO {
        private Integer max;
        private Float average;
        private String stars;
        private Integer min;
    }

    @Setter
    @Getter
    public static class DirectorsVO {
        private String alt;
        private String name;
        private String id;
        private MovieImageVO avatars;
    }

    @Setter
    @Getter
    public static class MovieImageVO {
        private String small;
        private String large;
        private String medium;
    }
}
