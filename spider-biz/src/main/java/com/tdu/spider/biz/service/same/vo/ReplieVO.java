package com.tdu.spider.biz.service.same.vo;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

@Data
public class ReplieVO implements Serializable {
    private Long   id;
    private Long   created_at;
    private String content;
    private String target_type;
    private String user_id;
    private Long   target_id;
    private Long   target_user_id;
    private Long   target_parent_id;
    private Long   target_parent_user_id;
    private UserVO user;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
