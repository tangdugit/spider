package com.tdu.spider.biz.service.same.thread;

import com.google.common.collect.Lists;
import com.tdu.spider.biz.service.same.SameApiService;
import com.tdu.spider.biz.service.same.vo.SenseLikeVO;
import com.tdu.spider.biz.util.ConvertUtils;
import com.tdu.spider.dao.SameSenseLikeRepository;
import com.tdu.spider.model.SenseLikeDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SameViewHisTask.java, v 0.1 2018年02月10日 上午10:59 tangdu Exp $
 */
public class SameViewHisTask implements Callable<List<SenseLikeDO>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SameViewHisTask.class);
    private SameApiService          sameApiService;
    private SameSenseLikeRepository sameSenseLikeRepository;
    private Long                    senseId;
    private Long                    targetUserId;

    public SameViewHisTask(SameApiService sameApiService, SameSenseLikeRepository sameSenseLikeRepository, Long senseId,Long targetUserId) {
        this.sameApiService = sameApiService;
        this.sameSenseLikeRepository = sameSenseLikeRepository;
        this.senseId=senseId;
        this.targetUserId=targetUserId;
    }

    @Override
    public List<SenseLikeDO> call() throws Exception {
        LOGGER.info("SameViewHisTask_start senseId:{},offset:{}", senseId);
        long start=System.currentTimeMillis();
        List<SenseLikeDO> senseLikeDOS = Lists.newArrayList();
        for(int i=0;i<3;i++) {
            try {
                List<SenseLikeVO> senseLikeVOS = sameApiService.querySenseLikeUsers(senseId);
                for (SenseLikeVO senseLikeVO : senseLikeVOS) {
                    SenseLikeDO senseLikeDO = ConvertUtils.copy(senseLikeVO, SenseLikeDO.class);
                    senseLikeDO.setUserId(senseLikeVO.getId());
                    senseLikeDO.setSource(2);
                    senseLikeDO.setSenseId(senseId);
                    senseLikeDO.setTargetUserId(targetUserId);
                    senseLikeDOS.add(senseLikeDO);
                }
                break;
            } catch (Exception e) {
                LOGGER.error("SameViewHisTask_querySenseLikeUsers_error msg:{}",e.getMessage());
            }
        }
//        for(int i=0;i<3;i++) {
//            try {
//                List<SenseLikeVO> senseLikeVOS = sameApiService.querySenseViewUsers(senseId);
//                for (SenseLikeVO senseLikeVO : senseLikeVOS) {
//                    SenseLikeDO senseLikeDO = ConvertUtils.copy(senseLikeVO, SenseLikeDO.class);
//                    senseLikeDO.setUserId(senseLikeVO.getId());
//                    senseLikeDO.setSource(1);
//                    senseLikeDO.setSenseId(senseId);
//                    senseLikeDO.setTargetUserId(targetUserId);
//                    senseLikeDOS.add(senseLikeDO);
//                }
//                break;
//            } catch (Exception e) {
//                LOGGER.error("SameViewHisTask_querySenseViewUsers_error msg:{}",e.getMessage());
//            }
//        }
        LOGGER.info("SameViewHisTask_end senseId:{},cost:{}ms", senseId, (System.currentTimeMillis() - start));
        return senseLikeDOS;
    }
}
