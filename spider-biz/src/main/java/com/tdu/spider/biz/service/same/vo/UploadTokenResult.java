package com.tdu.spider.biz.service.same.vo;


import java.io.Serializable;

public class UploadTokenResult implements Serializable {
    private String host;
    private String token;
    private String expire_at;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpire_at() {
        return expire_at;
    }

    public void setExpire_at(String expire_at) {
        this.expire_at = expire_at;
    }
}
