package com.tdu.spider.biz.task;

import com.tdu.spider.biz.service.same.SameService;
import com.tdu.spider.dao.SameSenseRepository;
import com.tdu.spider.model.SameSenseDO;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

/**
 * 默认渠道下载[1288952]
 *
 *
 * @author tangdu@qccr.com
 * @version $: VideoDownLoadTask.java, v 0.1 2017年10月2017/10/21日 上午7:31 tangdu Exp $
 * @name TODO: VideoDownLoadTask
 */
@Component
public class VideoDownLoadTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(VideoDownLoadTask.class);

    @Autowired
    private SameService sameService;

    @Autowired
    private SameSenseRepository sameSenseRepository;

    //@Scheduled(cron = "0 0 12 * * ?")
    public void 下载视频Sense至本地() {
        LOGGER.info("Scheduled.DaliyDianzanSameTask.begin.下载视频Sense至本地 定时任务:");
        Pageable pageable = PageRequest.of(1, 100);
        Page<SameSenseDO> senseDOS = sameSenseRepository.findAll(pageable);
        for (SameSenseDO senseDO : senseDOS.getContent()) {
            try {
                Response response = Request.Get(senseDO.getPhoto())
                        .addHeader("User-Agent", "NewsClient/2.4.2 (iPhone; iOS 10.3.2; Scale/2.00)")
                        .execute();
                response.saveContent(new File(senseDO.getId()+".mp4"));
            } catch (IOException e) {
                LOGGER.error("下载视频Sense params:{}",senseDO,e);
            }
        }
        LOGGER.info("Scheduled.DaliyDianzanSameTask.end.下载视频Sense至本地 定时任务:");
    }
}
