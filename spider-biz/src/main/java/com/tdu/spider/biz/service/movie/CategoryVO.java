package com.tdu.spider.biz.service.movie;

import lombok.Data;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: CategoryModel.java, v 0.1 2018年07月11日 下午11:13 tangdu Exp $
 */
@Data
public class CategoryVO {
    private String categoryCode;
    private String categoryTitle;
    private String categoryImage;
}
