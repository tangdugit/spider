package com.tdu.spider.biz.service.same.vo;

import com.tdu.spider.model.SameChannelDO;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
public class SenSeVO implements Serializable {

    private Long           id;
    private Boolean        is_active;
    private String         txt;
    private Long           user_id;
    private Long           created_at;
    private Long           channel_id;
    private String         photo;
    private Integer        height;
    private Integer        width;
    private Integer        media_id;
    private Boolean        is_folded;
    private Integer        cate;
    private Integer        likes;
    private Integer        views;
    private Integer        replies;

    private UserVO         user;
    private SameChannelDO  channel;
    private MediaVO        media;
    private List<ReplieVO> recent_replies;
    private Integer        is_liked;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
