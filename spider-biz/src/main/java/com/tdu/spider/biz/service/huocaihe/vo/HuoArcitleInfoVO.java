package com.tdu.spider.biz.service.huocaihe.vo;

import lombok.Data;

@Data
public class HuoArcitleInfoVO {
    private String id;

    private String topic_id;

    private String topic_name;

    private String topic_type;

    private String view;

    private String author_id;

    private String link_url;

    private String voice_url_mp3;

    private String voice_url_aac;

    private String voice_thumb;

    private String voice_thumb_org;

    private String voice_view;

    private String voice_long_time;

    private String voice_wave_data;

    private double voice_sample_rate;

    private String author;

    private String avatar;

    private String role;

    private String thumb;

    private String thumb_org;

    private String width;

    private String height;

    private String content;

    private String heart;

    private String forward;

    private String repost;

    private String comment;

    private String created;

    private String pubtime2;

    private String is_private;

    private String vip_type;

    private String relation;

    private String isself;

    private String is_fav;

    private String is_like;
}
