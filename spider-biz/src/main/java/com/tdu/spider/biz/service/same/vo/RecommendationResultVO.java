package com.tdu.spider.biz.service.same.vo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

public class RecommendationResultVO implements Serializable {
    private String                  type;

    private Reason                  reason;

    private String                  sort;

    private RecommendationChannelVO channel;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Reason getReason() {
        return reason;
    }

    public void setReason(Reason reason) {
        this.reason = reason;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public RecommendationChannelVO getChannel() {
        return channel;
    }

    public void setChannel(RecommendationChannelVO channel) {
        this.channel = channel;
    }

    public class Reason implements Serializable {
        private Integer type;
        private String  description;

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
        }

    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
