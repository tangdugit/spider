package com.tdu.spider.biz.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.apache.http.client.fluent.Request;
import org.springframework.stereotype.Service;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: ProxyService.java, v 0.1 2018年06月23日 上午11:56 tangdu Exp $
 */
@Service
@Slf4j
public class ProxyService {

    public HttpHost getHttpProxy(){
        try {
            String address = Request.Get("http://115.28.208.208:5010/get/").execute().returnContent().asString();
            log.info("GET_ADDRSS {}",address);
            return new HttpHost(address.split(":")[0], Integer.valueOf(address.split(":")[1]));
        } catch (Exception e) {
            return null;
        }
    }
}
