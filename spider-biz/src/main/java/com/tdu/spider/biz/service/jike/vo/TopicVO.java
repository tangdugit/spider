package com.tdu.spider.biz.service.jike.vo;


import lombok.Data;

@Data
public class TopicVO {

    private String id;
    private String messagePrefix;
    private String content;
    private Integer topicId;
    private String briefIntro;
    private String keywords;
    private String timeForRank;
    private String lastMessagePostTime;
    private String createdAt;
    private String updatedAt;
    private Integer subscribersCount;
    private Integer subscribedStatusRawValue;
    private String subscribedAt;
    private String topicType;
    private boolean isAnonymous;
    private boolean isDreamTopic;
    private boolean isValid;
    private String operateStatus;
    private String internalTags;
    private String friendsAlsoSubscribe;
    private String pictureUrl;
    private String thumbnailUrl;
    private String ref;

    private JikeSourceVO rectanglePicture;
    private JikeSourceVO squarePicture;

    @Data
    private class Maintainer{
        private String username;
        private String screenName;
        private String updatedAt;
        private String profileImageUrl;
        private String bio;
        private String gender;
        private String ref;
        private JikeSourceVO avatarImage;
    }
}

