package com.tdu.spider.biz.service.qutou.vo;


import lombok.Data;

@Data
public class QuTouArticleQueryVO {

    private String OSVersion="11.1.2";
    private Integer cid=255;
    private String content_type;
    private String deviceCode="CF01B801-232A-4B43-8E16-7893646660B0";
    private Integer dtu=100;
    private Long max_time=0l;
    private String network="WIFI";
    private Integer op=2;
    private Integer page=1;
    private String sign;
    private Long time;
    private String tk="";
    private String token;
    private String uuid;
    private Integer version=20600;
    private String versionName="2.6.0.1106.1150";



    public Long getTime() {
        this.time=System.currentTimeMillis()/1000l;
        return time;
    }
}
