package com.tdu.spider.biz.service.instagram;

import com.tdu.spider.biz.service.instagram.vo.InstagramResultVO;


public interface InstagramService {

    InstagramResultVO downloadImg(String url);
}
