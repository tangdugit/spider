package com.tdu.spider.biz.task;

import com.tdu.spider.biz.service.xiachufang.XiachufangService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: DaliyXiachufangTask.java, v 0.1 2018年01月27日 上午9:02 tangdu Exp $
 */
@Component
public class DaliyXiachufangTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(DaliyXiachufangTask.class);

    @Autowired
    private XiachufangService xiachufangService;

    @Scheduled(cron="0 0 6,8,9,11,12,14,16,17,18,19,20,21,23 * * ?")
    public void 好友点赞() {
        LOGGER.info("Scheduled.DaliyXiachufangTask.好友点赞 定时任务:");
        try {
            xiachufangService.autoDdigg();
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyXiachufangTask.好友点赞.Error ", e);
        }
    }

    @Scheduled(cron="0 0/15 6-23 ? * * ")
    public void 全网点赞() {
        LOGGER.info("Scheduled.DaliyXiachufangTask.全网点赞 定时任务:");
        try {
            xiachufangService.autoHotDdigg();
        } catch (Exception e) {
            LOGGER.error("Scheduled.DaliyXiachufangTask.全网点赞.Error ", e);
        }
    }

}
