package com.tdu.spider.biz.service.same.vo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: UserResultVO.java, v 0.1 2017年04月2017/4/29日 上午10:18 tangdu Exp $
 * @name TODO: UserResultVO
 */
public class UserResultVO implements Serializable {
    private UserVO user;

    public UserVO getUser() {
        return user;
    }

    public void setUser(UserVO user) {
        this.user = user;
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
