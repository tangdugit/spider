package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SameAnalyseQueryVO.java, v 0.1 2018年02月03日 下午6:14 tangdu Exp $
 */
@Setter
@Getter
public class SameAnalyseQueryVO {

    private Long userId;
}
