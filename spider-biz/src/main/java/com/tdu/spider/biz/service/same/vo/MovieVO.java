package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

@Setter
@Getter
public class MovieVO implements Serializable {
    private Long   id;
    /** 作者 **/
    private String actor;
    /** mid **/
    private Long   mid;
    /** 视频介绍 **/
    private String trailer;
    /** 图片 **/
    private String cover;
    /** 主演 **/
    private String director;
    /** 作者 **/
    private String img;
    /** 子标题 **/
    private String sub_title;
    /** 标题 **/
    private String title;
    /** 年份 **/
    private String year;


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
