package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: SameChannelVO.java, v 0.1 2018年02月03日 下午10:05 tangdu Exp $
 */
@Setter
@Getter
public class SameChannelVO {
    private Long    id;
    private String  name;
    private String  icon;
    private Integer cate;
    private Integer times;
    private Long    user_id;
    private Integer mode;

    private Integer count;
}
