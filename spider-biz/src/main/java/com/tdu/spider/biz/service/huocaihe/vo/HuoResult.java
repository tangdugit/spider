package com.tdu.spider.biz.service.huocaihe.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class HuoResult<T> implements Serializable {

    String  error_code;
    String  error_msg;
    T data;
}
