package com.tdu.spider.biz.service.same.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class CommonResult<T> implements Serializable {

    private Integer code;

    private String  detail;

    private String  duration;

    private T       data;

}
