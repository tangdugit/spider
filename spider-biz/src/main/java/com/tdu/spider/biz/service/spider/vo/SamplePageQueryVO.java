package com.tdu.spider.biz.service.spider.vo;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: SamplePageQueryVO.java, v 0.1 2017年04月2017/4/25日 下午1:17 tangdu Exp $
 * @name TODO: SamplePageQueryVO
 */
public class SamplePageQueryVO {
    String select;String url;

    public String getSelect() {
        return select;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
