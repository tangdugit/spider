package com.tdu.spider.biz.service.xiachufang.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: FeedsVO.java, v 0.1 2018年01月27日 上午9:08 tangdu Exp $
 */
@Getter
@Setter
public class FeedsVO implements Serializable {
    private String kind;
    private String create_time;
    private FeedsDishVO target;
}
