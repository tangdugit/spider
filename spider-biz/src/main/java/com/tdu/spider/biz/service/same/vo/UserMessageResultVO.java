package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: UserMessageResultVO.java, v 0.1 2018年02月18日 下午7:29 tangdu Exp $
 */
@Getter
@Setter
public class UserMessageResultVO {
    private Integer count;
    private List<UserMessageVO> results;
    private String nextmid;
}
