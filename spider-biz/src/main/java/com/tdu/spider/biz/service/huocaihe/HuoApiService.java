package com.tdu.spider.biz.service.huocaihe;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.google.common.collect.Lists;
import com.tdu.spider.biz.service.huocaihe.vo.*;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.Args;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class HuoApiService  {

    private static final Logger LOGGER = LoggerFactory.getLogger(HuoApiService.class);

    
    public List<HuoTopicVO> queryTopic(HuoQueryVO queryVO) throws Exception {
        Args.notNull(queryVO, "queryVO");
        Request request = Request.Post("https://soa.ihuochaihe.com:442/v1/topic/category")
            .addHeader("X-HCH-KEY", "pbrle2t7qs9fu169wraezdu8pjgqyau4ovbv7qdkpqfci8ad")
            .addHeader("Accept", "*/*").addHeader("Accept-Language", "zh-Hans-CN;q=1")
            .addHeader("Content-Type", "application/json")
            .addHeader("User-Agent", "matchbox/4.9.0 (iPhone; iOS 10.3.2; Scale/2.00)");

        JSONObject json = new JSONObject();
        json.put("source", "APP");
        json.put("platform", "IOS");
        json.put("size", "20");
        json.put("token_key", queryVO.getToken_key());
        json.put("version", "4.9.0");
        json.put("udid", queryVO.getUdid());
        json.put("register_id", queryVO.getRegister_id());
        json.put("lastid", queryVO.getLastid() == null ? "0" : queryVO.getLastid());
        json.put("uid", queryVO.getUid());
        json.put("category_id", queryVO.getCategory_id());
        StringEntity stringEntity = new StringEntity(json.toJSONString(),
            ContentType.APPLICATION_JSON);
        request.body(stringEntity);
        Response response = request.execute();
        String content = response.returnContent().asString();
        LOGGER.info("queryTopic url:{},params:{},result:{}", "", json.toJSONString(), content);

        if (StringUtils.hasLength(content)) {
            HuoResult<TopicDataVO> result = JSON.parseObject(content,
                new TypeReference<HuoResult<TopicDataVO>>() {
                });
            return result.getData().getList();
        }
        return Lists.newArrayList();
    }

    public HuoArcitleVO queryArticle(HuoQueryVO queryVO) throws Exception {
        Args.notNull(queryVO, "queryVO");
        Request request = Request.Post("https://soa.ihuochaihe.com:442/v1/topic/threads")
            .addHeader("X-HCH-KEY", "pbrle2t7qs9fu169wraezdu8pjgqyau4ovbv7qdkpqfci8ad")
            .addHeader("Accept", "*/*").addHeader("Accept-Language", "zh-Hans-CN;q=1")
            .addHeader("Content-Type", "application/json")
            .addHeader("User-Agent", "matchbox/4.9.0 (iPhone; iOS 10.3.2; Scale/2.00)");

        JSONObject json = new JSONObject();
        json.put("source", "APP");
        json.put("platform", "IOS");
        json.put("size", "20");
        json.put("p", queryVO.getP() == null ? "" : queryVO.getP());
        json.put("token_key", queryVO.getToken_key());
        json.put("version", "4.9.0");
        json.put("udid", queryVO.getUdid());
        json.put("register_id", queryVO.getRegister_id());
        json.put("lastid", queryVO.getLastid() == null ? "0" : queryVO.getLastid());
        json.put("uid", queryVO.getUid());
        json.put("topic_id", queryVO.getTopic_id());
        json.put("type", queryVO.getType() == null ? "hot" : queryVO.getType());
        json.put("category_id", queryVO.getCategory_id());
        StringEntity stringEntity = new StringEntity(json.toJSONString(),
            ContentType.APPLICATION_JSON);
        request.body(stringEntity);
        Response response = request.execute();
        String content = response.returnContent().asString();
        LOGGER.info("queryArticle url:{},params:{},result:{}", "", json.toJSONString(), content);

        if (StringUtils.hasLength(content)) {
            HuoResult<HuoArcitleVO> result = JSON.parseObject(content,
                new TypeReference<HuoResult<HuoArcitleVO>>() {
                });
            return result.getData();
        }
        return null;
    }
}
