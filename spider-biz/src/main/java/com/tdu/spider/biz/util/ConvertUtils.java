package com.tdu.spider.biz.util;

import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class ConvertUtils {

    public static <T> T copy(Object source, T target, String... ingoreProps) {
        BeanUtils.copyProperties(source, target, ingoreProps);
        return target;
    }

    public static <T> T copy(Object source, Class<T> targetClass, String... ingoreProps) {
        T t = BeanUtils.instantiateClass(targetClass);
        BeanUtils.copyProperties(source, t, ingoreProps);
        return t;
    }

    public static <T> List<T> copyList(List<?> list, Class<T> target, String... ingoreProps) {
        List<T> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(list)) {
            return result;
        }
        for (Object object : list) {
            result.add(copy(object, target, ingoreProps));
        }
        return result;
    }
}
