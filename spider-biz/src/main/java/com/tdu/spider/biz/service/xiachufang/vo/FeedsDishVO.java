package com.tdu.spider.biz.service.xiachufang.vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: FeedsDishVO.java, v 0.1 2018年01月27日 上午9:10 tangdu Exp $
 */
@Getter
@Setter
public class FeedsDishVO implements Serializable{
    private String pic;
    private String id;
    private  String diggs;
    private String name;
    private String url;
}
