package com.tdu.spider.biz.service.spider;

import com.tdu.spider.biz.service.spider.vo.SamplePageQueryVO;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * TODO: detail description
 *
 * @author tangdu@qccr.com
 * @version $: SamplePageSpiderServiceImpl.java, v 0.1 2017年04月2017/4/25日 下午1:17 tangdu Exp $
 * @name TODO: SamplePageSpiderServiceImpl
 */
@Service
public class SamplePageSpiderService {

    public String getContent(SamplePageQueryVO samplePageQueryVO) {
        try {
            Response response = Request.Get(samplePageQueryVO.getUrl()).userAgent("").execute();
            String content=response.returnContent().asString();
            Document document = Jsoup.parse(content);
            Elements elements = document.select(samplePageQueryVO.getSelect());
            return elements.text();
        } catch (IOException e) {
            return null;
        }
    }
}
