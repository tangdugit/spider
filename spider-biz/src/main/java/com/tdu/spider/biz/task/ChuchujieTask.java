package com.tdu.spider.biz.task;

import com.google.common.collect.Lists;
import org.apache.http.NameValuePair;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 楚楚街定时抽奖(每天10点执行)
 *
 * @author tangdu
 * @version $: ChuchujieTask.java, v 0.1 2018年01月24日 下午12:55 tangdu Exp $
 */
@Component
public class ChuchujieTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChuchujieTask.class);

    public void chou(String shopToken,String cookie,String formId,String sid){
        try {
            LOGGER.info("楚楚街定时抽奖----->"+shopToken);
            List<NameValuePair> valuePairs = Lists.newArrayList();
            valuePairs.add(new BasicNameValuePair("shopToken", shopToken));
            valuePairs.add(new BasicNameValuePair("platform", "iphone"));
            valuePairs.add(new BasicNameValuePair("version", "3.16.1"));
            valuePairs.add(new BasicNameValuePair("packageName", "com.culiu.huanletao"));
            valuePairs.add(new BasicNameValuePair("imei", "87D99354-FDB7-4EED-A6C6-8AD79EA7AC48"));
            valuePairs.add(new BasicNameValuePair("deviceId", "6936f8a59fc9db3440807bd1452e9203"));
            valuePairs.add(new BasicNameValuePair("tag12", "1"));
            Request request = Request.Post("https://huodong-hb.chuchujie.com/WeChatCoupon/public/pick_api.php")
                    .addHeader("Referer", "https://huodong-hb.chuchujie.com/WeChatCoupon/index.html?source=8")
                    .userAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_2 like Mac OS X) AppleWebKit/604.4.7 (KHTML, like Gecko) Mobile/15C202")
                    .addHeader("Cookie", cookie).bodyForm();
            Response response = request.bodyForm(valuePairs).execute();
            String result = response.returnContent().asString();
            LOGGER.info("A抽奖-->"+result);

            valuePairs.clear();
            valuePairs.add(new BasicNameValuePair("formId", formId));
            request = Request.Post("https://pintuan.chuchujie.com/app.php?c=HuoDong&m=pickBonus&sid="+sid)
                    .addHeader("Referer", "https://servicewechat.com/wxbe828768f6d149f1/36/page-frame.html")
                    .addHeader("loginType","smallapp")
                    .userAgent("Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_2 like Mac OS X) AppleWebKit/604.4.7 (KHTML, like Gecko) Mobile/15C202");
            response = request.bodyForm(valuePairs).execute();
            result = response.returnContent().asString();
            LOGGER.info("B抽奖-->"+result);
        }catch (Exception e){}
    }

    @Scheduled(cron = "0 0 10 * * ?")
    public void 楚楚街定时抽奖() {
        LOGGER.info("Scheduled.DaliyDianzanSameTask.begin.楚楚街定时抽奖 定时任务:");
        try {
            chou("ccjce15475350511d10000914353330a1547535051","CNZZDATA1258985697=1045959281-1515997097-%7C1516754403; access_token=ccjce15475350511d10000914353330a1547535051; client_type=ios; client_version=3.16.1; imei=87D99354-FDB7-4EED-A6C6-8AD79EA7AC48; package_name=com.culiu.huanletao; wx_coupon_per_day=91435333; wx_coupon_ccj_qrcodePic=http://huodongcdn.chuchujie.com/feedup/20180115/NTFlZDRjN2M1MjY0Y2ViMTY5NDgyY2Mz.png; UM_distinctid=160f895eba61-0c8da78ffc77bc-2f5f376a-3d10d-160f895eba7349",
                    "7f158ddb43e0b196c051478068c421d4","741CEBA923EDE37D4C6F69F15461A79B");
            chou("ccjd71548325628b37000092340385e91548325628","CNZZDATA1258985697=189746456-1516787794-%7C1528686408; access_token=ccjd71548325628b37000092340385e91548325628; client_type=ios; client_version=3.16.1; imei=F2474F84-2C20-4727-A0F9-7EC8704BC9F8; package_name=com.culiu.huanletao; UM_distinctid=16127b407d2b4-077b3a11b5d0f1-2e5e056e-2c600-16127b407d32c2",
                    "7f158ddb43e0b196c051478068c421d4","741CEBA923EDE37D4C6F69F15461A79B");
        } catch (Exception e) {
            LOGGER.error("楚楚街定时抽奖", e);
        }
        LOGGER.info("Scheduled.DaliyDianzanSameTask.end.楚楚街定时抽奖 定时任务:");
    }
}
