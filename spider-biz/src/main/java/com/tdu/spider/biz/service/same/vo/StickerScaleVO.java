package com.tdu.spider.biz.service.same.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: StickerVO.java, v 0.1 2018年02月18日 下午7:36 tangdu Exp $
 */
@Setter
@Getter
public class StickerScaleVO {

    private Integer angle;
    private Integer zoom;
    
}
