package com.tdu.spider.biz.service.same.vo;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.List;


public class ChannelSubscribeVO implements Serializable {
    private List<ChannelInfoVO> subscribe_channels;

    public List<ChannelInfoVO> getSubscribe_channels() {
        return subscribe_channels;
    }

    public void setSubscribe_channels(List<ChannelInfoVO> subscribe_channels) {
        this.subscribe_channels = subscribe_channels;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
