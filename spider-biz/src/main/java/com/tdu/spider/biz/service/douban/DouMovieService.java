package com.tdu.spider.biz.service.douban;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.tdu.spider.biz.cont.Keys;
import com.tdu.spider.biz.service.douban.vo.DouMovieInfoResultVO;
import com.tdu.spider.biz.service.douban.vo.DouMovieInfoVO;
import com.tdu.spider.biz.util.ConvertUtils;
import com.tdu.spider.dao.MovieInfoRepository;
import com.tdu.spider.model.MovieInfoDO;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * TODO: detail description
 *
 * @author tangdu
 * @version $: MovieService.java, v 0.1 2018年04月15日 上午7:16 tangdu Exp $
 */
@Service
public class DouMovieService {
    @Autowired
    private MovieInfoRepository movieInfoRepository;

    /**
     * 更新豆瓣250
     * @return
     */
    public boolean updateTop250() {
        try {
            movieInfoRepository.deleteAll();
            for (int i = 0; i < 300; i+=100) {
                Request              request     = Request.Get("http://api.douban.com/v2/movie/top250?start="+i+"&count=100").userAgent(Keys.USER_AGENT);
                Response             response    = request.execute();
                String               asString    = response.returnContent().asString();
                DouMovieInfoResultVO movieInfoVO = JSON.parseObject(asString, DouMovieInfoResultVO.class);
                if(movieInfoVO==null && CollectionUtils.isEmpty(movieInfoVO.getSubjects())){
                    return false;
                }
                List<MovieInfoDO> movieDOList = Lists.newArrayList();
                for (DouMovieInfoVO infoVO : movieInfoVO.getSubjects()) {
                    MovieInfoDO movieInfoDO = ConvertUtils.copy(infoVO, MovieInfoDO.class);
                    movieInfoDO.setCategorys(Lists.newArrayList("top250"));
                    movieInfoDO.setRating(ConvertUtils.copy(infoVO.getRating(),MovieInfoDO.RatingDO.class));
                    List<MovieInfoDO.DirectorsDO> directorsDOS=Lists.newArrayList();
                    for (DouMovieInfoVO.DirectorsVO directorsVO : infoVO.getCasts()) {
                        MovieInfoDO.DirectorsDO directorsVO1 = ConvertUtils.copy(directorsVO, MovieInfoDO.DirectorsDO.class);
                        if(directorsVO.getAvatars()!=null) {
                            directorsVO1.setAvatars(ConvertUtils.copy(directorsVO.getAvatars(), MovieInfoDO.MovieImageDO.class));
                        }
                        directorsDOS.add(directorsVO1);
                    }
                    movieInfoDO.setCasts(directorsDOS);
                    movieInfoDO.setImages(ConvertUtils.copy(infoVO.getImages(),MovieInfoDO.MovieImageDO.class));
                    movieDOList.add(movieInfoDO);
                }
                movieInfoRepository.saveAll(movieDOList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }


}
