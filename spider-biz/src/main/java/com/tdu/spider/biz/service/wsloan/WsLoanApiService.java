
package com.tdu.spider.biz.service.wsloan;

import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;

/**
 * 温商贷 API
 *
 * @author tangdu
 * @version 1.0.0, 2017-07-18
 */
@Service
public class WsLoanApiService {
    private static final Logger LOGGER = LoggerFactory.getLogger(WsLoanApiService.class);

    
    public void qiandao(String userId) throws Exception {
        String url = "https://sq2.wsloan.com/api/bbsAPi.ashx?q=CheckIn&userid=" + userId
                + "&ffrom=1&from=1&pagesize=20";
        Request request = Request.Get(url)
                .addHeader("Accept", "application/json")
                .addHeader("Origin", "https://sq1.wsloan.com")
                .addHeader("Pragma", "no-cache")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Cookie",
                        "acw_tc=AQAAADI96kdXFwEA8iNs2j2Hxsn+xwdD; ASP.NET_SessionId=1lawmlunvlvvjubthqhk3znp; UM_distinctid=15c1edbba0b39-062dac28-522b2139-38400-15c1edbba0e47; CNZZDATA1260021669=191364619-1495162124-%7C1495506648; SERVERID=2d551775314371629e3ddb98211f7280|1495507954|1495381635")
                .addHeader("Accept-Language", "zh-CN,en-US;q=0.8")
                .addHeader("Referer",
                        "https://sq1.wsloan.com/task-sign")
                .addHeader("User-Agent",
                        "Mozilla/5.0 (Linux; Android 5.1; XT1096 Build/LPES23.32-25-5-2; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043220 Safari/537.36/wsloan/Android");
        Response response = request.execute();
        String content = response.returnContent().asString();

        LOGGER.info("qiandao url:{},params:{},result:{}", url, userId, content);
    }

    
    public void choujiang(String userId) throws Exception {
        String url = "https://www.wsloan.com/m/apppages/active/zp/data.ashx?t=cj&lx=null&userid=" + URLEncoder.encode(userId,"UTF-8") + "&ffrom=2&v=02934572858034905";
        Request request = Request.Get(url)
                .addHeader("Accept", "application/json")
                .addHeader("Origin", "https://sq1.wsloan.com")
                .addHeader("Pragma", "no-cache")
                .addHeader("X-Requested-With", "com.wsloan")
                .addHeader("Cache-Control", "no-cache")
                .addHeader("Referer",
                        "https://www.wsloan.com/m/appActive/active/wheel/index.html?isneedlogin=1&amp;amp;lx=2&amp;amp;laiyuan=shouye")
                .addHeader("Cookie",
                        "acw_tc=AQAAADI96kdXFwEA8iNs2j2Hxsn+xwdD; ASP.NET_SessionId=1lawmlunvlvvjubthqhk3znp; UM_distinctid=15c1edbba0b39-062dac28-522b2139-38400-15c1edbba0e47; CNZZDATA1260021669=191364619-1495162124-%7C1495506648; SERVERID=2d551775314371629e3ddb98211f7280|1495507954|1495381635")
                .addHeader("User-Agent",
                        "Mozilla/5.0 (Linux; Android 5.1; XT1096 Build/LPES23.32-25-5-2; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043220 Safari/537.36/wsloan/Android");
        Response response = request.execute();
        String content = response.returnContent().asString();
        LOGGER.info("choujiang url:{},params:{},result:{}", url, userId, content);
    }

    
    public void choujiangAfter(String userId) throws Exception {
        String url = "https://www.wsloan.com/m/apppages/active/zp/data.ashx?t=fxlq&userid="+URLEncoder.encode(userId,"UTF-8")+"&ffrom=1&v=025886655228641786";
        Request request = Request.Get(url)
                .addHeader("Accept", "*/*")
                .addHeader("Cookie",
                        "acw_tc=AQAAADI96kdXFwEA8iNs2j2Hxsn+xwdD; ASP.NET_SessionId=1lawmlunvlvvjubthqhk3znp; UM_distinctid=15c1edbba0b39-062dac28-522b2139-38400-15c1edbba0e47; CNZZDATA1260021669=191364619-1495162124-%7C1495506648; SERVERID=2d551775314371629e3ddb98211f7280|1495507954|1495381635")
                .addHeader("Accept-Language", "zh-CN,en-US;q=0.8")
                .addHeader("Referer",
                        "https://www.wsloan.com/m/appActive/active/wheel/index.html?isneedlogin=1&lx=2&laiyuan=shouye")
                .addHeader("User-Agent",
                        "Mozilla/5.0 (Linux; Android 5.1; XT1096 Build/LPES23.32-25-5-2; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/53.0.2785.49 Mobile MQQBrowser/6.2 TBS/043220 Safari/537.36/wsloan/Android");
        Response response = request.execute();
        String   content  = response.returnContent().asString();

        LOGGER.info("choujiangAfter url:{},params:{},result:{}", url, userId, content);
    }
}
