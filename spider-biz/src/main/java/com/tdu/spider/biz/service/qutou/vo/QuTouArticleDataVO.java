package com.tdu.spider.biz.service.qutou.vo;

import lombok.Data;

import java.util.List;

@Data
public class QuTouArticleDataVO {
    private String cid;
    private String pv_id;
    private Integer count;
    private String op;
    private Integer page;
    private Long min_time;
    private Long max_time;
    private Long show_time;
    private List<String> top;
    private List<QuTouArticleVO> data;
}
