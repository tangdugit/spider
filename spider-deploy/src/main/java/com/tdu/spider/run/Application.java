package com.tdu.spider.run;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication(scanBasePackages = {
        "com.tdu.spider.biz.service",
        "com.tdu.spider.biz.aspect",
        "com.tdu.spider.model",
        "com.tdu.spider.biz.task",
        "com.tdu.spider.web.controller"
},exclude={
        DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class,
        ThymeleafAutoConfiguration.class})
@EnableScheduling
@EnableMongoRepositories(basePackages = {"com.tdu.spider.dao"})
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class Application  {

    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(20);
        scheduler.setThreadNamePrefix("spring-task-thread");
        return scheduler;
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
